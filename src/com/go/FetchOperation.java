package com.go;

import java.io.IOException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Time;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.util.EntityUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import tools.files.LoadProps;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import databaseTools.MySQLUtils;
import databaseTools.OurSqlConnection;

public class FetchOperation {
	
	

	public static String getHtmlFromURL(String input, String charSet,
			boolean isZh) throws Exception {
		String result = "";
		HttpClient httpclient = new DefaultHttpClient();
		HttpConnectionParams.setSoTimeout(httpclient.getParams(), 10000);
		HttpGet httpGet = new HttpGet(input);
		System.out.println("Extracting..." + httpGet.getURI());

		HttpResponse response = httpclient.execute(httpGet);
		HttpEntity entity = response.getEntity();
		String returnText = EntityUtils.toString(entity, charSet);
		result = returnText;
		if (isZh) {
			// 簡體轉繁體
			result = ChangeCode.toLong(result);
		}

		System.out.println("Finished");
		httpclient.getConnectionManager().shutdown();
		return result;
	}

	public static Document getDocumentFromURL(String url, String charSet,
			boolean isZh) throws Exception {
		Document doc = null;

		try {
			String sql = "";
			// 如果資料庫存在這一張表的話，直接取出資料庫
			if (isDB(url)) {
				System.out.println("存在 in DB");
				OurSqlConnection conn = new OurSqlConnection(
						LoadProps.getProperties("databaseName"));// test
																	// (Chris)跟isDB的資料庫名稱不同，要Check一下
				sql = "select * from  htmlTable where url='" + url + "'";
				conn.setSql(sql);

				ResultSet rs = conn.getResultSet();
				rs.next();

				String htmlDB = rs.getString("html");
				conn.close();
				if (isZh) {
					// 簡體轉繁體
					htmlDB = ChangeCode.toLong(htmlDB);
				}
				doc = Jsoup.parse(htmlDB);
			}
			// 資料庫不存在此表
			else {
				System.out.println("需要爬：" + url);

				doc = Jsoup.connect(url).timeout(100000).get();
				String html = doc.html();
				// System.out.println(html);
				if (isZh) {
					// 簡體轉繁體
					html = ChangeCode.toLong(doc.html());
				}
				// 存到資料庫

				Class.forName("com.mysql.jdbc.Driver");
				String tableName = LoadProps.getProperties("databaseName");
				String connStr = LoadProps.getProperties("url") + tableName
						+ "?useUnicode=true&characterEncoding=utf-8&user="
						+ LoadProps.getProperties("user") + "&password="
						+ LoadProps.getProperties("password");

				// String connStr =
				// "jdbc:mysql://140.125.84.58:3306/103wikiproject?useUnicode=true&characterEncoding=utf-8&user=root&password=root";
				Connection conn2 = DriverManager.getConnection(connStr);
				sql = "insert into htmlTable(url,html,time) values(?,?,?)";
				try {
					PreparedStatement sss = conn2.prepareStatement(sql);
					sss.setString(1, url);
					sss.setString(2, html);
					long lDateTime = new Date().getTime();
					sss.setLong(3, lDateTime);
					sss.executeUpdate();
					conn2.close();
				} catch (Exception e) {
					conn2.close();
					System.out.println(e.toString());
				}

			}

		} catch (IOException e) {

			System.out.println(e);
		} finally {

			System.out.println("Finished");
			return doc;
		}
	}

	public static boolean isDB(String url) {
		OurSqlConnection conn = new OurSqlConnection(
				LoadProps.getProperties("databaseName"));
		String sql = "Select count(*) from htmlTable where url='" + url + "'";
		conn.setSql(sql);
		ResultSet rs = conn.getResultSet();
		int dataCount;
		try {
			rs.next();
			dataCount = rs.getInt("count(*)");
			if (dataCount == 0) {
				conn.close();
				return false;
			} else {
				conn.close();
				return true;
			}
		} catch (SQLException e) {
			conn.close();
			System.out.println("?");
			return false;
		}
	}
}
