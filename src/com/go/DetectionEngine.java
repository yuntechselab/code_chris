package com.go;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import entity.WikiObject;

public class DetectionEngine {
	
	// 給適當的Type
	public static String getProperType(String searchName) {
		String result = "";

		if (isCompany(searchName)) {
			System.out.println("do company");
			result = "Company";
			return result;
		}
		// else if (isPerson(searchName)) {
		// result = "Person";
		// return result;
		// } else if (isStuff(searchName)) {
		// result = "Stuff";
		// return result;
		// } else if (isEvent(searchName)) {
		// result = "Event";
		// return result;
		// } else if (isPlace(searchName)) {
		// result = "Place";
		// return result;
		// }
		return result;
	}

	public static String getType(String searchName) {
		System.out.println("Get Type !!!!!" + searchName);
		String result = "";
		try {
			searchName = URLEncoder.encode(searchName, "UTF-8");
		} catch (UnsupportedEncodingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			System.err.print("URLEncoder error !!!");
		}
		Wiki wiki = new Wiki();
		WikiObject wo = null;
		try {
			wo = wiki.createWikiObject(searchName,
					"http://zh.wikipedia.org/w/api.php?action=parse&format=xml&page="
							+ searchName + "&redirects");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.err.print("wiki.createWikiObject Error !!");
		}
		if (isPeople(wo)) {
			result = "人名";
		} else if (isPlace(wo)) {
			result = "地名";
		} else if (isOrg(wo)) {
			result = "組織";
		}
		return result;
	}

	// 利用開放類別或資訊盒判斷是否為人 (Chris add)
	public static boolean isPeople(WikiObject wo) {
		String infoBox = wo.getInfoBoxSource();
		// System.out.println("InfoBox:" + infoBox);
		for (String s : wo.getOpenCategory()) {

			if (s.contains("出生") || s.contains("校友") || s.contains("教授")) {
				return true;
			}
		}

		return false;
	}

	// 利用開放類別或資訊盒判斷是否為地點 (Chris add)
	public static boolean isPlace(WikiObject wo) {
		ArrayList<String> notSet = new ArrayList<String>();
		notSet.add("組織");
		notSet.add("成立時間");

		for (String s : wo.getOpenCategory()) {
			// System.out.println("OpenCategory:" + s);
			if (s.contains("成員國") || s.contains("聯邦") || s.contains("島國")
					|| s.contains("行政區")) {
				return true;
			}
		}
		String infoBox = wo.getInfoBoxSource();
		// System.out.println(infoBox);

		// for(String key : wo.getSynonyms().keySet()){
		// System.out.println("Key:" + key + "  Value:" +
		// wo.getSynonyms().get(key));
		// }
		if (infoBox.contains("人口") || infoBox.contains("面積")
				|| infoBox.contains("行政區")) {
			for (String s : notSet) {
				if (infoBox.contains(s)) {
					return false;
				}
			}
			return true;
		}
		return false;
	}

	// 利用開放類別或資訊盒判斷是否為組織 (Chris add)
	public static boolean isOrg(WikiObject wo) {
		for (String s : wo.getOpenCategory()) {
			if (s.contains("組織") || s.contains("成立的公司") || s.contains("行政部門")
					|| s.contains("機關")) {
				return true;
			}
		}
		String infoBox = wo.getInfoBoxSource();
		if (infoBox.contains("總部") || infoBox.contains("成員")
				|| infoBox.contains("公司類型") || infoBox.contains("成立")) {
			return true;
		}
		return false;
	}

	// 偵測是否為人
	public static boolean isPerson(String peopleName) {
		boolean isPeople = false;
		try {
			// FB判斷
			// ||PeopleDetection.isPeopleByFaceBook(peopleName)
			if (PeopleDetection.isPeopleByWikiEng(peopleName)
					|| PeopleDetection.isPeopleByWiki(peopleName)
					|| PeopleDetection.isPeopleByFaceBook(peopleName)) {
				// 尋找英文維基百科 and FB
				isPeople = true;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			return isPeople;
		}
	}

	// 偵測是否為地方
	public static boolean isPlace(String searchKey) {
		boolean result = false;
		Wiki wiki = new Wiki();
		String html;
		try {
			// lu_map
			// 空白用+取代(facebook.com 李亞哲-->facebook.com+李亞哲)
			searchKey = searchKey.replace(" ", "+");
			html = FetchOperation.getHtmlFromURL(
					"http://maps.googleapis.com/maps/api/geocode/xml?address="
							+ searchKey + "&sensor=true", "UTF-8", false);
			if (html.contains("ZERO_RESULTS") == false) {
				result = true;
			}
		} catch (Exception e) {
		} finally {
			return result;
		}
	}

	// 是否為公司
	public static boolean isCompany(String companyName) {
		boolean isCompany = false;
		try {
			System.out.println("DO ISCOMPANY:" + companyName);
			if (CompanyDetection.isCompanyByWikipedia(companyName)
					|| CompanyDetection.isCompanyByWikipediaEng(companyName)) {
				// 尋找英文維基百科
				isCompany = true;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			return isCompany;
		}
	}

	// 是否為物品
	public static boolean isStuff(String stuffName) {
		boolean result = false;
		try {
			String html = FetchOperation.getDocumentFromURL(
					"http://search.buy.yahoo.com.tw/search.php?p=" + stuffName
							+ "&z=0&subno=0&from=fp&hpp=gdsearch&rs=0&catsel=",
					"UTF-8", false).html();
			Document doc = Jsoup.parse(html);
			Elements elements = doc
					.select(".brand.box_round li:has(a:has(span))");
			ArrayList<String> categoryList = new ArrayList<String>();
			for (Element e : elements) {
				String theCategory = e.text();
				if (theCategory.contains("書籍") == false) {
					categoryList.add(e.text());

				}
			}
			if (categoryList.size() != 0) {
				result = true;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	}

	// 是否為事件
	public static boolean isEvent(String eventName) {
		boolean isEvent = false;
		try {
			if (eventName.contains("案")) {
				isEvent = true;
			} else {
				Wiki wiki = new Wiki();
				WikiObject wo;
				wo = wiki.createWikiObject(eventName,
						"http://zh.wikipedia.org/w/api.php?action=parse&format=xml&page="
								+ eventName + "&redirects");
				if (wo != null) {
					String firstContent = wo.getFirstContent();
					if (firstContent.contains("[")
							|| firstContent.contains("]")) {
						isEvent = true;
					}
				}
			}
		} catch (Exception e) {

		} finally {
			return isEvent;
		}
	}

}
