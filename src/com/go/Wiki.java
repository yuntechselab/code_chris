package com.go;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.util.EntityUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.parser.Tag;
import org.jsoup.select.Elements;

//import com.sun.xml.internal.bind.v2.runtime.unmarshaller.XsiNilLoader.Array;
//import com.sun.xml.internal.ws.api.streaming.XMLStreamReaderFactory.Woodstox;

import tools.GeneralTools;
import tools.files.FileTools;
import entity.GoogleObject;
import entity.PatternObject;
import entity.WikiObject;

public class Wiki {
	/*
	 * wiki的一型多詞要解(縮寫詞判斷演算法)
	 */
	// 放wiki的Rule
	public String ruleSet = "D:\\workplace\\rockspace\\resource\\wikiRules.txt";

	public WikiObject createWikiObject(String searchKey, String URL)
			throws Exception {
		WikiObject wikiObject = new WikiObject(searchKey);
		HashMap<String, String> synonyms = wikiObject.getSynonyms();
		Document doc = FetchOperation.getDocumentFromURL(URL, "UTF-8", true);

		System.out.println("進createWikiObject");
		// 如果沒這一頁
		if (doc.select("error").size() != 0) {
			System.out.println("WIKI 查詢結果錯誤");
			// 不存在此頁面
			wikiObject.setExist(false);
			System.out.println("this");
			// 設定Reference
			ArrayList<GoogleObject> result = Google.getGoogleResults("wiki "
					+ searchKey);
			ArrayList<String> wSet = new ArrayList<String>();
			for (GoogleObject go : result) {
				String webSiteName = go.getWebsiteName();
				if (webSiteName.contains("维基百科")
						|| webSiteName.contains("encyclopedia")
						|| webSiteName.contains("Wiki")) {
					webSiteName = webSiteName.split("- ")[0];
					wSet.add(webSiteName);
				}
			}

			wikiObject.setReferences(wSet);

			return wikiObject;
		}
		// System.out.println("this");
		// 設定原始碼
		String htmlContent = doc.html();
		wikiObject.setSourceCode(htmlContent);

		// 抓開放類別 並判斷是否為一形多詞
		Elements categoryElements = doc.select("categories cl");// categories
		ArrayList<String> openCategory = wikiObject.getOpenCategory();

		// 是否Polysemy
		for (Element li : categoryElements) {
			// System.out.println(li.text());
			// 神奇的文字出現
			if (li.text().contains("引文格式")) {
				// 不包含hidden的就存 WIKI最下面
			} else if (!li.toString().contains("hidden")) {
				openCategory.add(li.text());
				// System.out.println(li.text());
				if (li.text().contains("消歧義")) {
					wikiObject.setPolysemy(true);
				}
			}
		}

		wikiObject.setOpenCategory(openCategory);

		// 設定標題(有時搜尋字和標題不一樣)
		String firstHead = doc.select("parse").attr("title");
		wikiObject.setFirstHeading(firstHead);
		// 若搜尋字和head一樣 視為同義字
		if (searchKey.equals(firstHead) == false) {
			synonyms.put(firstHead, "FirstHead");
			wikiObject.setSynonyms(synonyms);
		}
		Elements theContent = Jsoup.parse(doc.select("text").get(0).text())
				.select("p");
		Elements theContentHTML = Jsoup.parse(doc.select("text").get(0).text())
				.select("p");

		// 萃取出維基百科中的每一段落
		ArrayList<String> eachParagraph = wikiObject.getParagraphs();
		Elements paragraphsHTML = wikiObject.getParagraphsHTML();
		for (int i = 0; i < theContent.size(); i++) {
			eachParagraph.add(theContent.get(i).text());
		}

		for (Element element : theContentHTML) {
			paragraphsHTML.add(element);
		}
		// 儲存全部段落到物件中
		wikiObject.setParagraphs(eachParagraph);
		wikiObject.setParagraphsHTML(paragraphsHTML);
		boolean hasFirstContent = false;
		// 取出第一段(純文字)(包含wikiHead的字視為第一段)
		for (String paraGraph : eachParagraph) {
			if (paraGraph.contains(firstHead)) {
				wikiObject.setFirstContent(paraGraph);
				hasFirstContent = true;
				break;
			}
		}
		// 取出第一段(HTML)
		for (Element e : paragraphsHTML) {
			if (e.text().contains(firstHead)) {
				wikiObject.setFirstContentHTML(e);
				hasFirstContent = true;
				break;
			}
		}
		// 如果查詢字都沒出現在任何一段的時候，視"第一段"為"第一段"
		if (hasFirstContent == false) {
			wikiObject.setFirstContent(eachParagraph.get(0));
			wikiObject.setFirstContentHTML(paragraphsHTML.get(0));
		}
		// 如果是消歧義(一型多詞)
		if (wikiObject.isPolysemy()) {
			// 抓所有，之前的超連結
			HashSet<String> polysemyList = new HashSet<String>();

			Elements liElement = Jsoup.parse(doc.select("text").get(0).text())
					.select("ul li");
			for (Element li : liElement) {
				// 取逗號之前的超連結
				if (li.text().contains("，")) {
					Document d = Jsoup.parse(li.html().split("，")[0]);
					String theAnswer = d.select("a").attr("title");
					theAnswer = theAnswer.replace("（頁面不存在）", "");
					theAnswer = theAnswer.replace(" ", "_");
					polysemyList.add(theAnswer);
				} else {
					String theAnswer = li.select("a").attr("title");
					theAnswer = theAnswer.replace("（頁面不存在）", "");
					theAnswer = theAnswer.replace(" ", "_");
					polysemyList.add(theAnswer);
				}
			}

			wikiObject.setPolysemySet(polysemyList);
		}
		// 不是消歧義，針對內容執行萃取演算法
		else {
			// (針對第一段)執行rule配對
			HashMap<String, String> contentSynonyms = retrieveSynoynyms(
					wikiObject.getFirstContent(), wikiObject.getFirstHeading());
			// (針對第一段)抓取抓取粗體字
			HashMap<String, String> boldSynonyms = boldAlgorithm(wikiObject
					.getFirstContentHTML());
			// (針對右邊欄位)
			HashMap<String, String> infoBoxSynonyms = retrieveInfoBox(wikiObject
					.getSourceCode());

			// 抓英文語言的同義字
			HashMap<String, String> langSynonyms = retrieveLanguage(wikiObject
					.getSourceCode());
			HashMap<String, String> tempHashMap = wikiObject.getSynonyms();
			tempHashMap.putAll(contentSynonyms);
			tempHashMap.putAll(boldSynonyms);
			tempHashMap.putAll(infoBoxSynonyms);
			tempHashMap.putAll(langSynonyms);
			// 過濾同義字(如：[1] [2]（台灣紅不讓時期）) 尚未完成
			removePunctuation(tempHashMap);

			// 若searchKey有出現數字的話，代表是一型多詞，執行commaAlgo
			if (checkContainFigures(wikiObject.getFirstHeading())
					|| checkContainFigures(searchKey)) {
				// wiki表頭執行commaAlgo
				HashMap<String, String> temp = commaAlgo(
						wikiObject.getFirstHeading(),
						wikiObject.getParagraphsHTML());
				tempHashMap.putAll(temp);

				// wiki搜尋字
				temp = commaAlgo(searchKey, wikiObject.getParagraphsHTML());
				tempHashMap.putAll(temp);
			}
			// 如果 同義字裡面有出現searchKey則刪除
			tempHashMap.remove(wikiObject.getSearchKey());

			// 過濾標點符號
			HashMap<String, String> clearMap = new HashMap<String, String>();
			for (String key : tempHashMap.keySet()) {
				String value = "";
				value = tempHashMap.get(key);
				if (key.contains("[")) {
					key = key.substring(0, key.indexOf("["));
					value = "";
				} else if (key.contains("（")) {
					key = key.substring(0, key.indexOf("（"));
				} else if (key.contains("(")) {
					key = key.substring(0, key.indexOf("("));
				} else if (key.contains("/")) {
					key = key.substring(0, key.indexOf("/"));
				} else if (key.contains("'")) {
					key = key.substring(0, key.indexOf("'"));
					// key=key.replace("'", " ");
				}
				clearMap.put(key, value);
			}
			// for(String s : clearMap.keySet()){
			// System.out.println("!!Key:" + s +" Value:" + clearMap.get(s));
			// }
			wikiObject.setSynonyms(clearMap);

			// 設定infobox
			Elements infoBox = Jsoup.parse(doc.select("text").get(0).text())
					.select("table.infobox");
			if (infoBox.size() != 0) {
				wikiObject.setInfoBoxSource(infoBox.html());
			}
		}
		return wikiObject;
	}

	// 抓粗體字視為同義字
	public HashMap<String, String> boldAlgorithm(Element contentElements) {
		HashMap<String, String> result = new HashMap<String, String>();
		for (Element e : contentElements.select("b")) {
			result.put(e.text(), "boldAlgorithm");
		}
		return result;
	}

	// 根據Rule萃取出內文中的同義字
	public HashMap<String, String> retrieveSynoynyms(
			String completeWikiContent, String searchKey) throws Exception {
		HashMap<String, String> result = new HashMap<String, String>();
		String[] patternSet = FileTools.readFile(ruleSet).split("\r\n");
		for (String pattern : patternSet) {
			String wikiContent = completeWikiContent;
			pattern = pattern.replace("SearchKey", searchKey);
			ArrayList<PatternObject> starRange = getRangeFromText(pattern,
					"[*]");
			ArrayList<PatternObject> answerRange = getRangeFromText(pattern,
					"Answer");

			ArrayList<PatternObject> patternList = new ArrayList<PatternObject>();
			for (PatternObject patternObject : starRange) {
				patternList.add(patternObject);
			}

			for (PatternObject patternObject : answerRange) {
				patternList.add(patternObject);
			}

			Collections.sort(patternList, new Comparator() {
				public int compare(Object o1, Object o2) {
					PatternObject p1 = (PatternObject) o1;
					PatternObject p2 = (PatternObject) o2;
					Integer p1Position = p1.getLeftRange();
					Integer p2Position = p2.getRightRange();
					return p1Position.compareTo(p2Position);
				}
			});
			// 符號的區間會比符號數量多1(國小數學有教)
			ArrayList<String> gapList = findAllGapString(pattern, patternList);
			for (int i = 0; i < patternList.size(); i++) {
				try {
					// System.out.println(i);
					String patternName = patternList.get(i).getPatternName();
					String leftString = gapList.get(i);
					String rightString = gapList.get(i + 1);
					// System.out.println(leftString + "" + patternName + "" +
					// rightString);

					String[] leftArray = wikiContent.split(leftString);
					String[] rightArray = wikiContent.split(rightString);
					// Rule和Wiki內容都無法配對成功，左邊或右邊其中一個沒找到
					if (leftArray.length == 0 || rightArray.length == 0) {
						break;
					}

					// 最後一個符號
					else if (i + 1 == patternList.size()) {
						// System.out.println("wikiContent"+wikiContent);
						// System.out.println("left"+leftString);
						// System.out.println("right"+rightString);
						String theAnswer = "";
						theAnswer = wikiContent.substring(leftString.length());

						// System.out.println(rightString);
						theAnswer = theAnswer.split(rightString)[0];

						if (patternName.equals("Answer")) {
							// System.out.println("theAnswer->"+theAnswer);
							result.put(theAnswer, "Rule");
						}
					}

					// 左右邊都有
					else {
						String theAnswer = "";
						int theLeftIndex = wikiContent.indexOf(leftString);
						theLeftIndex += leftString.length();
						int theRightIndex = wikiContent.indexOf(rightString);

						// 左右兩邊的符號一模一樣
						if (leftString.equals(rightString)) {
							// System.out.println("一樣");
							// 看看wiki字串中是否有兩個以上的這個符號
							ArrayList<Integer> theDoubleArray = countTextDouble(
									wikiContent, leftString);
							if (theDoubleArray.size() >= 2) {
								// System.out.println("可以繼續做");
								// System.out.println(theDoubleArray);
								theRightIndex = theDoubleArray.get(1);

								// System.out.println(theLeftIndex + "~" +
								// theRightIndex);
							}
							// break;
						}

						theAnswer = wikiContent.substring(theLeftIndex,
								theRightIndex);

						if (patternName.equals("Answer")) {
							result.put(theAnswer, "Rule");
						}
						// System.out.println("theAnswer->" + theAnswer);

						wikiContent = wikiContent.substring(theRightIndex);
						// System.out.println("wikiContent剩下\t" + wikiContent);
					}
				} catch (Exception e) {
					// System.out.println("rule不OK("+pattern+")");
					// TODO: handle exception
				}
			}
		}
		// 過濾掉標點符號
		result = filterPunctuation(result);
		return result;
	}

	// 過濾符號、和searchKey同樣的字眼
	public HashMap<String, String> filterPunctuation(
			HashMap<String, String> synonyms) {
		HashMap<String, String> result = new HashMap<String, String>();
		ArrayList<String> punctuationList = new ArrayList<String>();
		punctuationList.add("(");
		punctuationList.add(")");
		punctuationList.add(".");
		punctuationList.add(",");
		punctuationList.add("，");
		punctuationList.add("」");
		punctuationList.add("）");
		punctuationList.add("（");
		punctuationList.add("、");
		punctuationList.add("。");
		punctuationList.add("：");
		punctuationList.add("[");
		punctuationList.add("/");
		punctuationList.add("]");

		// 跑所有的結果，驗證它們是否有標點符號、有不合理之處
		for (String theSynonym : synonyms.keySet()) {
			// 判斷有無標點符號
			boolean isPunctuation = false;
			for (String thePunctuation : punctuationList) {
				// 只要有其中一個標點符號，就不考慮這個同義字
				if (theSynonym.contains(thePunctuation)) {
					isPunctuation = true;
					break;
				}
			}
			// 如果這個同義字長度是0，那麼不加入喔
			if (theSynonym.length() == 0) {
				isPunctuation = true;
			}
			if (isPunctuation == false) {
				result.put(theSynonym, synonyms.get(theSynonym));
			}
		}

		return result;
	}

	// 找出所有所有的區間(*和Answer一起判斷)
	public ArrayList<String> findAllGapString(String pattern,
			ArrayList<PatternObject> patternList) {
		ArrayList<String> result = new ArrayList<String>();
		// 取得所有的區間
		for (int i = 0; i < patternList.size(); i++) {
			PatternObject patternObject = patternList.get(i);
			int beginIndex = patternObject.getLeftRange();
			String patternName = patternObject.getPatternName();
			// System.out.println("第" + (i + 1) + "個");
			if (i == 0) {
				// 如果第一個*或是Answer在pattern字串的第一位置
				if (beginIndex == 0) {
					// System.out.println("第一個符號在開頭的時候");
					// 抓0~下一個*或是Answer的leftRange

					// 後面沒有了
					if (patternList.size() == 1) {
						int thePatternNameLength = patternName.length();
						if (patternName.equals("[*]")) {
							thePatternNameLength = 1;
						}
						String theAnswer = pattern
								.substring(thePatternNameLength);
						result.add(theAnswer);
						// System.out.println("1The Answer->" + theAnswer);
					}
					// 有下一個符號
					else {
						String theAnswer = "";
						int thePatternNameLength = patternName.length();
						if (patternName.equals("[*]")) {
							thePatternNameLength = 1;
						}

						pattern = pattern.substring(thePatternNameLength);
						String theNextName = patternList.get(i + 1)
								.getPatternName();
						theAnswer = pattern.split(theNextName)[0];
						result.add(theAnswer);
						// System.out.println("2The Answer->" + theAnswer);
						// System.out.println("pattern剩下：" + pattern);
					}
				}
				// 如果第一個*或是Answer不在pattern字串的第一位置
				else {
					String theAnswer = "";
					theAnswer = pattern.split(patternName)[0];
					// System.out.println("3The Answer->" + theAnswer);
					result.add(theAnswer);
					int thePatternNameLength = patternName.length();
					if (patternName.equals("[*]")) {
						thePatternNameLength = 1;
					}
					pattern = pattern.substring(theAnswer.length()
							+ thePatternNameLength);
					// System.out.println("pattern剩下" + pattern);
				}
			}
			// 將處理除了第一個符號和最後一個符號
			else if (i != patternList.size()) {
				String theAnswer = "";
				theAnswer = pattern.split(patternName)[0];
				// System.out.println("4The Answer->" + theAnswer);
				result.add(theAnswer);
				int thePatternNameLength = patternName.length();
				if (patternName.equals("[*]")) {
					thePatternNameLength = 1;
				}

				pattern = pattern.substring(theAnswer.length()
						+ thePatternNameLength);
				// System.out.println("pattern剩下：" + pattern);
			} else {
				// System.out.println("最後了喔!!");
			}

			// 如果pattern沒有了就break;
			if (pattern.length() == 0) {
				break;
			}
			// 如果pattern都沒有Answer或是*時，回傳剩下的字串即為答案
			if (!pattern.contains("Answer") && !pattern.contains("*")) {
				// System.out.println("5The Answer->" + pattern);
				result.add(pattern);
			}
		}
		return result;
	}

	// 取得這個字在這個pattern的所有範圍
	public ArrayList<PatternObject> getRangeFromText(String inputStr,
			String patternStr) {
		ArrayList<PatternObject> result = new ArrayList<PatternObject>();
		Pattern pattern = Pattern.compile(patternStr);
		Matcher matcher = pattern.matcher(inputStr);
		boolean matchFound = matcher.find();
		while (matchFound) {
			PatternObject patternObject = new PatternObject(patternStr);
			patternObject.setLeftRange(matcher.start());
			patternObject.setRightRange(matcher.end());
			result.add(patternObject);
			if (matcher.end() + 1 <= inputStr.length()) {
				matchFound = matcher.find(matcher.end());
			} else {
				break;
			}
		}
		return result;
	}

	// 計算重複字串的數量
	public ArrayList<Integer> countTextDouble(String inputStr, String patternStr) {
		ArrayList<Integer> result = new ArrayList<Integer>();
		Pattern pattern = Pattern.compile(patternStr);
		Matcher matcher = pattern.matcher(inputStr);
		boolean matchFound = matcher.find();
		while (matchFound) {
			int theDoublePosition = matcher.start();
			result.add(theDoublePosition);
			for (int i = 0; i <= matcher.groupCount(); i++) {
				String groupStr = matcher.group(i);
				// System.out.println(i + ":" + groupStr);
			}
			if (matcher.end() + 1 <= inputStr.length()) {
				matchFound = matcher.find(matcher.end());
			} else {
				break;
			}
		}
		return result;
	}

	// 一型多詞演算法(ex:三軍、四中、文房四寶、六人行、香港四大天王)
	public HashMap<String, String> commaAlgo(String searchKey,
			Elements paragraphs) {
		HashMap<String, String> result = new HashMap<String, String>();
		// 中文轉數字
		ArrayList<Integer> commaNeed = chineseToNumber(searchKey);
		// 執行每一段
		for (Element p : paragraphs) {
			// 將句號和逗號拆開 視為一個段落
			System.out.println("yoho~~~~");
			String[] sentencesA = p.html().split(
					"[\\。\\，\\：\\“\\”\\或\\的\\等\\指\\為\\即]");
			List<String> sentences = new ArrayList<String>();
			sentences = (List<String>) Arrays.asList(sentencesA);

			for (String eachSentence : sentences) {
				Document doc = Jsoup.parse(eachSentence);
				String[] comma = doc.html().split("[\\、\\和\\或]");
				if (commaNeed.contains(comma.length)) {
					// <長度，次數>
					HashMap<Integer, Integer> lengthCount = new HashMap<Integer, Integer>();
					String s = "";
					// 計算眾數
					for (int i = 1; i < comma.length - 1; i++) {
						Document d = Jsoup.parse(comma[i]);
						// 取代空白
						String dealWith = d.text().replaceAll(" ", "");
						if (lengthCount.get(dealWith.length()) == null) {
							lengthCount.put(dealWith.length(), 1);
						} else {
							lengthCount.put(dealWith.length(),
									lengthCount.get(dealWith.length()) + 1);
						}
					}
					// 次數
					int theMaxCount = 0;
					// 適當長度
					int theMaxLength = 0;
					// 把最大的數值抓出來
					for (int v : lengthCount.values()) {
						if (v > theMaxCount) {
							theMaxCount = v;
						}
					}
					for (int v : lengthCount.keySet()) {
						if (lengthCount.get(v) == theMaxCount) {
							theMaxLength = v;
						}
					}
					for (int i = 0; i < comma.length; i++) {
						Document d = Jsoup.parse(comma[i]);
						// 取代空白
						String dealWith = d.text().replaceAll(" ", "");

						if (i == 0 && dealWith.length() > theMaxLength) {
							s += dealWith.substring(dealWith.length()
									- theMaxLength, dealWith.length());
						}
						// 最後一個字串
						else if ((i + 1) == comma.length
								&& dealWith.length() > theMaxLength) {
							s += dealWith.substring(0, theMaxLength);
						} else {
							s += dealWith;
						}

						// 還原、
						if ((i + 1) != comma.length) {
							s += "、";
						}
					}
					result.put(s, "commaAlgo");
				}
			}
		}
		return result;
	}

	public ArrayList<Integer> chineseToNumber(String chineseTxt) {
		ArrayList<Integer> result = new ArrayList<Integer>();
		String[] numberSet = { "一", "二", "三", "四", "五", "六", "七", "八", "九", "十" };
		for (int i = 0; i < numberSet.length; i++) {
			if (chineseTxt.contains(numberSet[i])) {
				result.add(i + 1);
			}
		}
		return result;
	}

	// 判斷中文字有沒有出現數字
	public boolean checkContainFigures(String chineseTxt) {
		String[] numberSet = { "一", "二", "三", "四", "五", "六", "七", "八", "九", "十" };
		for (int i = 0; i < numberSet.length; i++) {
			if (chineseTxt.contains(numberSet[i])) {
				return true;
			}
		}
		return false;
	}

	// 取得畫面右邊表格的資料
	public HashMap<String, String> retrieveInfoBox(String wikiContent) {
		HashMap<String, String> result = new HashMap<String, String>();
		// key入要抓取的欄位
		ArrayList<String> filiterString = new ArrayList<String>();
		filiterString.add("本名");
		filiterString.add("昵称");
		filiterString.add("暱稱");
		filiterString.add("英文名");
		filiterString.add("羅馬拼音");
		filiterString.add("綽號");
		filiterString.add("別名");
		filiterString.add("現任成員");
		filiterString.add("球隊歷史");
		filiterString.add("隊名");
		filiterString.add("籍貫");
		Document doc = Jsoup.parse(wikiContent);
		Elements table = Jsoup.parse(doc.select("text").get(0).text()).select(
				"table.infobox");
		Elements eachRow = table.select("tr");
		for (int i = 0; i < eachRow.size(); i++) {
			Element e = eachRow.get(i);
			String impliedData = e.text();
			// System.out.println("DO e.text: "+ impliedData);
			for (String f : filiterString) {
				// 有出現上面列表
				if (impliedData.contains(f)) {
					// 切割空白判斷右邊表格的格式
					String[] eachField = impliedData.split(" ");

					// 左右兩欄格式
					if (eachField.length > 1) {
						// 又是球隊歷史的欄位
						if (f.equals("球隊歷史")) {
							Elements bSet = e.select("b");
							for (Element b : bSet) {
								if (!b.text().equals("球隊歷史")) {
									result.put(b.text(), "InfoBox");
								}
							}
						} else {
							// 把後面全部接起來
							StringBuilder sb = new StringBuilder();
							for (int j = 1; j < eachField.length; j++) {
								sb.append(eachField[j]);
								if (j + 1 != eachField.length) {
									sb.append(" ");
								}
							}

							// F4(言程序、仔仔...)頓號要保留
							if (f.equals("現任成員")) {
								result.put(sb.toString(), "InfoBox");
							}
							// 把A、B拆開來 分別存入 A和B
							else {
								if (sb.toString().contains("、")) {
									String s = sb.toString();
									String[] ss = s.split("、");
									if (ss.length > 1) {
										for (int j = 0; j < ss.length; j++) {
											result.put(ss[j], "InfoBox");
										}
									}
								}
								// 沒包含頓號、 直接加入
								else {
									result.put(sb.toString(), "InfoBox");
								}
							}
						}

					}
					// 上下兩列
					else {
						if (f.equals("隊名")) {
							StringBuilder sb = new StringBuilder();
							sb.append(eachRow.get(i + 1).text());
							Elements eachTeam = eachRow.get(i + 2).select("li");
							for (Element team : eachTeam) {
								sb.append(team.text());
							}

							result.put(sb.toString(), "InfoBox");
						} else {
							result.put(eachRow.get(i + 1).text(), "InfoBox");
						}
					}
				}
			}
		}
		for (String s : result.keySet()) {
			// System.out.println("Key:" + s + " Value:" + result.get(s));
		}
		return result;
	}

	// 判斷維基百科中有沒有這個頁面
	public boolean isPage(String searchKey, boolean isEnglish) {
		boolean result = false;
		try {
			String wikiLanguge = "zh";
			if (isEnglish) {
				wikiLanguge = "en";
			}
			String url = "http://" + wikiLanguge + ".wikipedia.org/wiki/"
					+ searchKey;
			Document d = FetchOperation.getDocumentFromURL(url, "UTF-8", true);
			String thePage = d.text();
			if (thePage.contains("維基百科目前還沒有與上述標題相同的條目") == false
					&& thePage
							.contains("Wikipedia does not have an article with this exact name") == false) {
				return true;
			}
		} catch (Exception e) {
			return false;
		}
		return result;
	}

	// 取得左邊英文欄位
	public HashMap<String, String> retrieveLanguage(String wikiContent) {
		HashMap<String, String> result = new HashMap<String, String>();
		Document doc = Jsoup.parse(wikiContent);
		Elements langElements = doc.select("langlinks ll");
		for (Element a : langElements) {
			String language = a.attr("lang");
			if (language.equals("en")) {
				result.put(a.text(), "en");
			}
		}
		return result;
	}

	// Beta:移除標點符號(如：[1] [2]（台灣紅不讓時期）) 未完成
	public void removePunctuation(HashMap<String, String> synonyms) {
		for (String s : synonyms.keySet()) {
		}
	}
}
