package com.go;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import tools.files.LoadProps;
import databaseTools.OurSqlConnection;
import entity.GoogleObject;

public class Google {
	static OurSqlConnection conn = new OurSqlConnection(
			LoadProps.getProperties("databaseName"));

	// 取得google的搜尋結果
	public static ArrayList<GoogleObject> getGoogleResults(String searchKey) {
		ArrayList<GoogleObject> result = new ArrayList<GoogleObject>();
		String html;
		try {
			// 空白用+取代(facebook.com 李亞哲-->facebook.com+李亞哲)
			searchKey = searchKey.replace(" ", "+");
			String url = "https://www.google.com.tw/search?hl=zh-TW&q="
					+ searchKey;
			// DB內有暫存資料
			if (isDB(url)) {
				System.out.println("有暫存");
				String sql = "";
				sql = "select * from  htmlTable where url='" + url + "'";
				OurSqlConnection conn = new OurSqlConnection(
						LoadProps.getProperties("databaseName"));
				conn.setSql(sql);
				ResultSet rs = conn.getResultSet();
				rs.next();
				String htmlDB = rs.getString("html");
				conn.close();
				Document doc = Jsoup.parse(htmlDB);
				Elements searchElements = doc.select("#ires h3");
				for (int i = 0; i < searchElements.size(); i++) {
					GoogleObject go = new GoogleObject();
					String websiteName = searchElements.get(i).text();
					go.setWebsiteName(websiteName);
					result.add(go);
				}
			} else {
				System.out.println("沒有暫存");
				html = FetchOperation.getHtmlFromURL(url, "UTF-8", false);
				// 將結果存進去資料庫
				// String connStr =
				// "jdbc:mysql://140.125.84.58/103wikiproject?useUnicode=true&characterEncoding=utf-8&user=root&password=selab";
				String tableName = LoadProps.getProperties("databaseName");
				String connStr = LoadProps.getProperties("url") + tableName
						+ "?useUnicode=true&characterEncoding=utf-8&user="
						+ LoadProps.getProperties("user") + "&password="
						+ LoadProps.getProperties("password");
				Connection conn2 = DriverManager.getConnection(connStr);
				String sql = "insert into htmlTable(url,html,time) values(?,?,?)";
				PreparedStatement sss = conn2.prepareStatement(sql);
				sss.setString(1, url);
				sss.setString(2, html);
				sss.setLong(3, Calendar.getInstance().getTimeInMillis());
				sss.executeUpdate();
				Document doc = Jsoup.parse(html);

				Elements searchElements = doc.select("#ires h3");
				for (int i = 0; i < searchElements.size(); i++) {
					GoogleObject go = new GoogleObject();
					String websiteName = searchElements.get(i).text();
					go.setWebsiteName(websiteName);
					result.add(go);
				}
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {

			return result;
		}
	}

	// 取得googleMap的搜尋結果
	public static boolean getGoogleMapResults(String searchKey) {
		boolean result = false;
		Wiki wiki = new Wiki();
		String html;
		try {
			// lu_map
			// 空白用+取代(facebook.com 李亞哲-->facebook.com+李亞哲)
			searchKey = searchKey.replace(" ", "+");
			html = FetchOperation.getDocumentFromURL(
					"http://maps.googleapis.com/maps/api/geocode/xml?address="
							+ searchKey + "&sensor=true", "UTF-8", false)
					.html();
			if (html.contains("ZERO_RESULTS") == false) {
				result = true;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			return result;
		}
	}

	public static boolean isDB(String url) {
		String sql = "Select count(*) from htmlTable where url='" + url + "'";
		OurSqlConnection conn = new OurSqlConnection(
				LoadProps.getProperties("databaseName"));
		conn.setSql(sql);
		ResultSet rs = conn.getResultSet();
		int dataCount;
		try {
			rs.next();
			dataCount = rs.getInt("count(*)");
			if (dataCount == 0) {
				return false;
			} else {
				return true;
			}
		} catch (SQLException e) {
			System.out.println("?");
			return false;
		} finally {
			conn.close();
		}
	}
}
