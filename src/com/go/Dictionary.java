package com.go;

import java.util.HashMap;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import com.memetix.mst.language.Language;
import com.memetix.mst.translate.Translate;

import entity.WikiObject;

public class Dictionary {
    // http://www.stars21.com/translator/english_to_chinese.html

    // 從Google翻譯抓取中文名
    public static String getChineseByGoogle(String engName) {
	String result = "";
	try {
	    result = TranslateUtil.en2tw(engName);
	} catch (Exception e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}
	return result;
    }

    // 從Microsoft翻譯
    public static String getChineseByMicrosoft(String engName) {
	String result = "";
	try {
	    // 帳號資訊登入後，在https://datamarket.azure.com/account可以看到。
	    String clientID = "mymislife";
	    String clientSecret = "cAbRol9ePn2FcMUp+T677T1GyZW783/MXs2t6B6WwFM=";

	    Translate.setClientId(clientID);
	    Translate.setClientSecret(clientSecret);

	    String translatedText = Translate.execute(engName, Language.ENGLISH, Language.CHINESE_TRADITIONAL);
	    result = translatedText;
	} catch (Exception e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}
	return result;
    }

    // 從Baidu翻譯
    public static String getChineseByBaidu(String engName) {
	String result = "";
	try {
	    engName = engName.replace(" ", "+");
	    String url = "http://baike.baidu.com/search/word?word=" + engName + "&pic=1&sug=1&enc=utf8&oq=" + engName;
	    String html = FetchOperation.getDocumentFromURL(url, "gb2312", true).html();
	    Document doc = Jsoup.parse(html);
	    result = doc.select("title").text().replace("_百度百科", "");
	} catch (Exception e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}
	return result;
    }

    // 從維基百科抓取中文名
    public static HashMap<String, String> getChineseByWiki(String englishName) {
	HashMap<String, String> result = new HashMap<String, String>();
	try {
	    Wiki wiki = new Wiki();
	    englishName = englishName.replace(" ", "%20");
	    String html = FetchOperation.getDocumentFromURL("http://en.wikipedia.org/wiki/" + englishName, "UTF-8", true).html();
	    Document doc = Jsoup.parse(html);
	    // 有台灣wiki版面
	    Elements twElements = doc.select(".interwiki-zh > a");
	    if (twElements.size() != 0) {
		String theAnswer = twElements.attr("title");
		// 英文翻中文
		result.put(theAnswer, "enWiki");
		String hyperLink = "http:" + twElements.attr("href");
		WikiObject wikiObject = wiki.createWikiObject(theAnswer, hyperLink);

		// 把右邊欄位、粗體的同義字抓出來
		HashMap<String, String> synonyms = wikiObject.getSynonyms();
		for (String s : synonyms.keySet()) {
		    String theType = synonyms.get(s);
		    // 中文頁面加入
		    if (theType.equals("boldAlgorithm") || theType.equals("InfoBox")) {
			result.put(s, "zhWiki");
		    }
		}
	    }
	} catch (Exception e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	} finally {
	    return result;
	}
    }
}
