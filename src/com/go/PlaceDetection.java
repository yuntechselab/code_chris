package com.go;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import entity.WikiObject;

public class PlaceDetection {
    public static boolean isPlaceByWiki(WikiObject wo,String peopleName) {
	boolean isPlace = false;
	try {
	    peopleName = peopleName.replace(" ", "_");
	    // By開放類別
	    for (String s : wo.getOpenCategory()) {
		if (s.contains("機場") || s.contains("河") || s.contains("島") || s.contains("地") || s.contains("洲") || s.contains("州")) {
		    isPlace = true;
		}
	    }
	    // ByInfobox
	    String infoboxHTML = wo.getInfoBoxSource();
	    Elements bElements = Jsoup.parse(infoboxHTML).select("b");
	    for(Element e: bElements){		
		String s = e.text();
		if (s.contains("座標") || s.contains("坐標") || s.contains("城") || s.contains("人口") || s.contains("面積") || s.contains("河")) {
		    isPlace = true;
		}
	    }
	} catch (Exception e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	} finally {
	    return isPlace;
	}
    }
}
