package com.go;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Ngram {
    // 丟文字，跑gram
    public ArrayList<String> nGram(String txt, int n) {
	ArrayList<String> result = new ArrayList<String>();
	// n要大於1
	if (n >= 1) {
	    // 若n==1，從頭跑到尾
	    if (n == 1) {
		for (int j = 0; j < txt.length(); j++) {
		    Character theHead = txt.charAt(j);
		    result.add(theHead.toString());
		}
	    }
	    // n>1
	    else {
		// 跑2~長度gram
		for (int j = 0; j < txt.length(); j++) {
		    // 怕超出界外
		    if (j + n <= txt.length()) {
			StringBuilder sb = new StringBuilder();
			sb.append(txt.charAt(j));
			for (int k = 1; k < n; k++) {
			    Character succeedingWord = txt.charAt(j + k);
			    sb.append(succeedingWord);
			}
			result.add(sb.toString());
		    }
		}

	    }
	}
	return result;
    }

    // 取得中文的標點符號陣列
    public ArrayList<String> getPunctuationList(String s) {
	ArrayList<String> result = new ArrayList<String>();
	result.add("，");
	result.add("。");
	result.add("(");
	result.add(")");
	result.add("「");
	result.add("」");

	result.add("（");
	result.add("）");
	result.add("、");
	for (int i = 1; i <= 9; i++) {
	    result.add(new Integer(i).toString());
	}
	return result;
    }
}
