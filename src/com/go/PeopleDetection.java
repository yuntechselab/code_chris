package com.go;
import java.util.ArrayList;

import entity.GoogleObject;
import entity.WikiObject;

public class PeopleDetection {
    public static boolean isPeopleByFaceBook(String peopleName) {
	boolean result = false;
	// 取得google的搜尋結果
	String searchKey = "facebook " + peopleName;
	ArrayList<GoogleObject> googleResult = Google.getGoogleResults(searchKey);
	for (GoogleObject go : googleResult) {
	    String s = go.getWebsiteName();
	    if (s.contains("Facebook")) {
		peopleName = peopleName.replace(" ", "");
		s = s.replace(" ", "");
		s = s.replace("|Facebook", "");
		// | Facebook
		if (s.equals(peopleName + "的個人檔案") || s.equals(peopleName)) {
		    result = true;
		}
	    }
	}
	return result;
    }
    
    public static boolean isPeopleByWiki(String peopleName) {
	boolean isPeople = false;
	Wiki wiki = new Wiki();
	WikiObject wo;
	try {
	    peopleName = peopleName.replace(" ", "_");
	    wo = wiki.createWikiObject(peopleName, "http://zh.wikipedia.org/w/api.php?action=parse&format=xml&page=" + peopleName + "&redirects");
	    System.out.println("do people\n\n\n\n\n");
	    for (String s : wo.getOpenCategory()) {
	    	System.out.println(s);
		if (s.contains("出生") || s.contains("校友") || s.contains("教授")) {
		
		    isPeople = true;
		}
	    }
	} catch (Exception e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}
	finally{
	    return isPeople;    
	}
    }
    
    public static boolean isPeopleByWikiEng(String peopleName) {
	boolean isPeople = false;
	Wiki wiki = new Wiki();
	WikiObject wo;
	try {
	    peopleName = peopleName.replace(" ", "_");
	    wo = wiki.createWikiObject(peopleName, "http://en.wikipedia.org/w/api.php?action=parse&format=xml&page=" + peopleName + "&redirects");
	    for (String s : wo.getOpenCategory()) {
		if (s.contains("births") || s.contains("alumni") || s.contains("living")|| s.contains("professor")) {
		    isPeople = true;
		}
	    }
	} catch (Exception e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}
	finally{
	    return isPeople;    
	}
    }
}
