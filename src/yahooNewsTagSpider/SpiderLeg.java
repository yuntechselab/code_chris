package yahooNewsTagSpider;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import database.DbInvoker;
import entity.News;
import static java.nio.charset.StandardCharsets.*;
public class SpiderLeg {
	// We'll use a fake USER_AGENT so the web server thinks the robot is a
	// normal web browser.
	private static final String USER_AGENT = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/535.1 (KHTML, like Gecko) Chrome/13.0.782.112 Safari/535.1";
	private List<String> links = new LinkedList<String>();
	private Document htmlDocument;
private String _url;
	/**
	 * This performs all the work. It makes an HTTP request, checks the
	 * response, and then gathers up all the links on the page. Perform a
	 * searchForWord after the successful crawl
	 * 
	 * @param url
	 *            - The URL to visit
	 * @return whether or not the crawl was successful
	 */

	public boolean crawl(String url) {
		try {
			_url = url;
			Connection connection = Jsoup.connect(url).userAgent(USER_AGENT);
			Document htmlDocument = connection.get();
			this.htmlDocument = htmlDocument;
			if (connection.response().statusCode() == 200) // 200 is the HTTP OK
															// status code
															// indicating that
															// everything is
															// great.
			{
				System.out
						.println("\n**Visiting** Received web page at " + url);
			}
			if (!connection.response().contentType().contains("text/html")) {
				System.out
						.println("**Failure** Retrieved something other than HTML");
				return false;
			}
//			Elements linksOnPage = htmlDocument.select("a[href]");
//			System.out.println("Found (" + linksOnPage.size() + ") links");
//			for (Element link : linksOnPage) {
//				this.links.add(link.absUrl("href"));
//			}
			return true;
		} catch (IOException ioe) {
			// We were not successful in our HTTP request
			return false;
		} catch (Exception e) {
			// We were not successful in our HTTP request
			return false;
		}
	}

	
	
	
	
	/**
	 * Performs a search on the body of on the HTML document that is retrieved.
	 * This method should only be called after a successful crawl.
	 * 
	 * @param searchWord
	 *            - The word or string to look for
	 * @return whether or not the word was found
	 */
	public boolean searchForWord(String searchWord) {
		// Defensive coding. This method should only be used after a successful
		// crawl.
		if (this.htmlDocument == null) {
			System.out
					.println("ERROR! Call crawl() before performing analysis on the document");
			return false;
		}
		System.out.println("Searching for the word " + searchWord + "...");
		String bodyText = this.htmlDocument.body().text();
		return bodyText.toLowerCase().contains(searchWord.toLowerCase());
	}

	public void saveHtmlDocument() throws IOException {

		if (this.htmlDocument == null) {
			System.out
					.println("ERROR! Call crawl() before performing analysis on the document");
		} else {
//			String bodyText = this.htmlDocument.body().text();
//			System.out.println("bodyText: " + bodyText);
//			try{
			
			
			//Get title
			Element title = this.htmlDocument.select("h1.headline").get(0);//h1.headline
			//Get content
			Element content = this.htmlDocument.select("div.bd:has(p.first)").first();
			//Get firstparagraph
			Element first = this.htmlDocument.select("div.bd>p.first").first();
			// Get time
			String txt = this.htmlDocument.select(".byline>abbr").text();
			String date = txt.split(" ")[0].replace("年", "-").replace("月", "-")
					.replace("日", "");
			String time = transferTime(txt.split(" ")[1]);

			//Get publisher
			String publisher = this.htmlDocument.select(".byline>span").text();
			
			
			
			
			
			News news = new News();
			news.set_title(toUtf8(title.text()));
			news.set_content(toUtf8(content.text()));
			news.set_date(toUtf8(date));
			news.set_time(toUtf8(time));
			news.set_publisher(toUtf8(publisher));
			news.set_url(toUtf8(_url));
			
			 
			
			System.out.println("title: " + title.text());
			System.out.println("content: " + content.text());
			System.out.println("date: " + date);
			System.out.println("time: " + time);
			System.out.println("publisher: " + publisher);
			System.out.println("url: " + _url);

			
			
			
			

			String fileName = this.htmlDocument.head().toString();
			// file name set html header
			// System.out.println(fileName.indexOf("<title>"));
			// System.out.println(fileName.indexOf("</title>"));
			if (fileName.contains("<title>")) {
				fileName = fileName.substring(fileName.indexOf("<title>") + 7,
						fileName.indexOf("</title>")).trim();
				// System.out.println(fileName);

//				FileTools.writeText(contentName, "data/" + titleName + ".txt",
//						"UTF-8", false);
				DbInvoker dbInvoker = new DbInvoker();
				dbInvoker.saveNews(news);
				System.out.println("save successful");
			}
			
			
//			}catch(Exception e){
//				System.out.println(e);
//			}
		}

	}

	public List<String> getLinks() {
		return this.links;
	}
	public static String toUtf8(String str) {
		try {
			return new String(str.getBytes("UTF-8"),"UTF-8");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	private static String transferTime(String input) {
		String result = "";
		String preFix = input.substring(0, input.indexOf("午") + 1);
		String time = input.substring(input.indexOf("午") + 1, input.length());
		String[] timePart = time.split(":");

		int hour = new Integer(timePart[0]);
		String minuteStr = timePart[1];

		if (preFix.equals("下午")) {
			hour += 12;
		}

		String hourString = new Integer(hour).toString();
		if (hourString.length() < 2) {
			hourString = "0" + hourString;
		}

		result = hourString + ":" + minuteStr;
		return result;
	}
	
	
//	public boolean crawl_clone(String url) {
//		try {
//			Connection connection = Jsoup.connect(url).userAgent(USER_AGENT);
//			Document htmlDocument = connection.get();
//			this.htmlDocument = htmlDocument;
//			if (connection.response().statusCode() == 200) // 200 is the HTTP OK
//															// status code
//															// indicating that
//															// everything is
//															// great.
//			{
//				System.out
//						.println("\n**Visiting** Received web page at " + url);
//			}
//			if (!connection.response().contentType().contains("text/html")) {
//				System.out
//						.println("**Failure** Retrieved something other than HTML");
//				return false;
//			}
//			Elements linksOnPage = htmlDocument.select("a[href]");
//			System.out.println("Found (" + linksOnPage.size() + ") links");
//			for (Element link : linksOnPage) {
//				this.links.add(link.absUrl("href"));
//			}
//			return true;
//		} catch (IOException ioe) {
//			// We were not successful in our HTTP request
//			return false;
//		} catch (Exception e) {
//			// We were not successful in our HTTP request
//			return false;
//		}
//	}

}