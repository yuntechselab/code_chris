package yahooNewsTagSpider;
import java.io.IOException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import yahooNewsTag.YahooNewsTag;

public class Spider {
	private static final int MAX_PAGES_TO_SEARCH = 1000;
	private Set<String> pagesVisited = new HashSet<String>();
	private List<String> pagesToVisit = new LinkedList<String>();
	private static final String USER_AGENT = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/535.1 (KHTML, like Gecko) Chrome/13.0.782.112 Safari/535.1";

	/**
	 * Our main launching point for the Spider's functionality. Internally it
	 * creates spider legs that make an HTTP request and parse the response (the
	 * web page).
	 * 
	 * @param url
	 *            - The starting point of the spider
	 * @param searchWord
	 *            - The word or string that you are searching for
	 * @throws IOException
	 */
	public void crawl() throws IOException {
		while (this.pagesVisited.size() < pagesToVisit.size()) {
			try {
				String currentUrl = null;
				SpiderLeg leg = new SpiderLeg();
				if (this.pagesToVisit.isEmpty()) {
//					currentUrl = url;
					// this.pagesVisited.add(url);
				} else {
					currentUrl = this.nextUrl();
				}
				leg.crawl(currentUrl); // Lots of stuff happening here. Look at
										// the
										// crawl method in
										// SpiderLeg
				
				YahooNewsTag yahooNewsTag = new YahooNewsTag(currentUrl);
				yahooNewsTag.tagYahooNews();
				System.out.println("currentUrl: " + currentUrl);
//				leg.saveHtmlDocument();
				// this.pagesToVisit.addAll(leg.getLinks());

				// if (this.pagesVisited.size() < 2)
				// this.pagesToVisit.addAll(leg.getLinks());
			} catch (Exception e) {
				System.out.println(e);
			}
		}
	}

	// public void crawl_clone(String url) throws IOException {
	// while (this.pagesVisited.size() < MAX_PAGES_TO_SEARCH) {
	// try {
	// String currentUrl;
	// SpiderLeg leg = new SpiderLeg();
	// if (this.pagesToVisit.isEmpty()) {
	// currentUrl = url;
	// this.pagesVisited.add(url);
	// } else {
	// currentUrl = this.nextUrl();
	// }
	// leg.crawl(currentUrl); // Lots of stuff happening here. Look at
	// // the
	// // crawl method in
	// // SpiderLeg
	// leg.saveHtmlDocument();
	// this.pagesToVisit.addAll(leg.getLinks());
	//
	// // if (this.pagesVisited.size() < 2)
	// // this.pagesToVisit.addAll(leg.getLinks());
	// } catch (Exception e) {
	// }
	// }
	// }
	// public void search(String url, String searchWord) throws IOException {
	// while (this.pagesVisited.size() < MAX_PAGES_TO_SEARCH) {
	// String currentUrl;
	// SpiderLeg leg = new SpiderLeg();
	// if (this.pagesToVisit.isEmpty()) {
	// currentUrl = url;
	// this.pagesVisited.add(url);
	// } else {
	// currentUrl = this.nextUrl();
	// }
	// leg.crawl(currentUrl); // Lots of stuff happening here. Look at the
	// // crawl method in
	// // SpiderLeg
	// boolean success = leg.searchForWord(searchWord);
	//
	// if (success) {
	// System.out.println(String.format(
	// "**Success** Word %s found at %s", searchWord,
	// currentUrl));
	// break;
	// }
	// this.pagesToVisit.addAll(leg.getLinks());
	//
	// }
	// System.out.println("\n**Done** Visited " + this.pagesVisited.size()
	// + " web page(s)");
	//
	// }

	/**
	 * Returns the next URL to visit (in the order that they were found). We
	 * also do a check to make sure this method doesn't return a URL that has
	 * already been visited.
	 * 
	 * @return
	 */

	private String nextUrl() {
		String nextUrl;
		do {
			nextUrl = this.pagesToVisit.remove(0);
		} while (this.pagesVisited.contains(nextUrl));
		this.pagesVisited.add(nextUrl);
		return nextUrl;
	}

	public boolean extractNewsHyperLinkListFromYAHOOTitle(String URL)
			throws IOException {
		ArrayList<String> result = new ArrayList<String>();

		Connection connection = Jsoup.connect(URL).userAgent(USER_AGENT);
		String htmlResult = connection.get().toString();

		Document doc = Jsoup.parse(htmlResult);
		Elements els = doc.select("#MediaStoryList>div>ul>li>div>div>h4>a");
		for (Element e : els) {
			String thehyperLink = e.attr("href").split(";")[0].replace("/", "");
			thehyperLink = URLEncoder.encode(thehyperLink, "utf8");
			// System.out.println(thehyperLink);
			result.add("https://tw.news.yahoo.com/" + thehyperLink);
			this.pagesToVisit.add("https://tw.news.yahoo.com/" + thehyperLink);
		}
		
		
		String thehyperLink = els.get(0).attr("href").split(";")[0].replace("/", "");
		if(thehyperLink != null||!thehyperLink.isEmpty()){
			return true;
		}
		return false;
		
	}
	
	
	
	public boolean extractWebsiteSourceExist(String URL)
			throws IOException {
		ArrayList<String> result = new ArrayList<String>();

		Connection connection = Jsoup.connect(URL).userAgent(USER_AGENT);
		String htmlResult = connection.get().toString();

		Document doc = Jsoup.parse(htmlResult);
		Elements els = doc.select("#MediaStoryList>div>ul>li>div>div>h4>a");
		String thehyperLink = null;
		for (Element e : els) {
			 thehyperLink = e.attr("href").split(";")[0].replace("/", "");
			}
		
		
		if(thehyperLink != null||!thehyperLink.isEmpty()){
			return true;
		}
		return false;
	}
}