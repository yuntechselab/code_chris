package yahooNewsTagSpider;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Locale.Category;

import tools.files.FileTools;
import entity.Website;

public class SpiderTest {
	/**
	 * This is our test. It creates a spider (which creates spider legs) and
	 * crawls the web.
	 * 
	 * @param args
	 *            - not used
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException {
		Spider spider = new Spider();

		Map<String, String> map = new HashMap<String, String>();

		ArrayList<Website> websiteArray = new ArrayList<Website>();

		int websiteCount = 0;
		for (String source : FileTools.read("newsSource.txt")) {
			websiteCount = websiteCount + 1;

			Website website = new Website();
			website.set_id(websiteCount);
			website.set_title(source.split(">")[1]);
			website.set_url(source.split(">")[0]);
			// System.out.println(source.split(">")[0]);
			// System.out.println(source.split(">")[1]);
			websiteArray.add(website);
		}

		ArrayList<String> categoryArray = new ArrayList<String>();
		 categoryArray.add("stock");
		 categoryArray.add("industry");
		 categoryArray.add("economy");
		 categoryArray.add("real-estate");
		 categoryArray.add("money-career");
		 categoryArray.add("international-finance");
		 categoryArray.add("finance");

		 categoryArray.add("lifestyle");
		 categoryArray.add("weather");
		 categoryArray.add("consumption");
		 categoryArray.add("travel");
		 categoryArray.add("transportation");
		 categoryArray.add("pets");
		 categoryArray.add("life");
		 categoryArray.add("pr-news");

		categoryArray.add("politics");
		categoryArray.add("taipei");
		categoryArray.add("north-taiwan");
		categoryArray.add("mid-taiwan");
		categoryArray.add("south-taiwan");
		categoryArray.add("east-taiwan");

//		GlobalVariable.table = "local1";
		categoryArray.add("entertainment");
		categoryArray.add("celebrity");
		categoryArray.add("tv-radio");
		categoryArray.add("music");
		categoryArray.add("jp-kr");
		categoryArray.add("movies");
		categoryArray.add("goldenmelody");

		for (String categoryName : categoryArray) {
			System.out.println("Category Name: " + categoryName);
			for (Website website : websiteArray) {
				System.out.println(website.get_url() + " : "
						+ website.get_title());
				System.out.println("website count: " + website.get_id());
				int pageCount = 1;

				try {
					for (pageCount = 1; pageCount < 41; pageCount++) {
						System.out.println("page: " + pageCount);
						Spider spiderLeg = new Spider();
						String crawlUrl = "https://tw.news.yahoo.com/"
								+ website.get_url() + "--" + categoryName
								+ "/archive/2592000--" + pageCount + ".html";
						if (!spiderLeg
								.extractNewsHyperLinkListFromYAHOOTitle(crawlUrl)) {
							break;
						}
						spider.extractNewsHyperLinkListFromYAHOOTitle(crawlUrl);
						spider.crawl();
					}
				} catch (Exception e) {
					e.printStackTrace();
				}

			}

		}
	}
}