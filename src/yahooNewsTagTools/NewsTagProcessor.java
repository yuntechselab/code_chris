package yahooNewsTagTools;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import entity.News;

public class NewsTagProcessor {

	Set<String> termList = new LinkedHashSet<String>();
	List<String> processedTermList = new ArrayList<String>();
	int maxAllowSize = 2;

	public List ckipAndSegmentAndPreprocessing(String contentText) {
		Set<String> termSet = new HashSet<String>();

		// TODO Auto-generated method stub
		// ckip
		Sinica sinica = Sinica.getInstance();
		List<String> contentPOS = sinica.segment(contentText).getUwpos();
		System.out.println("contentPOS: " + contentPOS);

		// if sentence contain nb or nc then segement
		for (String sentence : contentPOS) {
			if (sentence.contains("Nb") || sentence.contains("Nc")) {
				termList.addAll(segement(sentence));
			}
		}

		// not number and length >=2 && reduce duplicate
		for (String term : termList) {
			if (!isNumeric(term) && term.length() >= maxAllowSize) {
				termSet.add(term);
			}
		}
		for (String term : termSet) {
			processedTermList.add(term);
		}
		// long term
		longterm();
		return processedTermList;
	}

	public boolean isNumeric(String str) {
		Pattern pattern = Pattern.compile("[0-9]*");
		Matcher isNum = pattern.matcher(str);
		if (!isNum.matches()) {
			return false;
		}
		return true;
	}

	public List segement(String ckipResult) {
		// TODO Auto-generated method stub
		String rowToken = "\n----------------------------------------------------------------------------------------------------------------------------------\n";
		String rowToken2 = "----------------------------------------------------------------------------------------------------------------------------------";
		String splitHeadToken = "(";
		String splitTailToken = ")";
		List<String> result = new ArrayList<>();
		// System.out.println(article.getCkipResult());
		for (String s : ckipResult.split("　")) {
			String term = "";
			String pos = "";
			s = s.replace(rowToken2, "");
			String checkToken = s.substring(0, s.length());
			// System.out.println("checkToken: "+checkToken);

			if (checkToken.indexOf(splitHeadToken) != -1) {
				term = s.substring(0, s.indexOf(splitHeadToken)).trim();
				pos = s.substring(s.indexOf(splitHeadToken), s.length());
				pos = pos.replace(splitHeadToken, "");
				pos = pos.replace(splitTailToken, "");
				// System.out.println("term: "+term);
				// System.out.println("pos: "+pos);

				// article.getPosList().add(pos);
				// article.getTermList().add(term);
				result.add(term);
				checkToken = "";
			}
		}
		// System.out.println(article.getPosList());
		// System.out.println(article.getTermList());
		return result;
	}

	private void longterm() {
		// TODO Auto-generated method stub

		List<String> resultcompare = new ArrayList<String>(
				Arrays.asList(new String[processedTermList.size()]));
		Collections.copy(resultcompare, processedTermList);
		for (String string1 : resultcompare) {
			for (int i = 0; i < processedTermList.size(); i++) {
				if (string1.indexOf(processedTermList.get(i)) > -1
						&& !string1.equals(processedTermList.get(i))) {
					// System.out.println(result.get(i)+":"+string1);
					processedTermList.remove(i);
				}
			}
		}
	}

	public String replaceWord(News news, Map<String, String> tagWordTypeMap) {
		// TODO Auto-generated method stub
		String finalResult = news.get_htmlContent();
		List<String> regexList = new ArrayList<>(); // original list
		List<String> replacementList = new ArrayList<>(); // replace list
		Map<String, String> replaceMap = new HashMap<>();

		String Taga = "<a href=https://140.125.84.49/Wiki/jump.jsp?&wordInput=";
		String TagaAtrr = " target=_blank555"
				+ " onclick=window.open(this.href,'_blank555','location=no,width=1005,height=525'); return false;";

		for (String string : tagWordTypeMap.keySet()) {
			System.out.println("key: " + string + " value: "
					+ tagWordTypeMap.get(string));
			if (tagWordTypeMap.get(string).equals("人名")) {
				replaceMap.put(string, Taga + string + TagaAtrr
						+ "><nobr class=person>" + string + "</nobr></a>");
			} else if (tagWordTypeMap.get(string).equals("地名")) {
				replaceMap.put(string, Taga + string + TagaAtrr
						+ "><nobr class=location>" + string + "</nobr></a>");
			} else if (tagWordTypeMap.get(string).equals("組織")) {
				replaceMap
						.put(string, Taga + string + TagaAtrr
								+ "><nobr class=organization>" + string
								+ "</nobr></a>");
			}
		}

		for (String string : replaceMap.keySet()) {
			System.out.println("key: " + string + " value: "
					+ replaceMap.get(string));
			finalResult = finalResult
					.replaceAll(string, replaceMap.get(string));
			// finalResult = finalResult.replaceAll(string,
			// "<a href=https://140.125.84.47/Wiki/jump.jsp?&wordInput="
			// + string + " target=_new" + ">" + string + "</a>");

		}
		return finalResult;
	}
		// System.out.println("finalResult: " + finalResult);

		// for (String string : regexList) {
		// finalResult = finalResult
		// .replaceAll(
		// string,
		// "<a href=https://140.125.84.47/Wiki/jump.jsp?&wordInput="
		// + string
		// + " target=_blank555"
		// +
		// " onclick=window.open(this.href,'_blank555','location=no,width=500'); return false;"
		// + ">" + string + "</a>");
		// // finalResult = finalResult.replaceAll(string,
		// // "<a href=https://140.125.84.47/Wiki/jump.jsp?&wordInput="
		// // + string + " target=_new" + ">" + string + "</a>");
		// }
}
