package yahooNewsTagTools;

import java.io.IOException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Time;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.util.EntityUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import tools.files.LoadProps;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import database.DbInvoker;
import databaseTools.MySQLUtils;
import databaseTools.OurSqlConnection;
import entity.HtmlEntity;

public class FetchOperation {
	public static void main(String[] args) throws Exception {
		long startTime = System.currentTimeMillis();

		String searchName = "三軍";
		searchName = URLEncoder.encode(searchName, "UTF-8");

		String URL = "http://zh.wikipedia.org/w/api.php?action=parse&format=xml&page="
				+ searchName + "&redirects";
		System.out.println("URL: " + URL);
		long beforeDbTime = System.currentTimeMillis();

		System.out.println("DB實驗前共花了" + (beforeDbTime - startTime) / 1000.0
				+ "秒");

		DbInvoker dbInvoker = new DbInvoker();
		String html = new DbInvoker().readHtml(URL);

		long afterDbTime = System.currentTimeMillis();
		System.out.println("DB實驗後共花了" + (afterDbTime - startTime) / 1000.0
				+ "秒");

		if (html != null) {
			System.out.println("exist in db");
			 System.out.println(html);
		} else {
			System.out.println("not in db");
			long lDateTime = new Date().getTime();

			Document doc = Jsoup.connect(URL).timeout(100000).get();
			String html1 = doc.html();
			// System.out.println(html1);

			html1 = ChangeCode.toLong(doc.html());
			HtmlEntity htmlEntity = new HtmlEntity(URL, html1, lDateTime);
			DbInvoker dbInvoker1 = new DbInvoker();

			dbInvoker1.saveHtml(htmlEntity);
			System.out.println("not in db");
		}
		// Document doc = FetchOperation.getDocumentFromURL(URL, "UTF-8", true);
		long endTime = System.currentTimeMillis();

		System.out.println("實驗共花了" + (endTime - startTime) / 1000.0 + "秒");

	}

	public static String getHtmlFromURL(String input, String charSet,
			boolean isZh) throws Exception {
		String result = "";
		HttpClient httpclient = new DefaultHttpClient();
		HttpConnectionParams.setSoTimeout(httpclient.getParams(), 10000);
		HttpGet httpGet = new HttpGet(input);
		System.out.println("Extracting..." + httpGet.getURI());

		HttpResponse response = httpclient.execute(httpGet);
		HttpEntity entity = response.getEntity();
		String returnText = EntityUtils.toString(entity, charSet);
		result = returnText;
		if (isZh) {
			// 簡體轉繁體
			result = ChangeCode.toLong(result);
		}

		System.out.println("Finished");
		httpclient.getConnectionManager().shutdown();
		return result;
	}

	public static Document getDocumentFromURL(String url, String charSet,
			boolean isZh) throws Exception {
		long startTime = System.currentTimeMillis();
		System.out.println("getDocumentFromURL前共花了" + (startTime) / 1000.0
				+ "秒");

		Document doc = null;
		DbInvoker dbInvoker = new DbInvoker();

		try {
			String sql = "";

			long beforeReadDbTime = System.currentTimeMillis();
			System.out.println("getDocumentFromURL讀DB前花了" + (beforeReadDbTime)
					/ 1000.0 + "秒");

			// 如果資料庫存在這一張表的話，直接取出資料庫
			String htmlDB = dbInvoker.readHtml(url);

			long afterReadDbTime = System.currentTimeMillis();
			System.out.println("getDocumentFromURL讀DB後花了" + (afterReadDbTime)
					/ 1000.0 + "秒");
			System.out.println("getDocumentFromURL讀DB後共花了"
					+ (afterReadDbTime - beforeReadDbTime) / 1000.0 + "秒");

			if (htmlDB != null) {
				// if (isDB(url)) {
				// System.out.println("存在 in DB");
				// OurSqlConnection conn = new OurSqlConnection(
				// LoadProps.getProperties("databaseName"));// test
				// // (Chris)跟isDB的資料庫名稱不同，要Check一下
				// sql = "select * from  htmlTable where url='" + url + "'";
				// conn.setSql(sql);
				//
				// ResultSet rs = conn.getResultSet();
				// rs.next();
				//
				// String htmlDB = rs.getString("html");
				// conn.close();
				if (isZh) {
					// 簡體轉繁體
					htmlDB = ChangeCode.toLong(htmlDB);
				}
				doc = Jsoup.parse(htmlDB);
			}
			// 資料庫不存在此表
			else {
				long beforeCrawlTime = System.currentTimeMillis();
				System.out.println("getDocumentFromURL爬資料前花了"
						+ (beforeCrawlTime) / 1000.0 + "秒");

				System.out.println("需要爬：" + url);

				doc = Jsoup.connect(url).timeout(100000).get();
				String html = doc.html();
				// System.out.println(html);
				if (isZh) {
					// 簡體轉繁體
					html = ChangeCode.toLong(doc.html());
				}
				long afterCrawlTime = System.currentTimeMillis();
				System.out.println("getDocumentFromURL爬資料後花了"
						+ (afterCrawlTime) / 1000.0 + "秒");
				System.out.println("getDocumentFromURL爬資料後共花了"
						+ (afterCrawlTime - beforeCrawlTime) / 1000.0 + "秒");

				// 存到資料庫

				// Class.forName("com.mysql.jdbc.Driver");
				// String tableName = LoadProps.getProperties("databaseName");
				// String connStr = LoadProps.getProperties("url") + tableName
				// + "?useUnicode=true&characterEncoding=utf-8&user="
				// + LoadProps.getProperties("user") + "&password="
				// + LoadProps.getProperties("password");

				// String connStr =
				// "jdbc:mysql://140.125.84.58:3306/103wikiproject?useUnicode=true&characterEncoding=utf-8&user=root&password=root";
				// Connection conn2 = DriverManager.getConnection(connStr);
				// sql = "insert into htmlTable(url,html,time) values(?,?,?)";
				try {
					// PreparedStatement sss = conn2.prepareStatement(sql);
					// sss.setString(1, url);
					// sss.setString(2, html);
					long lDateTime = new Date().getTime();
					// sss.setLong(3, lDateTime);
					// sss.executeUpdate();
					// conn2.close();
					HtmlEntity htmlEntity = new HtmlEntity(url, html, lDateTime);
					dbInvoker.saveHtml(htmlEntity);
				} catch (Exception e) {
					// conn2.close();
					System.out.println(e.toString());
				}

			}
			long endTime = System.currentTimeMillis();
			System.out.println("getDocumentFromURL後花了" + (endTime) / 1000.0
					+ "秒");
			System.out.println("getDocumentFromURL後共花了" + (endTime - startTime)
					/ 1000.0 + "秒");
		} catch (IOException e) {

			System.out.println(e);
		} finally {

			System.out.println("Finished");
			return doc;

		}

	}

	public static Document getDocumentFromURL(String url, String charSet,
			boolean isZh, String queryWord) throws Exception {
		System.out.println("queryWord: " + queryWord);
		long startTime = System.currentTimeMillis();
		System.out.println("getDocumentFromURL前共花了" + (startTime) / 1000.0
				+ "秒");

		Document doc = null;
		DbInvoker dbInvoker = new DbInvoker();

		try {
			String sql = "";

			long beforeReadDbTime = System.currentTimeMillis();
			System.out.println("getDocumentFromURL讀DB前花了" + (beforeReadDbTime)
					/ 1000.0 + "秒");

			// 如果資料庫存在這一張表的話，直接取出資料庫
			// String htmlDB = dbInvoker.readHtmlWithQueryWord(queryWord);
			String htmlDB = null;

			long afterReadDbTime = System.currentTimeMillis();
			System.out.println("getDocumentFromURL讀DB後花了" + (afterReadDbTime)
					/ 1000.0 + "秒");
			System.out.println("getDocumentFromURL讀DB後共花了"
					+ (afterReadDbTime - beforeReadDbTime) / 1000.0 + "秒");

			if (htmlDB != null) {
				// if (isDB(url)) {
				// System.out.println("存在 in DB");
				// OurSqlConnection conn = new OurSqlConnection(
				// LoadProps.getProperties("databaseName"));// test
				// // (Chris)跟isDB的資料庫名稱不同，要Check一下
				// sql = "select * from  htmlTable where url='" + url + "'";
				// conn.setSql(sql);
				//
				// ResultSet rs = conn.getResultSet();
				// rs.next();
				//
				// String htmlDB = rs.getString("html");
				// conn.close();
				if (isZh) {
					// 簡體轉繁體
					htmlDB = Encoder.SimToTra(htmlDB);
					// htmlDB = ChangeCode.toLong(htmlDB);
					System.out.println("可怕的簡體:" + htmlDB);
				}
				doc = Jsoup.parse(htmlDB);
			}
			// 資料庫不存在此表
			else {
				long beforeCrawlTime = System.currentTimeMillis();
				System.out.println("getDocumentFromURL爬資料前花了"
						+ (beforeCrawlTime) / 1000.0 + "秒");

				System.out.println("需要爬：" + url);

				doc = Jsoup.connect(url).timeout(100000).get();
				String html = doc.html();
				// System.out.println(html);
				if (isZh) {
					// 簡體轉繁體
					html = ChangeCode.toLong(doc.html());
				}
				long afterCrawlTime = System.currentTimeMillis();
				System.out.println("getDocumentFromURL爬資料後花了"
						+ (afterCrawlTime) / 1000.0 + "秒");
				System.out.println("getDocumentFromURL爬資料後共花了"
						+ (afterCrawlTime - beforeCrawlTime) / 1000.0 + "秒");

				// 存到資料庫

				// Class.forName("com.mysql.jdbc.Driver");
				// String tableName = LoadProps.getProperties("databaseName");
				// String connStr = LoadProps.getProperties("url") + tableName
				// + "?useUnicode=true&characterEncoding=utf-8&user="
				// + LoadProps.getProperties("user") + "&password="
				// + LoadProps.getProperties("password");

				// String connStr =
				// "jdbc:mysql://140.125.84.58:3306/103wikiproject?useUnicode=true&characterEncoding=utf-8&user=root&password=root";
				// Connection conn2 = DriverManager.getConnection(connStr);
				// sql = "insert into htmlTable(url,html,time) values(?,?,?)";
				try {
					// PreparedStatement sss = conn2.prepareStatement(sql);
					// sss.setString(1, url);
					// sss.setString(2, html);
					long lDateTime = new Date().getTime();
					// sss.setLong(3, lDateTime);
					// sss.executeUpdate();
					// conn2.close();
					HtmlEntity htmlEntity = new HtmlEntity(url, html,
							lDateTime, queryWord);
					dbInvoker.saveHtmlWithQueryWord(htmlEntity);
				} catch (Exception e) {
					// conn2.close();
					System.out.println(e.toString());
				}

			}
			long endTime = System.currentTimeMillis();
			System.out.println("getDocumentFromURL後花了" + (endTime) / 1000.0
					+ "秒");
			System.out.println("getDocumentFromURL後共花了" + (endTime - startTime)
					/ 1000.0 + "秒");
		} catch (IOException e) {

			System.out.println(e);
		} finally {

			System.out.println("Finished");
			return doc;

		}

	}
	// public static boolean isDB(String url) {
	// OurSqlConnection conn = new OurSqlConnection(
	// LoadProps.getProperties("databaseName"));
	// String sql = "Select count(*) from htmlTable where url='" + url + "'";
	// conn.setSql(sql);
	// ResultSet rs = conn.getResultSet();
	// int dataCount;
	// try {
	// rs.next();
	// dataCount = rs.getInt("count(*)");
	// if (dataCount == 0) {
	// conn.close();
	// return false;
	// } else {
	// conn.close();
	// return true;
	// }
	// } catch (SQLException e) {
	// conn.close();
	// System.out.println("?");
	// return false;
	// }
	// }
}
