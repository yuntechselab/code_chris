package yahooNewsTagTools;


import com.spreada.utils.chinese.ZHConverter;
import databaseTools.OurSqlConnection;

public class Encoder {

	
	//簡轉繁
	public static String SimToTra(String simpStr) {  
        ZHConverter converter = ZHConverter  
                .getInstance(ZHConverter.TRADITIONAL);  
        String traditionalStr = converter.convert(simpStr);  
        return traditionalStr;  
    }
	
	//繁轉簡
	public static String TraToSim(String tradStr) {  
        ZHConverter converter = ZHConverter.getInstance(ZHConverter.SIMPLIFIED);  
        String simplifiedStr = converter.convert(tradStr);  
        return simplifiedStr;  
    }  
	
	
}
