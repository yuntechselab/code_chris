package yahooNewsTagTools;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import WeaveNerTools.DetectionEngine;
import database.DbInvoker;
import entity.TagWord;
import socket.SearchEngine;
import synonymResearch.WikiTermBean_backup;

public class Wikipedia {
	List result = new ArrayList<>();

	public Map<String, String> wikiNerDetect(List<String> termList) {
		Map<String, String> tagWordTypeMap = new HashMap<>();

		Map<String, String> tagWordDb = null;
		List<String> nonTagWordDb = null;
		DbInvoker dbInvoker = new DbInvoker();
		try {
			tagWordDb = dbInvoker.readTagWordBatch(termList);
			nonTagWordDb = dbInvoker.readNonTagWordBatch(termList);

		} catch (Exception e) {
			e.printStackTrace();
		}
		for (String key : termList) {

			try {
				// System.out.println(key+key.length());
				// String type = dbInvoker.readTagWord(key);
				// String nonTagWord = dbInvoker.readNonTagWord(key);
				System.out.println("------------------" + key
						+ "------------------");

				if (tagWordDb.get(key) != null) {
					System.out.println("--------------" + key
							+ "有在TagWord資料庫------------------");
					tagWordTypeMap.put(key, tagWordDb.get(key));
				} else if (nonTagWordDb.contains(key)) {
					System.out.println("--------------" + key
							+ "有在NonTagWord資料庫------------------");
				} else {
					String type = new DetectionEngine()
							.getTypeFromWikiWoDb(key);
					if (type != null && type != "" && !type.isEmpty()
							&& !type.equals("")) {

						tagWordTypeMap.put(key, type);
						TagWord tagWord = new TagWord(key, type);
						dbInvoker.saveTagWord(tagWord);
					} else {
						dbInvoker.saveNonTagWord(key);
					}

				}
			} catch (Exception e) {
				e.printStackTrace();
			}

		}
		return tagWordTypeMap;
	}

	public Map<String, String> wikiNerDetectMultiThread(List<String> termList) {
		Map<String, String> tagWordTypeMap = new HashMap<>();
		for (String key : termList) {

			try {
				// System.out.println(key+key.length());
				DbInvoker dbInvoker = new DbInvoker();
				String type = dbInvoker.readTagWord(key);
				String nonTagWord = dbInvoker.readNonTagWord(key);
				System.out.println("------------------" + key
						+ "------------------");

				if (type != null && type != "" && !type.isEmpty()
						&& !type.equals("")) {
					System.out.println("--------------" + key
							+ "有在TagWord資料庫------------------");
					tagWordTypeMap.put(key, type);
				} else if (nonTagWord != null && nonTagWord != ""
						&& !nonTagWord.isEmpty() && !nonTagWord.equals("")) {
					System.out.println("--------------" + key
							+ "有在NonTagWord資料庫------------------");
					continue;
				} else {
					type = new DetectionEngine().getTypeFromWikiWoDb(key);
					if (type != null && type != "" && !type.isEmpty()
							&& !type.equals("")) {

						tagWordTypeMap.put(key, type);
						TagWord tagWord = new TagWord(key, type);
						dbInvoker.saveTagWord(tagWord);
					} else {
						dbInvoker.saveNonTagWord(key);
					}

				}
			} catch (Exception e) {
				e.printStackTrace();
			}

		}
		return tagWordTypeMap;
	}
}