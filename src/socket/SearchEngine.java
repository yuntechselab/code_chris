package socket;

import java.io.UnsupportedEncodingException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import synonymResearch.SingleTermForWikiTerm;
import synonymResearch.WikiTermBean_backup;
import databaseTools.OurSqlConnectionSinglton;

public class SearchEngine {
	public static void main(String[] args) {
		String queryWord = "美國";
		try {
//			queryWord = new String(queryWord.getBytes("ISO-8859-1"), "UTF-8");
			SearchEngine search = new SearchEngine(queryWord);
			Map<String, WikiTermBean_backup> map = search.getSearchResult();
			System.out.println("result"+ map);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static String searchTableName = "answerword";
	private String queryWord;

	public SearchEngine(String queryWord) {
		this.queryWord = queryWord;
	}

	public Map<String, WikiTermBean_backup> getSearchResult() {

		SingleTermForWikiTerm single = new SingleTermForWikiTerm(this.queryWord);
		Map<String, WikiTermBean_backup> result1 = null;
		try {
			result1 = single.getSynoym();
			if (result1 == null) {
				return null;
			} else {
				result1 = SingleTermForWikiTerm.removeDuplicate(result1);
			}

		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		Map<String, WikiTermBean_backup> result2 = null;
		;
		try {
			result2 = single.getRelationshipWord();
			result2 = null;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (result2 == null) {
			result2 = new HashMap<String, WikiTermBean_backup>();
		}
		result2.putAll(result1);
		OurSqlConnectionSinglton.close();

		return result2;

	}

}
