package Process;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 去除新聞雜訊
 * 
 * @author Eviles
 * @modify Yi
 * @valid Mill
 */

public class ContentFilter {
	//private static final String pattern1 = "^（圖文）|^〈快訊〉|〈獨家〉|（.+?報導）|（.+?日電）|〔.+?報導〕|〈.+?報導〉|（.+?電）|〔.+?電〕|〈.+?電〉|【.+?訊】|（.+?訊）|〔.+?訊〕|〈.+?訊〉";
	private static final String pattern2 = ".*電---|快訊|(圖)|(中國時報)|（完）--.*|（●.*?）|●東森論壇.*|.*?[／].*?專訪|.*?[／╱].*?報導|.*?[/].*?報導|.*?[／╱].*?特稿|</?[aA-zZ].*?[^<>]*>|<!--.*?-->|.*?日電|.*?專電";
	private static final String pattern3 = "\\d{6}$";
	private static final String pattern4 = "（完）--.*";
	
	private static final String pattern1 = "^（圖文）|^〈快訊〉|〈獨家〉|（（.+報導）|〔.+報導〕|〈.+報導〉|（.+?電）|〔.+?電〕|〈.+?電〉|【.+訊】|【.*?】|（.+訊）|〔.+訊〕|〈.+訊〉|.+報導】|新頭殼.+[/]..報導|中國時報.+[╱].+電】|旺報.+】|中國時報.+[╱].+分析】";
	//private static final String pattern2 = ".*電---|（完）--.*|（●.*?）|●東森論壇.*|.*?[／╱].*?報導|.*?[／╱].*?特稿|</?[aA-zZ].*?[^<>]*>|<!--.*?-->|快訊|(圖)";

	/*
	 * update @ 2008/04 去除 中央社***攝,中央社***報導,記者***報導,歐新社, 軍聞社等
	 */
	private static final String pattern5 = "中央社+[\\S&&[^攝真]]*[攝真]+|歐新社|記者[\\S]*特稿+|（軍聞社[\\S&&[^）]]+）|（請配合本社社稿+[\\S&&[^）]]+|民視新聞綜合報導+。?）+|●東森論壇徵稿區→http://www.ettoday.com/write/●來稿或參與討論的文章也可寄至public@ettoday.com+。?";
	/*
	 * 去除日期 如:九十七年二月二十四日
970224。 註：僅有 "九十七年" "97"開頭數字日期 有效，日後請再修改
	 */
	private static final String pattern6 = "[\\s　]+九十九年+[\\S&&[^月]]+月{1}[\\S&&[^日]]+日{1}。?|(101){1}[01]{1}\\d{1}[0123]{1}\\d{1}。?";

	/**
	 * 數字半型轉換全型
	 * 
	 * @param content
	 * @return 轉換的結果
	 */
	public static final String reNum(String content) {
		String[] num = new String[] { "０", "１", "２", "３", "４", "５", "６", "７",
				"８", "９" };
		for (int i = 0; i <= 9; i++) {
			content = content.replaceAll(num[i], String.valueOf(i));
		}
		return content;
	}

	/**
	 * 取代HTML Unicode 例：&#22531; = ?
	 * 
	 * @param content
	 * @return 轉換的結果
	 */
	public static final String reUnicode(String content) {
		content = content.replaceAll("＆#36;", "＄");
		Matcher matcher = Pattern.compile("＄u\\d+?;").matcher(content);
		while (matcher.find()) {
			String code = matcher.group();
			int codenum = Integer
					.parseInt(code.substring(6, code.length() - 1));
			content = content.replaceAll(code, String.valueOf(Character
					.toChars(codenum)));
		}
		matcher = Pattern.compile("＆#\\d+?;").matcher(content);
		while (matcher.find()) {
			String code = matcher.group();
			int codenum = Integer
					.parseInt(code.substring(2, code.length() - 1));
			content = content.replaceAll(code, String.valueOf(Character
					.toChars(codenum)));
		}
		return content;
	}

	/**
	 * 取代內文字詞雜訊
	 * 
	 * @param content
	 * @return 轉換的結果
	 */
	public static final String reTerms(String content) {
		// 取代有問題的符號
		content = content.replaceAll("\\&", "＆");
		content = content.replaceAll("\\=", "＝");
		content = content.replaceAll("\\*", "＊");
		content = content.replaceAll("\\+", "＋");
//		content = content.replaceAll("\\(", "（");
//		content = content.replaceAll("\\)", "）");
		content = content.replaceAll("\\（", "");
		content = content.replaceAll("\\）", "");
		content = content.replaceAll("\\(", "");
		content = content.replaceAll("\\)", "");
		content = content.replaceAll("。」", "」。");
		content = content.replaceAll("﹔", "；");
		content = content.replaceAll("＆quot;", "”");
		//content = content.replaceAll(" ", "，");
		content = content.replaceAll("　", "，");
	
		// 去除斷行
		content = content.replaceAll("\r\n", "");
		content = content.replaceAll("\r", "");
		// 去除報導雜訊
		content = content.replaceAll(pattern1, "");
		content = content.replaceAll(pattern2, "");
		content = content.replaceAll(pattern3, "");
		content = content.replaceAll(pattern4, "");

		content = content.replaceAll(pattern5, "");
		content = content.replaceAll(pattern6, "");
		// 取代</P><P>為斷行
		content = content.replaceAll("\\<\\/p\\>\\<p\\>", "\r\n");
		// 取代已知問題字
		content =content.replaceAll("(倈)", "來");
		content =content.replaceAll("陳沖", "陳冲");
		content =content.replaceAll("萁","其");
		content=content.replaceAll("瑠", "留");
		content=content.replaceAll("堃", "坤");
		content=content.replaceAll("崐", "焜");
		// 去TAB
		content = content.replaceAll("\\t", "");
	
		// 去頭尾空白
		content = content.trim();
		return content;
	}

	/**
	 * 若文末無句號補上句號
	 * 
	 * @param content
	 * @return 轉換的結果
	 */
	public static final String reCategory(String content) {
		String tmp = content.replaceAll("\r|\n|\\s", "");
		if (tmp.length() > 1) {
			if ("！".equals(tmp.substring(tmp.length() - 1))) {
				content = content.substring(0, content.lastIndexOf("！"));
			} else if ("？".equals(tmp.substring(tmp.length() - 1))) {
				content = content.substring(0, content.lastIndexOf("？"));
			} else if ("。。".equals(tmp.substring(tmp.length() - 2))) {
				content = content.substring(0, content.lastIndexOf("。"));
			}
			if (!"。".equals(tmp.substring(tmp.length() - 1))) {
				content = content + "。";
			}
		}
		return content;
	}

	/**
	 * 全文雜訊處理
	 * 
	 * @param content
	 * @return 轉換的結果
	 */
	public static final String filter(String content) {
		content = reTerms(content);
		content = reUnicode(content);
		content = reCategory(content);
		return content;
	}
}
