package Process;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import com.go.Wiki;

import databaseTools.OurSqlConnection;

import entity.WikiObject;

public class SynonymProcessing {
    public static void main(String[] args) {
	ArrayList<String> list = searchSynonymSQL("建中");
	OurSqlConnection conn = new OurSqlConnection("synonymdb");
	for(String sql : list){
	    conn.setSql(sql);
	    conn.InsertNew(sql);
	}
    }

    public static ArrayList<String> searchSynonymSQL(String search_txt) {
	ArrayList<String> sqlList = new ArrayList<String>();
	try {
	    Wiki wiki = new Wiki();
	    WikiObject wo = wiki.createWikiObject(search_txt, "http://zh.wikipedia.org/w/api.php?action=parse&format=xml&page=" + search_txt + "&redirects");
	    Date d = new Date();
	    // 一形多詞
	    if (wo.isExist() == false) {
		StringBuilder sb = new StringBuilder();
		int count = 0;
		// 參考
		for (String s : wo.getReferences()) {
		    sqlList.add("Insert into synonyms(queryWord,mappingWord,mappingRule,reasonability,openCategory,mappingTime) values('" + search_txt + "','" + s + "','reference','100%','" + wo.getOpenCategory().toString().replace("'", "") + "','" + d.getTime() + "'");
		    sb.append("參考：" + s + "\n");
		    if (++count >= 3) {
			break;
		    }
		}
		System.out.println(sb.toString());
	    } else {
		// 一型多詞
		if (wo.isPolysemy()) {
		    StringBuilder sb = new StringBuilder();
		    for (String s : wo.getPolysemySet()) {
			sb.append("一型多詞：" + s + "\n");
			sqlList.add("Insert into synonyms(queryWord,mappingWord,mappingRule,reasonability,openCategory,mappingTime) values('" + search_txt + "','" + s + "','polysemy','100%','" + wo.getOpenCategory().toString().replace("'", "") + "','" + d.getTime() + "')");
		    }
		    System.out.println(sb.toString());
		} else {
		    HashMap<String, String> synonyms = wo.getSynonyms();
		    StringBuilder sb = new StringBuilder();

		    ArrayList<String> ruleResult = new ArrayList<String>();

		    // Rule Mapping
		    for (String s : synonyms.keySet()) {
			String mappingType = synonyms.get(s);
			if (mappingType.equals("Rule")) {
			    ruleResult.add(s);
			} else {
			    sb.append("Rule Mapping：" + s + "\n");
			}
			sqlList.add("Insert into synonyms(queryWord,mappingWord,mappingRule,reasonability,openCategory,mappingTime) values('" + search_txt + "','" + s + "','Rule','100%','" + wo.getOpenCategory().toString().replace("'", "") + "','" + d.getTime() + "')");
		    }
		    for (String rule : ruleResult) {
			sb.append("insert into synonyms " + rule + "\n");

		    }
		    System.out.println(sb.toString());
		}
	    }

	} catch (IndexOutOfBoundsException iobe) {
	    System.out.println("目前查無此同義字配對");
	} catch (Exception e) {
	    System.out.println("出錯" + e.toString());
	}
	finally{
	    return sqlList;
	}
    }
}
