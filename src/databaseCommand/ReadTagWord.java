package databaseCommand;

import java.sql.SQLException;
import java.util.ArrayList;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.ResultSet;
import com.mysql.jdbc.Statement;

import tools.files.LoadProps;
import entity.News;

public class ReadTagWord extends ReadCommand {
	private String tagWord;

	public ReadTagWord(String tagWord) {
		this.tagWord = tagWord;
	}

	@Override
	public Object processResult() {
		// TODO Auto-generated method stub
		String tagType = null;
		try {
			while (rs.next()) {
				tagType = rs.getString("tagType");
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			// dbMgr.closeDB(rs, sta, conn);
		}

		return tagType;
	}

	@Override
	public void queryDB() {
		// TODO Auto-generated method stub

		try {
			conn =  dbMgr.initDB();
			sta =  conn.createStatement();
			String sql = "SELECT tagType FROM "
					+ LoadProps.getProperties("tagWordTableName")
					+ " Where tagWord = '" + tagWord + "'";
			rs = sta.executeQuery(sql);

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
