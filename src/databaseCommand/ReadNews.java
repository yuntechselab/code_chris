package databaseCommand;

import java.sql.SQLException;
import java.util.ArrayList;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.ResultSet;
import com.mysql.jdbc.Statement;

import tools.files.LoadProps;
import entity.News;

public class ReadNews extends ReadCommand {
	private String _url;

	public ReadNews(String _url) {
		this._url = _url;
	}

	@Override
	public Object processResult() {
		// TODO Auto-generated method stub
		News news = new News();
		try {
			while (rs.next()) {
				news.set_tagContent(rs.getString("_tagContent"));
				news.set_htmlRelatedNewsContent(rs.getString("_htmlRelatedNewsContent"));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			// dbMgr.closeDB(rs, sta, conn);
		}

		return news;
	}

	@Override
	public void queryDB() {
		// TODO Auto-generated method stub

		try {
			conn = dbMgr.initDB();
			sta = (Statement) conn.createStatement();
			String sql = "SELECT _tagContent, _htmlRelatedNewsContent FROM "
					+ LoadProps.getProperties("newsTableName")
					+ " Where _url = '" + _url + "'";
			rs = (ResultSet) sta.executeQuery(sql);

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
