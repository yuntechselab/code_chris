package databaseCommand;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.ResultSet;
import com.mysql.jdbc.Statement;

import tools.files.LoadProps;
import entity.News;
import entity.WikiTermBean;

public class ReadAnswerWord extends ReadCommand {
	private String queryWord;

	public ReadAnswerWord(String queryWord) {
		this.queryWord = queryWord;
	}

	@Override
	public Object processResult() {
		// TODO Auto-generated method stub
		Map<String, WikiTermBean> totalTerm = new HashMap<String, WikiTermBean>();
		// boolean flag = true;
		try {
			while (rs.next()) {
				// if(flag){
				// String tmp = result.getString("openCategory");
				// if(!(tmp.equals("[]"))){
				// flag = false;
				// ArrayList<String> OCTerm = this.getOpenCategory(tmp);
				// for(String s : OCTerm){
				// totalTerm.put(s, "Rule");
				// }
				// }
				// }
				String mappingWord = rs.getString("mappingWord");
				// System.out.println("mappingWord: " + mappingWord);
				String mappingRule = rs.getString("mappingRule");
				String reasonability = rs.getString("reasonability");
				int totalNumber = rs.getInt("totalNumber");
				int agreeNumber = rs.getInt("agreeNumber");
				WikiTermBean t = new WikiTermBean(mappingWord, mappingRule,
						reasonability, totalNumber, agreeNumber);
				totalTerm.put(mappingWord, t);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("getResult error !!");
		}
		return totalTerm;

	}

	@Override
	public void queryDB() {
		// TODO Auto-generated method stub

		try {
			conn = dbMgr.initDB();
			sta = (Statement) conn.createStatement();
			String sql = "Select * From answerword Where queryWord ='"
					+ queryWord + "'";
			rs = sta.executeQuery(sql);
			System.out.println("sql: " + sql);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
