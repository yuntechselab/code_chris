package databaseCommand;

import java.util.ArrayList;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.PreparedStatement;

import entity.*;

public class UpdatePersonAndPlace extends SqlImpCmd {
	String sql = "update "+ "" + " set _person = ?, _place = ? where id = ?";

	@Override
	public Object processResult() {
		ArrayList arrayList =  (ArrayList) object;
		try {
			conn =  dbMgr.initDB();

//			sta = (Statement) conn.createStatement();
			
			pst =  conn.prepareStatement(sql);			
			
			pst.setString(1, (String) arrayList.get(1));
	        pst.setString(2,  (String) arrayList.get(2));

	        pst.setInt(3,  (Integer) arrayList.get(0));
			
	        
	        pst.addBatch();

	        pst.executeUpdate();
			
			
			
			
			
			
			
	        
			
			
			

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			dbMgr.closeDBBatch(pst, conn);
		}

		return null;
	}

	@Override
	public void queryDB() {
	}

	public String getSql() {
		return sql;
	}

	public void setSql(String sql) {
		this.sql = sql;
	}
	
	public void disconnectDB() {
		// TODO Auto-generated method stub
		// Close
		dbMgr.closeDB(sta, conn);

	}
}
