package databaseCommand;



import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

import database.DbMgr;
/**
 * Created by weichingxiao on 2016/5/28.
 */

public abstract class SqlImpCmd implements Cloneable {

	String name;
	// DbMgr dbMgr = new DbMgr();
	DbMgr dbMgr;
	ResultSet resultSet;
	Connection conn = null;
	Statement sta = null;
	ResultSet rs = null;
	PreparedStatement pst = null;
	String temp = "";
	Object object;

	public SqlImpCmd() {
		super();
	}

	public Object getObject() {
		return object;
	}

	public void setObject(Object object) {
		this.object = object;
	}

	public boolean execute() {
		try {
			openDB();
			queryDB();
			object = processResult();
			disconnectDB();

			return true;

		} catch (Exception e) {
//			disconnectDB();

			e.printStackTrace();
		}
		return false;
	}

	public abstract Object processResult();

	public void disconnectDB() {
		// TODO Auto-generated method stub
		// Close
		dbMgr.closeDB(rs, sta, conn);
//		dbMgr.closeDB(sta, conn);
	}

	public void openDB() {
		try {
			dbMgr = DbMgr.getDbCon();
			conn =  dbMgr.initDB();
		} catch (Exception sqle) {
			sqle.printStackTrace();
		}
	}

	public abstract void queryDB();

}
