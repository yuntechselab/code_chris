package databaseCommand;

import java.sql.SQLException;
import java.util.ArrayList;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.ResultSet;
import com.mysql.jdbc.Statement;

import tools.files.LoadProps;
import entity.HtmlEntity;
import entity.News;

public class ReadHtmlWithQueryWord extends ReadCommand {
	private String queryWord;

	public ReadHtmlWithQueryWord(String queryWord) {
		this.queryWord = queryWord;
	}

	@Override
	public Object processResult() {
		// TODO Auto-generated method stub
		String  html = null;
		try {
			while (rs.next()) {
				html =rs.getString("html");
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			// dbMgr.closeDB(rs, sta, conn);
		}

		return html;
	}

	@Override
	public void queryDB() {
		// TODO Auto-generated method stub

		try {
			conn =  dbMgr.initDB();
			sta =  conn.createStatement();
			String sql = "SELECT html FROM "
					+ LoadProps.getProperties("htmlTableName")
					+ " Where queryWord = '" + queryWord + "'";
			rs = sta.executeQuery(sql);

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
