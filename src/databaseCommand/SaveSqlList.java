package databaseCommand;

import java.sql.Statement;

import tools.files.LoadProps;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.PreparedStatement;

import entity.*;

public class SaveSqlList extends SaveCommand {
	String sql = "";
	@Override
	public Object processResult() {

		String sql = (String) object;
		try {
			conn = dbMgr.initDB();
			// sta = (Statement) conn.createStatement();
			pst =  conn.prepareStatement(sql);

			
			pst.addBatch(sql);
			pst.executeBatch();
			
		
			
			System.out.println("===Save Sql Success===");

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
//			dbMgr.closeDBBatch(pst, conn);
		}

		return null;
	}

	@Override
	public void queryDB() {
	}

	public String getSql() {
		return sql;
	}

	public void setSql(String sql) {
		this.sql = sql;
	}
}
