package databaseCommand;

import tools.files.LoadProps;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.PreparedStatement;

import entity.*;

public class SaveNews extends SaveCommand {
	String sql = "INSERT INTO "+LoadProps.getProperties("newsTableName")+" (_title,_content,_url,_publisher,_date,_time,_htmlContent,_tagContent,_htmlRelatedNewsContent) VALUES(?,?,?,?,?,?,?,?,?)";
	@Override
	public Object processResult() {

		News news = (News) object;
		try {
			conn = dbMgr.initDB();
			// sta = (Statement) conn.createStatement();
			pst =  conn.prepareStatement(sql);

			pst.setString(1, news.get_title());
			pst.setString(2, news.get_content());
			pst.setString(3, news.get_url());
			pst.setString(4, news.get_publisher());
			pst.setString(5, news.get_date());
			pst.setString(6, news.get_time());
			pst.setString(7, news.get_htmlContent());
			pst.setString(8, news.get_tagContent());
			pst.setString(9, news.get_htmlRelatedNewsContent());

			
			pst.addBatch();
			pst.executeBatch();
			System.out.println("===Save News Success===");

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
//			dbMgr.closeDBBatch(pst, conn);
		}

		return null;
	}

	@Override
	public void queryDB() {
	}

	public String getSql() {
		return sql;
	}

	public void setSql(String sql) {
		this.sql = sql;
	}
}
