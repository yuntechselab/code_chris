package databaseCommand;

import tools.files.LoadProps;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.PreparedStatement;

import entity.*;

public class SaveNonTagWord extends SaveCommand {
	String sql = "INSERT INTO "+LoadProps.getProperties("nonTagWordTableName")+" (nonTagWord) VALUES(?)";
	@Override
	public Object processResult() {

		String nonTagWord= (String) object;
		try {
			conn =  dbMgr.initDB();
			// sta = (Statement) conn.createStatement();
			pst =  conn.prepareStatement(sql);

			pst.setString(1, nonTagWord);
			

			pst.addBatch();
			pst.executeBatch();
			System.out.println("===Save NonTag Word Success===");

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
//			dbMgr.closeDBBatch(pst, conn);
		}

		return null;
	}

	@Override
	public void queryDB() {
	}

	public String getSql() {
		return sql;
	}

	public void setSql(String sql) {
		this.sql = sql;
	}
}
