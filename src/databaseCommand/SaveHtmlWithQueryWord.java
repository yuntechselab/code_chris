package databaseCommand;

import tools.files.LoadProps;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.PreparedStatement;

import entity.*;

public class SaveHtmlWithQueryWord extends SaveCommand {
	String sql = "INSERT INTO "+LoadProps.getProperties("htmlTableName")+" (url, html, time, queryWord) VALUES(?,?,?,?)";
	@Override
	public Object processResult() {

		HtmlEntity html = (HtmlEntity) object;
		try {
			conn =  dbMgr.initDB();
			// sta = (Statement) conn.createStatement();
			pst = conn.prepareStatement(sql);

			pst.setString(1, html.getUrl());
			pst.setString(2, html.getHtml());
			pst.setLong(3, html.getTime());
			pst.setString(4, html.getQueryWord());


			
			pst.addBatch();
			pst.executeBatch();
			System.out.println("===Save Html With QueryWord Success===");

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
//			dbMgr.closeDBBatch(pst, conn);
		}

		return null;
	}

	@Override
	public void queryDB() {
	}

	public String getSql() {
		return sql;
	}

	public void setSql(String sql) {
		this.sql = sql;
	}
}
