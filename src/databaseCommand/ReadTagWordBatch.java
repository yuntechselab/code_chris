package databaseCommand;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.ResultSet;
import com.mysql.jdbc.Statement;

import tools.files.LoadProps;
import entity.HtmlEntity;
import entity.News;

public class ReadTagWordBatch extends ReadCommand {
	private Object object;

	public ReadTagWordBatch(Object object) {
		this.object = object;
	}

	@Override
	public Object processResult() {
		// TODO Auto-generated method stub

		Map<String, String> tagWordMap = new HashMap<>();
		try {
			while (rs.next()) {
				tagWordMap
						.put(rs.getString("tagWord"), rs.getString("tagType"));

			}
			System.out.println(tagWordMap);

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			// dbMgr.closeDB(rs, sta, conn);
		}

		return tagWordMap;
	}

	@Override
	public void queryDB() {
		// TODO Auto-generated method stub
		List<String> tagWordArray = (List<String>) object;

		try {

			conn = dbMgr.initDB();
			sta = conn.createStatement();

			StringBuilder sql = new StringBuilder();
			sql.append("select * from "
					+ LoadProps.getProperties("tagWordTableName")
					+ " where tagWord in(");
			for (int i = 0; i < tagWordArray.size(); i++) {
				sql.append("?");
				if (i + 1 < tagWordArray.size()) {
					sql.append(",");
				}
			}
			sql.append(")");
			System.out.println(sql.toString());

			PreparedStatement stm = conn.prepareStatement(sql.toString());
			for (int i = 0; i < tagWordArray.size(); i++) {
				stm.setString(i + 1, tagWordArray.get(i));
			}

			rs = stm.executeQuery();

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
