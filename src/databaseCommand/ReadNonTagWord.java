package databaseCommand;

import java.sql.SQLException;
import java.util.ArrayList;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.ResultSet;
import com.mysql.jdbc.Statement;

import tools.files.LoadProps;
import entity.News;

public class ReadNonTagWord extends ReadCommand {
	private String nonTagWord;

	public ReadNonTagWord(String nonTagWord) {
		this.nonTagWord = nonTagWord;
	}

	@Override
	public Object processResult() {
		// TODO Auto-generated method stub
		String tagType = null;
		try {
			while (rs.next()) {
				tagType = rs.getString("nonTagWord");
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			// dbMgr.closeDB(rs, sta, conn);
		}

		return tagType;
	}

	@Override
	public void queryDB() {
		// TODO Auto-generated method stub

		try {
			conn =  dbMgr.initDB();
			sta =  conn.createStatement();
			String sql = "SELECT nonTagWord FROM "
					+ LoadProps.getProperties("nonTagWordTableName")
					+ " Where nonTagWord = '" + nonTagWord + "'";
			rs = sta.executeQuery(sql);

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
