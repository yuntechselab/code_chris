package databaseCommand;

import tools.files.LoadProps;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.PreparedStatement;

import entity.*;

public class SaveTagWord extends SaveCommand {
	String sql = "INSERT INTO "+LoadProps.getProperties("tagWordTableName")+" (tagWord,tagType) VALUES(?,?)";
	@Override
	public Object processResult() {

		TagWord tagWord= (TagWord) object;
		try {
			conn =  dbMgr.initDB();
			// sta = (Statement) conn.createStatement();
			pst =  conn.prepareStatement(sql);

			pst.setString(1, tagWord.getTagWord());
			pst.setString(2, tagWord.getTagType());
			

			pst.addBatch();
			pst.executeBatch();
			System.out.println("===Save Tag Word Success===");
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
//			dbMgr.closeDBBatch(pst, conn);
		}

		return null;
	}

	@Override
	public void queryDB() {
	}

	public String getSql() {
		return sql;
	}

	public void setSql(String sql) {
		this.sql = sql;
	}
}
