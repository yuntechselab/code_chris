package database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import tools.files.LoadProps;

/**
 * Created by Jason on 2016/4/3.
 */
public class DbMgr {
	public static DbMgr db;

	public Connection initDB() {
		Connection conn = null;

		try {
			Class.forName("com.mysql.jdbc.Driver");
			// String url = "jdbc:mysql://140.125.84.57:3306/yahoo_news";
			String url = LoadProps.getProperties("url")+LoadProps.getProperties("databaseName")+"?useUnicode=true&characterEncoding=utf8";
			conn = DriverManager.getConnection(url, "root", "root");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return conn;
	}

	public static synchronized DbMgr getDbCon() {
		if (db == null) {
			db = new DbMgr();
		}
		return db;

	}

	public void closeDB(Statement sta, Connection conn) {
		try {

			sta.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void closeDB(ResultSet rs, Statement sta, Connection conn) {
		try {
			rs.close();
			sta.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void closeDBBatch(PreparedStatement pst, Connection conn) {
		try {
			pst.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
