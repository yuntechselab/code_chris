package database;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import databaseCommand.*;
import entity.News;
import entity.WikiTermBean;

public class DbInvoker {
	public Map<String, String> readTagWordBatch(List<String> termList) {
		SqlImpCmd sqlImpCmd = new ReadTagWordBatch(termList);
		sqlImpCmd.execute();
		return  (Map<String, String>) sqlImpCmd.getObject();
	}
	public List<String> readNonTagWordBatch(List<String> termList) {
		SqlImpCmd sqlImpCmd = new ReadNonTagWordBatch(termList);
		sqlImpCmd.execute();
		return  (List<String>) sqlImpCmd.getObject();
	}
	
	public String readHtmlWithQueryWord(String queryWord) {
		SqlImpCmd sqlImpCmd = new ReadHtmlWithQueryWord(queryWord);
		sqlImpCmd.execute();
		return (String) sqlImpCmd.getObject();
	}	
	public boolean saveHtmlWithQueryWord(Object object) {
		SqlImpCmd sqlImpCmd = new SaveHtmlWithQueryWord();
		sqlImpCmd.setObject(object);
		sqlImpCmd.execute();
		return false;
	}
	
	public boolean saveSql(String sql) {
		SqlImpCmd sqlImpCmd = new SaveSqlList();
		sqlImpCmd.setObject(sql);
		sqlImpCmd.execute();
		return false;
	}
	public Map<String , WikiTermBean> readAnswerWord(String queryWord) {
		SqlImpCmd sqlImpCmd = new ReadAnswerWord(queryWord);
		sqlImpCmd.execute();
		return (Map<String , WikiTermBean>) sqlImpCmd.getObject();
	}
	
	
	public String readHtml(String _url) {
		SqlImpCmd sqlImpCmd = new ReadHtml(_url);
		sqlImpCmd.execute();
		return (String) sqlImpCmd.getObject();
	}	
	public boolean saveHtml(Object object) {
		SqlImpCmd sqlImpCmd = new SaveHtml();
		sqlImpCmd.setObject(object);
		sqlImpCmd.execute();
		return false;
	}
	public boolean saveNonTagWord(Object object) {
		SqlImpCmd sqlImpCmd = new SaveNonTagWord();
		sqlImpCmd.setObject(object);
		sqlImpCmd.execute();
		return false;
	}
	public String readNonTagWord(String nonTagWord) {
		SqlImpCmd sqlImpCmd = new ReadNonTagWord(nonTagWord);
		sqlImpCmd.execute();
		return (String) sqlImpCmd.getObject();
	}
	
	public boolean saveTagWord(Object object) {
		SqlImpCmd sqlImpCmd = new SaveTagWord();
		sqlImpCmd.setObject(object);
		sqlImpCmd.execute();
		return false;
	}
	public String readTagWord(String tagWord) {
		SqlImpCmd sqlImpCmd = new ReadTagWord(tagWord);
		sqlImpCmd.execute();
		return (String) sqlImpCmd.getObject();
	}
	
	
	
	public boolean saveNews(Object object) {
		SqlImpCmd sqlImpCmd = new SaveNews();
		sqlImpCmd.setObject(object);
		sqlImpCmd.execute();
		return false;
	}

	public News readNews(String _url) {
		SqlImpCmd sqlImpCmd = new ReadNews(_url);
		sqlImpCmd.execute();
		return (News) sqlImpCmd.getObject();
	}

	

	public boolean updatePersonAndPlace(Object object) {
		SqlImpCmd sqlImpCmd = new UpdatePersonAndPlace();
		sqlImpCmd.setObject(object);
		sqlImpCmd.execute();
		return false;
	}

	

}
