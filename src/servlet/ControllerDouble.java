﻿package servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.JSONException;
import org.json.JSONObject;

import WeaveNerTools.DetectionEngine;
import WeaveNerTools.Main;
import WeaveWordWeb.PresentationLogic;
import WeaveWordWeb.SearchEngine;
import WeaveWordWeb.SearchEngineDouble;
import entity.WikiTermBean;
import tools.Hashtools;
import yahooNewsTagTools.ChangeCode;

/**
 * Servlet implementation class Controller
 */
public class ControllerDouble extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ControllerDouble() {
		super();
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) throws UnsupportedEncodingException, JSONException {
		String queryWord = "一月二號";

		// queryWord = new String(queryWord.getBytes("ISO-8859-1"), "UTF-8");

		SearchEngineDouble s = new SearchEngineDouble(queryWord);
		Map<String, ArrayList<String>> terms = s.queryTwoLayer();
		for(String test : terms.keySet()){
			System.out.println("Yoooooo "+ test);
		}
//		if (terms != null) {
//			// terms = PresentationLogic.removeNoise(terms);
//			// add top 10
//			// Map<String, WikiTermBean> topTerms =
//			// PresentationLogic.getTop(
//			// terms, 10);
//			JSONObject json = PresentationLogic.getTwoLayerPresentationJson(
//					terms, queryWord);
//			//tes
//		}
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		try {
			search(request, response);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		try {
			search(request, response);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	protected void search(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, JSONException {
		response.setContentType("text/html ; charset=UTF-8");
		try {
			System.out.println("The New Request Start -----!!!");
			HttpSession session = request.getSession(true);
			//
			String queryWord;
			try {
				queryWord = session.getAttribute("queryWord").toString();
				String queryWord1 = request.getParameter("wordInput");
				queryWord1 = new String(queryWord1.getBytes("ISO-8859-1"),
						"UTF-8");
				System.out.println("session:" + queryWord + "querWord1:"
						+ queryWord1);
				if (!queryWord.equals(queryWord1) && !queryWord1.equals("")) {
					queryWord = queryWord1;
				}

			} catch (Exception e) {
				queryWord = request.getParameter("wordInput");// queryWord
				queryWord = new String(queryWord.getBytes("ISO-8859-1"),
						"UTF-8");
			}

			session.setAttribute("queryWord", queryWord);

			// =========20160818=========
			// queryWord = ChangeCode.toLong(queryWord);
			SearchEngineDouble s = new SearchEngineDouble(queryWord);
			Map<String, ArrayList<String>> twoLayersTerms = s.queryTwoLayer();
			Map<String, WikiTermBean> terms = s.getSearchResult();

			// =========20160818=========

			if (twoLayersTerms != null) {
				terms = PresentationLogic.removeNoise(terms);
				// add top 10
				Map<String, WikiTermBean> topTerms = PresentationLogic.getTop(
						terms, 10);
				JSONObject json = PresentationLogic
						.getTwoLayerPresentationJson(twoLayersTerms, queryWord);
				JSONObject jsonAcceptance = PresentationLogic
						.getAcceptanceLogicJson(terms);
				// queryWord = PresentationLogic.getQueryWordAndType(queryWord,
				// type);
				session.setAttribute("JSON", json);
				session.setAttribute("JSONAcceptance", jsonAcceptance);
				String finalQueryWord = "";
				// String type =
				// DetectionEngine.getProperType(queryWord);//URLEncoder.encode();
				// //原始方法(只比對「公司」)
				DetectionEngine DetectionEngine = new DetectionEngine();
				// getType
				String type = DetectionEngine.getTypeFromWikiWoDb(queryWord);// URLEncoder.encode();
				// //用InfoBox
				// 和 開放類別 比對
				if (!(type.equals(""))) {
					finalQueryWord = queryWord + " " + type ; 		// 原始方法
																	// finalQueryWord
																	// =
						                   											// queryWord
																	// +
																	// "(組織名)";
				} else {
					finalQueryWord = Main.parseRuleForSingleTerm(queryWord);
				}

				//

				//
				session.setAttribute("finalQueryWord", finalQueryWord);

				PrintWriter out = response.getWriter();
				out.println("<html>");
				out.println("<script type=\"text/javascript\">");
				// out.println("window.open (\"http://140.125.84.47:8080/Wiki/acceptance.jsp\",\"aa\");");
				out.println("</script>");
				RequestDispatcher dispatcher = request
						.getRequestDispatcher("/index.jsp");
				dispatcher.include(request, response);
				out.println("</html>");
				session.setAttribute("alreadyAcceptance", "false");
				// request.getRequestDispatcher("index_main.html").forward(request,
				// response);
				// response.sendRedirect("index_main.html");
			} else {
				Enumeration em = request.getSession().getAttributeNames();
				while (em.hasMoreElements()) {
					request.getSession().removeAttribute(
							em.nextElement().toString());
				}
				PrintWriter out = response.getWriter();
				out.println("<html>");
				out.println("<script type=\"text/javascript\">");
				out.println("</script>");
				out.println("<h2 style=color:red> 查無資料!!!</p>");
				RequestDispatcher dispatcher = request
						.getRequestDispatcher("/index.jsp");
				dispatcher.include(request, response);
				out.println("</html>");
				System.out.println("The New Request End -----!!!");
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
