package servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import selab.initialapex.NewsTools;
import socket.SearchEngine;
import synonymResearch.WikiTermBean_backup;
import thesis.Sinica;
import tools.*;

import com.go.DetectionEngine;
import com.go.FetchOperation;

public class ControllerNews extends HttpServlet {
	private static final long serialVersionUID = 1L;

	 String newsUrl = "https://tw.news.yahoo.com/%E7%B2%BE%E9%9D%88%E5%AF%B6%E5%8F%AF%E5%A4%A2%E5%A4%AF%E5%85%A8%E5%8F%B0-%E9%90%B5%E9%99%B8%E7%A9%BA%E7%A6%81%E7%8E%A9-215003526.html";
	 Set<String> termList = new LinkedHashSet<String>();
	 String contentText = "";
	 String contentHTML = "";
	 String contentRelatedNews = "";
	 String finalResult = "";
	 List<String> result = new ArrayList<String>();
	 int keyLength = 2;

	public ControllerNews() {
		super();
		// TODO Auto-generated constructor stub
	}

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		try {
			ControllerNews cn = new ControllerNews();
			cn.extractNews(request, response);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static void main(String[] args) throws Exception {

	}

	// protected void doPost(HttpServletRequest request,
	// HttpServletResponse response) throws ServletException, IOException {
	// try {
	// ControllerNews cn =new ControllerNews();
	// cn.extractNews(request, response);
	// } catch (Exception e) {
	// // TODO Auto-generated catch block
	// e.printStackTrace();
	// }
	// }

	// protected static void doPost(HttpServletRequest request,
	// HttpServletResponse response) throws ServletException, IOException

	protected void extractNews(HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		response.setContentType("text/html ; charset=UTF-8");
		String queryWord = request.getParameter("wordInput");// queryWord
		queryWord = URLEncoder.encode(queryWord, "UTF-8");
		newsUrl = "https://tw.news.yahoo.com/" + queryWord + ".html";

		// initialize
		contentRelatedNews = "";

		try {
			PrintWriter out = response.getWriter();

			// check() isExist url
			crawlNews();
			ckipAndSegment();
			 wikiSearch();
//			wikiDetect();
			removeDuplicate(); // 去重複
			longterm(); // 取長詞

			replaceWord();

			// System.out.println("termList: " + termList);
			// System.out.println("Result: " + result);
			// System.out.println("content: " + contentText);
			// System.out.println("content2: " + finalResult);

			out.println(finalResult + contentRelatedNews);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void longterm() {
		// TODO Auto-generated method stub

		List<String> resultcompare = new ArrayList<String>(
				Arrays.asList(new String[result.size()]));
		Collections.copy(resultcompare, result);
		for (String string1 : resultcompare) {
			for (int i = 0; i < result.size(); i++) {
				if (string1.indexOf(result.get(i)) > -1
						&& !string1.equals(result.get(i))) {
					// System.out.println(result.get(i)+":"+string1);
					result.remove(i);
				}
			}
		}
	}

	private void replaceWord() {
		// TODO Auto-generated method stub
		finalResult = contentHTML;
		for (String string : result) {
			finalResult = finalResult
					.replaceAll(
							string,
							"<a href=https://140.125.84.47/Wiki/jump.jsp?&wordInput="
									+ string
									+ " target=_blank555"
									+ " onclick=window.open(this.href,'_blank555','location=no,width=700,hight=600'); return false;"
									+ ">" + string + "</a>");
			// finalResult = finalResult.replaceAll(string,
			// "<a href=https://140.125.84.47/Wiki/jump.jsp?&wordInput="
			// + string + " target=_new" + ">" + string + "</a>");
		}
	}

	private void wikiDetect() {
		System.out.println("--------------\n");
		for (String key : termList) {
			// System.out.println(key+key.length());
			if (key.length() >= keyLength) {
				String type = DetectionEngine.getType(key);
				if (type != null && !isNumeric(key) && type != ""&&!type.isEmpty()&&!type.equals("")) {
					result.add(key);
				}
			}
		}
	}

	private void wikiSearch() {
		// TODO Auto-generated method stub
		for (String key : termList) {
			// System.out.println(key+key.length());
			if (key.length() >= keyLength) {
				SearchEngine s = new SearchEngine(key);
				Map<String, WikiTermBean_backup> terms = s.getSearchResult();
				if (terms != null && !isNumeric(key)) {
					result.add(key);
				}
			}
		}
	}

	public boolean isNumeric(String str) {
		Pattern pattern = Pattern.compile("[0-9]*");
		Matcher isNum = pattern.matcher(str);
		if (!isNum.matches()) {
			return false;
		}
		return true;
	}

	private void ckipAndSegment() {
		// TODO Auto-generated method stub
		// ckip
		System.out.println("contentText: " + contentText);

		Sinica sinica = Sinica.getInstance();
		List<String> contentPOS = sinica.segment(contentText).getUwpos();
		System.out.println("contentPOS: " + contentPOS);

		// segement
		for (String sentence : contentPOS) {
			termList.addAll(segement(sentence));
		}
	}

	private void crawlNews() throws Exception {
		// TODO Auto-generated method stub
		newsUrl = new String(newsUrl.getBytes("ISO-8859-1"), "UTF-8");
		System.out.println(newsUrl);

		Document doc = FetchOperation
				.getDocumentFromURL(newsUrl, "UTF-8", true);

		if (doc.select("error").size() != 0) {
			System.out.println("錯誤!沒有此頁面");
		} else {
			// Get title
			Element title = doc.select("h1.headline").get(0);// h1.headline
			// Get content
			// Element content = doc.select("#mediaarticlebody > div > p");
			contentText = doc.select(".yom-art-content P").text();

			// 切割更多相關新聞
			contentHTML = doc.select(".yom-art-content").html();
			try {
				contentRelatedNews = contentHTML.substring(contentHTML
						.indexOf("更多相關新聞"));
				contentHTML = contentHTML.substring(0,
						contentHTML.indexOf("更多相關新聞"));

			} catch (Exception e) {

			}
			System.out.println("Title: " + title.text());
			System.out.println("Content: " + contentHTML);
		}
	}

	public List segement(String ckipResult) {
		// TODO Auto-generated method stub
		String rowToken = "\n----------------------------------------------------------------------------------------------------------------------------------\n";
		String rowToken2 = "----------------------------------------------------------------------------------------------------------------------------------";
		String splitHeadToken = "(";
		String splitTailToken = ")";
		List<String> result = new ArrayList<>();
		// System.out.println(article.getCkipResult());
		for (String s : ckipResult.split("　")) {
			String term = "";
			String pos = "";
			s = s.replace(rowToken2, "");
			String checkToken = s.substring(0, s.length());
			// System.out.println("checkToken: "+checkToken);

			if (checkToken.indexOf(splitHeadToken) != -1) {
				term = s.substring(0, s.indexOf(splitHeadToken)).trim();
				pos = s.substring(s.indexOf(splitHeadToken), s.length());
				pos = pos.replace(splitHeadToken, "");
				pos = pos.replace(splitTailToken, "");
				// System.out.println("term: "+term);
				// System.out.println("pos: "+pos);

				// article.getPosList().add(pos);
				// article.getTermList().add(term);
				result.add(term);
				checkToken = "";
			}
		}
		// System.out.println(article.getPosList());
		// System.out.println(article.getTermList());
		return result;
	}

	public void removeDuplicate() {
		HashSet h = new HashSet(result);
		result.clear();
		result.addAll(h);
	}
}
