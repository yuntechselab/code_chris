package servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.JSONException;
import org.json.JSONObject;

import com.go.DetectionEngine;

import socket.SearchEngine;
import synonymResearch.PresentationLogic;
import synonymResearch.Test;
import synonymResearch.WikiTermBean_backup;
/**
 * Servlet implementation class Controller
 */
public class ControlerMobile extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ControlerMobile() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			search (request , response);
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			search(request , response);
	}
	
	protected void search(HttpServletRequest request, HttpServletResponse response) throws ServletException{
		response.setContentType("text/html ; charset=UTF-8");
		try {
			HttpSession session = request.getSession(true);
			String queryWord = request.getParameter("wordInput");//queryWord
			System.out.println(queryWord);
//			queryWord = new String( queryWord.getBytes( "ISO-8859-1" ) ,  "UTF-8" ) ;
//			queryWord =  new String(queryWord.getBytes("ISO-8859-1"), "UTF-16LE");
//			System.out.print(queryWord);
			
			//test
//			String type = DetectionEngine.getProperType(URLEncoder.encode(queryWord, "UTF-8"));
//			System.out.println("type:" + type);
			
			session.setAttribute("queryWord", queryWord);
			//test
			String tmpS = java.net.URLEncoder.encode(queryWord,"UTF-8");
//			SearchEngine s = new SearchEngine(tmpS);
			System.out.println(queryWord + "dododo");
			//
			SearchEngine s = new SearchEngine(queryWord);
			Map<String ,WikiTermBean_backup> terms =  s.getSearchResult();
			terms = PresentationLogic.removeNoise(terms);
			JSONObject json = PresentationLogic.getPresentationJson(terms);
			JSONObject jsonAcceptance = PresentationLogic.getAcceptanceLogicJson(terms);
//			String type = DetectionEngine.getProperType(URLEncoder.encode(queryWord, "UTF-8"));
//			queryWord = PresentationLogic.getQueryWordAndType(queryWord, type);
			session.setAttribute("JSON", json);
			session.setAttribute("JSONAcceptance", jsonAcceptance);
			session.setAttribute("finalQueryWord", queryWord);
			
			PrintWriter out = response.getWriter();   
			out.println("<html>"); 
			session.setAttribute("alreadyAcceptance", "false");
			out.println("<script type=\"text/javascript\">");  
			//out.println("window.open (\"http://140.125.84.47:8080/Wiki/acceptance.jsp\",\"aa\");"); 
			out.println("</script>"); 
			
			RequestDispatcher dispatcher = request.getRequestDispatcher("/mobile_search.jsp");
			dispatcher.include(request, response);
			out.println("</html>");
//			request.getRequestDispatcher("index_main.html").forward(request, response);
//			response.sendRedirect("index_main.html");
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	

	

}
