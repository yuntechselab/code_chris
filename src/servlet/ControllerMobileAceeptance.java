package servlet;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.NumberFormat;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.JSONException;
import org.json.JSONObject;

import databaseTools.OurSqlConnectionSinglton;
import synonymResearch.Store;

/**
 * Servlet implementation class ControllerMobileAceeptance
 */
public class ControllerMobileAceeptance extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ControllerMobileAceeptance() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		update(request , response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		update(request , response);
	}
	
	protected void update(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String accept ="on";
		int index =  Integer.parseInt(request.getParameter("hiddenIndex"));
//		System.out.println("Index:" + index);
		String queryWord =  request.getParameter("queryWord");
//		System.out.println("queryWord:" + queryWord);
		ArrayList<String> sqlList = new ArrayList<String>();
		ArrayList<String> tmpMappingWord = new ArrayList<String>();
		for(int i =1; i<=index ;i++){
			String mappingWord = request.getParameter("hiddenMappingWord" + i);
			tmpMappingWord.add(mappingWord);
			String s = "term" + i;
			String decideS = request.getParameter(s);
//			System.out.println(i + ":" + mappingWord +" decideS:" + decideS );
			String sql ="";
			if(decideS != null && decideS.equals(accept)){
				 sql = "UPDATE answerword AS A , (SELECT * FROM answerword WHERE queryWord ='" + queryWord + "' AND mappingWord ='" + mappingWord +"') AS B SET A.agreeNumber = B.agreeNumber + 1 , A.totalNumber = B.totalNumber +1 WHERE A.queryWord ='" + queryWord +"' AND A.mappingWord='" + mappingWord +"';";
			}else{
				 sql = "UPDATE answerword AS A , (SELECT * FROM answerword WHERE queryWord ='" + queryWord + "' AND mappingWord ='" + mappingWord +"') AS B SET A.totalNumber = B.totalNumber +1 WHERE A.queryWord ='" + queryWord +"' AND A.mappingWord='" + mappingWord +"';";
			}
			sqlList.add(sql);
		}
		Store.writeIntoDB(sqlList); //updateSQL
		HttpSession session = request.getSession(true);
		session.setAttribute("alreadyAcceptance", "true");
		//for real time update --------------------------------------
				String tmpS = session.getAttribute("JSONAcceptance").toString();
				JSONObject json = null;
				try {
					json = new JSONObject(tmpS);
				} catch (JSONException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				OurSqlConnectionSinglton dB = OurSqlConnectionSinglton.getInstance();
				ArrayList<String> sqlList2= new ArrayList<String>();
				NumberFormat nf = NumberFormat.getInstance();
				nf.setMaximumFractionDigits( 2 );  //設定小數點兩位
				for(String s : tmpMappingWord){
					String sql = "Select agreeNumber , totalNumber From answerword Where queryWord ='" + queryWord + "' AND mappingWord ='" + s + "'";
//					System.out.println(sql);
					dB.setSql(sql);
					ResultSet result = dB.getResultSet();
					try {
						result.next();
					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					int agreeNumber ;
					int totalNumber ;
					try {
						agreeNumber = result.getInt("agreeNumber");
						totalNumber = result.getInt("totalNumber");
						String reasonability = nf.format(((double)(agreeNumber) / totalNumber)*100) + "%";//更新接受度
						sqlList2.add("UPDATE answerword SET reasonability='"+ reasonability + "' WHERE queryWord ='" + queryWord +"' AND mappingWord='" + s +"';");
						JSONObject tmpJ = null;
						try {
							tmpJ = json.getJSONObject(s);
							tmpJ.put("agreeNumber", agreeNumber);
							tmpJ.put("totalNumber", totalNumber);
							tmpJ.put("reasonability",reasonability );
							json.put(s, tmpJ);
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				Store.writeIntoDB(sqlList2);
				session.setAttribute("JSONAcceptance", json);
				//-----------------------------------------------
				try {
					response.sendRedirect("mobile_search.jsp");
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	}
}
