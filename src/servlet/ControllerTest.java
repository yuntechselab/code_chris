package servlet;

import thesis.Main;

import com.go.DetectionEngine;

public class ControllerTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String queryWord = "王王王王";
		String finalQueryWord = "";
		// String type =
		// DetectionEngine.getProperType(queryWord);//URLEncoder.encode();
		// //原始方法(只比對「公司」)
		String type = DetectionEngine.getType(queryWord);// URLEncoder.encode();
															// //用InfoBox
															// 和 開放類別 比對
		System.out.println("type: "+ type);
		if (!(type.equals(""))) {
			finalQueryWord = queryWord + "(" + type + ")"; // 原始方法
															// finalQueryWord
															// =
															// queryWord
															// +
															// "(組織名)";
		} else {
			finalQueryWord = Main.parseRuleForSingleTerm(queryWord);
		}
		System.out.println("finalQueryWord: " + finalQueryWord);
	}

}
