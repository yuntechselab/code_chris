package servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import selab.initialapex.NewsTools;
import socket.SearchEngine;
import synonymResearch.WikiTermBean_backup;
import thesis.Sinica;
import tools.*;
import yahooNewsTag.CrawlYahooNews;
import yahooNewsTag.YahooNewsTag;

import com.go.DetectionEngine;
import com.go.FetchOperation;

public class ControllerYahooNews extends HttpServlet {
	private static final long serialVersionUID = 1L;

	String newsUrl = "https://tw.news.yahoo.com/%E7%B2%BE%E9%9D%88%E5%AF%B6%E5%8F%AF%E5%A4%A2%E5%A4%AF%E5%85%A8%E5%8F%B0-%E9%90%B5%E9%99%B8%E7%A9%BA%E7%A6%81%E7%8E%A9-215003526.html";
	Set<String> termList = new LinkedHashSet<String>();
	String contentText = "";
	String contentHTML = "";
	String contentRelatedNews = "";
	String finalResult = "";
	List<String> result = new ArrayList<String>();
	int keyLength = 2;

	public ControllerYahooNews() {
		super();
		// TODO Auto-generated constructor stub
	}

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		try {
			ControllerYahooNews cn = new ControllerYahooNews();
			cn.extractNews(request, response);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static void main(String[] args) throws Exception {

	}

	// protected void doPost(HttpServletRequest request,
	// HttpServletResponse response) throws ServletException, IOException {
	// try {
	// ControllerNews cn =new ControllerNews();
	// cn.extractNews(request, response);
	// } catch (Exception e) {
	// // TODO Auto-generated catch block
	// e.printStackTrace();
	// }
	// }

	// protected static void doPost(HttpServletRequest request,
	// HttpServletResponse response) throws ServletException, IOException

	protected void extractNews(HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		response.setContentType("text/html ; charset=UTF-8");
		String queryWord = request.getParameter("wordInput");// queryWord
		queryWord = URLEncoder.encode(queryWord, "UTF-8");
		newsUrl = "https://tw.news.yahoo.com/" + queryWord + ".html";

		// initialize
		contentRelatedNews = "";

		try {
			PrintWriter out = response.getWriter();

			YahooNewsTag yahooNewsTag = new YahooNewsTag(newsUrl);
			finalResult = yahooNewsTag.tagYahooNews();
			
			out.println(finalResult + contentRelatedNews);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
