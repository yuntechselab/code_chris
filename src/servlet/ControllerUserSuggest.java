package servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import databaseTools.OurSqlConnectionSinglton;
import synonymResearch.Store;

/**
 * Servlet implementation class ControllerUserSuggest
 */
public class ControllerUserSuggest extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ControllerUserSuggest() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		update(request , response);
		htmlPage(request , response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		update(request , response);
		htmlPage(request , response);
	}
	
	protected void htmlPage(HttpServletRequest request, HttpServletResponse response){
		PrintWriter out;
		try {
			out = response.getWriter();
			out.println("<html>");
			out.println("<h2 style=color:red>SuggestWords Successful !!!</h2>");
			out.println("</htnl>");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}   
		
	}
	protected void update(HttpServletRequest request, HttpServletResponse response){
		System.out.println("Do suggest update ...");
		String queryWord = request.getParameter("hiddenQueryWord");
		int index = Integer.parseInt(request.getParameter("hiddenIndex"));
		ArrayList<String> sqlList = new ArrayList<String>();
		OurSqlConnectionSinglton dB = OurSqlConnectionSinglton.getInstance();
		dB.setSql("Select * from " + socket.SearchEngine.searchTableName + " Where queryWord = '" +queryWord+"'");
		ResultSet result = dB.getResultSet();
		System.out.print("done");
		for(int i=1; i<=index; i++){
			String tmpSuggestWord = request.getParameter("suggestWord" + i);
//			System.out.println(tmpSuggestWord);
			if(tmpSuggestWord.equals("")){
//				System.out.println("isEmpty");
			}else{
				if(!(this.isExistDB(result , tmpSuggestWord))){
					String sql = "Insert into answerword(queryWord,mappingWord,mappingRule,reasonability,openCategory,mappingTime,SimpleID,totalNumber,agreeNumber) values('"+ queryWord + "','" + tmpSuggestWord + "','userSuggest','0%','[]','0','54321','0','0')";
					sqlList.add(sql);
				}
			}
		}
//		System.out.println("doing DB...");
//		Store.writeIntoDB(sqlList);
//		System.out.println("End suggest update...");
	}
	protected boolean isExistDB(ResultSet result , String word){
		try {
			result.first();//指標回到第一個
			result.previous();//指標再往前一格
			while(result.next()){
				String DBWord = result.getString("mappingWord");
				if(DBWord.equals(word)){
					return true;
				}
//				System.out.println(result.getString("mappingWord"));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}

}
