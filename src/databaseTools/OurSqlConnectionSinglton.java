 

package databaseTools;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;

import tools.files.LoadProps;

public class OurSqlConnectionSinglton {
    private String sql;
    private static Connection conn;
    private static Statement stat;
    private static String databaseName ="selab";
    private static String databseTable;
    private static OurSqlConnectionSinglton dB;
    public static HashMap<String, Integer> tempStr; //Dirty
    
 public static OurSqlConnectionSinglton getInstance(){
	 if(dB != null){
		return dB;
	 }
	 dB = new OurSqlConnectionSinglton(databaseName);
	 return dB;
 }

    public static String getDatabaseName() {
        return databaseName;
    }

    public static void setting(String databaseName,String databseTable) {
        OurSqlConnectionSinglton.databaseName = databaseName;
        OurSqlConnectionSinglton.databseTable = databseTable;
    }

    public static String getDatabseTable() {
        return databseTable;
    }

    private OurSqlConnectionSinglton(String databaseName) {
    
	try {
	    this.databaseName = databaseName;
	   
	    Class.forName("com.mysql.jdbc.Driver");
	     String connStr = LoadProps.getProperties("url") + databaseName + "?useUnicode=true&characterEncoding=utf-8&user="+LoadProps.getProperties("user")+ "&password="+LoadProps.getProperties("password") ;
//	     String connStr = "jdbc:mysql://140.125.84.58/" + databaseName + "?useUnicode=true&characterEncoding=utf-8&user=root&password=selab";
	    //String connStr = "jdbc:mysql://140.125.84.45/" + databaseName + "?useUnicode=true&characterEncoding=utf-8&user=root&password=selab";
	  
	     conn = DriverManager.getConnection(connStr);
	    
	    stat = conn.createStatement();
	   
//	    System.out.println(connStr);
	   

	} catch (ClassNotFoundException e) {
	    System.out.println("找不到驅動程式");
	} catch (SQLException e) {
	    System.out.println(e.toString());	    	    
	  
	  
	}
	
	
	
    }

    public void setSql(String sql) {
	this.sql = sql;
    }
    
    public int updateData() {
	try {
	    return stat.executeUpdate(sql);
	} catch (SQLException e) {
	    // TODO Auto-generated catch block
	    System.err.println(e.toString());
//	    System.exit(1);
	   
	    return 0;
	}
    }

    public ResultSet getResultSet() {
	try {
	    return stat.executeQuery(sql);
	} catch (SQLException e) {
	    System.out.println(e.toString());
	    return null;
	}
    }

    public void InsertNew(String sql) {
	this.sql = sql;
	try {
	    Statement aStatement = conn.createStatement();
	    aStatement.executeUpdate(sql);
	} catch (SQLException e) {
	    System.out.println(e.toString());
//	    System.exit(1);
	}
	finally{
	    
		try {  stat.cancel(); } catch (Exception e) {};
	    try {  conn.clearWarnings(); } catch (Exception e) {};
		}

    }
    public static void close(){
    	try {
    		stat.close();
			conn.close();
			dB = null;
//			System.out.println("connect close");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("connect close error");
		}
    }
}
