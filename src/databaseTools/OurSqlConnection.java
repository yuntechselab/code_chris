 

package databaseTools;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;

import tools.files.LoadProps;

public class OurSqlConnection {
    private String sql;
    private static Connection conn;
    private static Statement stat;
    private static String databaseName;
    private static String databseTable;
    public static HashMap<String, Integer> tempStr; //Dirty
    
    
 

    public static String getDatabaseName() {
        return databaseName;
    }

    public static void setting(String databaseName,String databseTable) {
        OurSqlConnection.databaseName = databaseName;
        OurSqlConnection.databseTable = databseTable;
    }

    public static String getDatabseTable() {
        return databseTable;
    }

    public OurSqlConnection(String databaseName) {
    
	try {
	    this.databaseName = databaseName;
	   
	    Class.forName("com.mysql.jdbc.Driver");
//	     String connStr = "jdbc:mysql://140.125.84.49:3306/" + databaseName + "?useUnicode=true&characterEncoding=utf-8&user=root&password=root";
	     String connStr = LoadProps.getProperties("url") + databaseName + "?useUnicode=true&characterEncoding=utf-8&user="+LoadProps.getProperties("user")+"&password="+LoadProps.getProperties("password");

	     //	     String connStr = "jdbc:mysql://140.125.84.58/" + databaseName + "?useUnicode=true&characterEncoding=utf-8&user=root&password=selab";
	    //String connStr = "jdbc:mysql://140.125.84.45/" + databaseName + "?useUnicode=true&characterEncoding=utf-8&user=root&password=selab";
	  
	     conn = DriverManager.getConnection(connStr);
	    
	    stat = conn.createStatement();
	   
//	    System.out.println(connStr);
	   

	} catch (ClassNotFoundException e) {
	    System.out.println("找不到驅動程式");
	} catch (SQLException e) {
	    System.out.println(e.toString());	    	    
	  
	  
	}
	
	
	
    }

    public void setSql(String sql) {
	this.sql = sql;
    }
    
    public int updateData() {
	try {
	    return stat.executeUpdate(sql);
	} catch (SQLException e) {
	    // TODO Auto-generated catch block
	    System.err.println(e.toString());
//	    System.exit(1);
	   
	    return 0;
	}
    }

    public ResultSet getResultSet() {
	try {
	    return stat.executeQuery(sql);
	} catch (SQLException e) {
	    System.out.println(e.toString());
	    return null;
	}
    }

    public void InsertNew(String sql) {
	this.sql = sql;
	try {
	    Statement aStatement = conn.createStatement();
	    aStatement.executeUpdate(sql);
	} catch (SQLException e) {
	    System.out.println(e.toString());
//	    System.exit(1);
	}
	finally{
	    
		try {  stat.cancel(); } catch (Exception e) {};
	    try {  conn.clearWarnings(); } catch (Exception e) {};
		}

    }
    public void close(){
    	try {
			conn.close();
//			System.out.println("connect close");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("connect close error");
		}
    }
}
