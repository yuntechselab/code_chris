package yahooNewsTag;

import java.io.UnsupportedEncodingException;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import database.DbInvoker;
import tools.files.LoadProps;
import yahooNewsTagTools.ChangeCode;
import yahooNewsTagTools.FetchOperation;
import entity.News;

public class CrawlYahooNews {
	String content = "";
	String htmlContent = "";
	String _htmlRelatedNewsContent = "";

	public News crawlYahooNews(String newsUrl) throws Exception {
		newsUrl = new String(newsUrl.getBytes("ISO-8859-1"), "UTF-8");
		// System.out.println(newsUrl);
		News news = new News();

		Document doc = Jsoup.connect(newsUrl).timeout(100000).get();
		String html = doc.html();
		html = ChangeCode.toLong(doc.html());

		if (doc.select("error").size() != 0) {
			System.out.println("錯誤!沒有此頁面");
		} else {

			// Get time
			String txt = doc.select(".byline>abbr").text();
			String date = txt.split(" ")[0].replace("年", "-").replace("月", "-")
					.replace("日", "");
			String time = transferTime(txt.split(" ")[1]);

			// Get publisher
			String publisher = doc.select(".byline>span").text();
			// Get title
			Element title = doc.select("h1.headline").get(0);// h1.headline
			// Get content
			content = doc.select(".yom-art-content P").text();

			// 切割更多相關新聞
			htmlContent = doc.select(".yom-art-content").html();
			try {
				_htmlRelatedNewsContent = htmlContent.substring(htmlContent
						.indexOf("更多相關新聞"));
				htmlContent = htmlContent.substring(0,
						htmlContent.indexOf("更多相關新聞"));

			} catch (Exception e) {

			}

			System.out.println("newsUrl: " + newsUrl);
			System.out.println("contentText: " + content);
			System.out.println("ContentHtml: " + htmlContent);
			System.out.println("Title: " + title.text());
			System.out.println("publisher: " + publisher);
			System.out.println("date: " + date);
			System.out.println("time: " + time);

			news.set_url(toUtf8(newsUrl));
			news.set_content(toUtf8(content));
			news.set_htmlContent(toUtf8(htmlContent));
			news.set_title(toUtf8(title.text()));
			news.set_publisher(toUtf8(publisher));
			news.set_date(toUtf8(date));
			news.set_time(toUtf8(time));
			news.set_htmlRelatedNewsContent(toUtf8(_htmlRelatedNewsContent));
			
		}
		return news;
	}

	private String transferTime(String input) {
		String result = "";
		String preFix = input.substring(0, input.indexOf("午") + 1);
		String time = input.substring(input.indexOf("午") + 1, input.length());
		String[] timePart = time.split(":");

		int hour = new Integer(timePart[0]);
		String minuteStr = timePart[1];

		if (preFix.equals("下午")) {
			hour += 12;
		}

		String hourString = new Integer(hour).toString();
		if (hourString.length() < 2) {
			hourString = "0" + hourString;
		}

		result = hourString + ":" + minuteStr;
		return result;
	}

	public String toUtf8(String str) {
		try {
			return new String(str.getBytes("UTF-8"), "UTF-8");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
}
