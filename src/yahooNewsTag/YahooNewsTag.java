package yahooNewsTag;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import tools.FileTools;
import yahooNewsMultiThreadTest.WikipediaMultiThread;
import yahooNewsTagTools.NewsTagProcessor;
import yahooNewsTagTools.Wikipedia;
import database.DbInvoker;
import entity.News;

public class YahooNewsTag {
	private String newsUrl;
	private List<String> termList = new ArrayList<String>();
	private String finalResult;
	private Map<String, String> tagWordTypeMap = new HashMap<>();

	public YahooNewsTag(String newsUrl) throws Exception {
		// TODO Auto-generated constructor stub
		this.newsUrl = newsUrl;
	}

	// <p class=person><a>string</a></p>

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		try {
			String url = "https://tw.news.yahoo.com/%E5%90%B3%E6%9D%B1%E9%80%B2%E9%90%B5%E4%BA%86%E5%BF%83%E7%94%A8%E6%9D%8E%E7%B4%80%E7%8F%A0-%E6%96%B0%E5%85%89%E9%87%91%E6%9C%AA%E4%BE%86%E8%A6%81%E4%B8%8A%E6%BC%94-%E9%9B%99%E5%A7%9D%E9%AC%A5-001617774--finance.html";
			YahooNewsTag yahooNewsTag = new YahooNewsTag(url);
			String testFinalResult = yahooNewsTag.tagYahooNews();
			System.out.println(testFinalResult);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public String tagYahooNews() throws Exception {
		long startTime = System.currentTimeMillis();
		System.out.println("tagYahooNews前花了" + (startTime) / 1000.0 + "秒");
		DbInvoker dbInvoker = new DbInvoker();
		
		// read tagContent and htmlRelatedNewsContent to news
		News news = dbInvoker.readNews(newsUrl);
		finalResult = news.get_tagContent() + news.get_htmlRelatedNewsContent();

		// if news.get_tagContent() not exist
		if (news.get_tagContent() == null) {
			// crawl yahoo news
			CrawlYahooNews crawlYahooNews = new CrawlYahooNews();
			news = crawlYahooNews.crawlYahooNews(newsUrl);

			// preprocessing:ckip, segement, longterm
			NewsTagProcessor newsTagProcessor = new NewsTagProcessor();
			termList = newsTagProcessor.ckipAndSegmentAndPreprocessing(news
					.get_content());
			System.out.println("termList: " + termList);

			// ner detect
			Wikipedia wikipedia = new Wikipedia();

			tagWordTypeMap = wikipedia.wikiNerDetect(termList);
			System.out.println("result: " + tagWordTypeMap);

			// replace
			finalResult = newsTagProcessor.replaceWord(news, tagWordTypeMap)
					+ news.get_htmlRelatedNewsContent();
			// save to db
			news.set_tagContent(finalResult);
			dbInvoker.saveNews(news);
			System.out.println(finalResult);
		} else {
			// if tagContent exist
			System.out.println(finalResult);
			System.out.println("tagContent exist");
		}
		long endTime = System.currentTimeMillis();
		System.out.println("tagYahooNews後花了" + (endTime) / 1000.0 + "秒");
		System.out.println("tagYahooNews後共花了" + (endTime - startTime) / 1000.0
				+ "秒");
		
		FileTools.writeText("tagYahooNews後共花了" + (endTime - startTime) / 1000.0
				+ "秒\n", "time.txt", "utf-8", true);
		return finalResult;

	}

	public String tagYahooNewsMultiThread() throws Exception {
		DbInvoker dbInvoker = new DbInvoker();

		// read tagContent and htmlRelatedNewsContent to news
		News news = dbInvoker.readNews(newsUrl);
		finalResult = news.get_tagContent() + news.get_htmlRelatedNewsContent();

		// if news.get_tagContent() not exist
		if (news.get_tagContent() == null) {
			// crawl yahoo news
			CrawlYahooNews crawlYahooNews = new CrawlYahooNews();
			news = crawlYahooNews.crawlYahooNews(newsUrl);

			// preprocessing:ckip, segement, longterm
			NewsTagProcessor newsTagProcessor = new NewsTagProcessor();
			termList = newsTagProcessor.ckipAndSegmentAndPreprocessing(news
					.get_content());
			System.out.println("termList: " + termList);

			// ner detect
			// Wikipedia wikipedia = new Wikipedia();
			WikipediaMultiThread wikipedia = new WikipediaMultiThread();

			tagWordTypeMap = wikipedia.wikiNerDetect(termList);
			System.out.println("result: " + tagWordTypeMap);

			// replace
			finalResult = newsTagProcessor.replaceWord(news, tagWordTypeMap)
					+ news.get_htmlRelatedNewsContent();
			// save to db
			news.set_tagContent(finalResult);
			// dbInvoker.saveNews(news);
			System.out.println(finalResult);
		} else {
			// if tagContent exist
			System.out.println(finalResult);
			System.out.println("tagContent exist");
		}
		return finalResult;

	}
}
