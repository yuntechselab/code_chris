package WeaveNerTools;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import entity.Sentence;
import entity.Term;

public class FindAllSatisfyTerm {
	private static FindAllSatisfyTerm instance;
	private FindAllSatisfyTerm() {
		
	}
	public static FindAllSatisfyTerm getInstance() {
		if(instance == null) {
			instance = new FindAllSatisfyTerm();
		}
		return instance;
	}
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		// TODO Auto-generated method stub
		String[] rules = new String[] {
				"Na+Nc+Nb",
				"(Na+Nc+Nb)",
				"Nc+(Na+Nc+Nb)",
				"(Na+Nc+Nb)+VE",
				"Nc+(Na+Nc+Nb)+VE"
		};
		String[] terms = new String[] {
				"Nc","Na","Na","Nc","Nb","Ne","P","VE","Na","Nc","Nb","Neu"
		};
		for(String rule : rules) {
			System.out.println("---------------"+rule+"---------------");
			String[] rulePOSs = rule.split("\\+");
			List<Integer> relatedPOSs = new ArrayList<Integer>();
			for(int i=0; i<terms.length; i++) {
				String term = terms[i];
				for(String rulePOS : rulePOSs) {
					if(rulePOS.indexOf("(") != -1) {
						rulePOS = rulePOS.split("\\(")[1];
					}
					if(rulePOS.indexOf(")") != -1) {
						rulePOS = rulePOS.split("\\)")[0];
					}
					if(term.equals(rulePOS)) {
						relatedPOSs.add(i);
						System.out.print(i+", ");
						break;
					}
				}
			}
			System.out.println();
			System.out.println(Arrays.toString(terms));
			for(int i=0; i<relatedPOSs.size(); i++) {
				System.out.print(terms[relatedPOSs.get(i)]+" ");
			}
			System.out.println();
			System.out.println("找符合詞性");
			List<List<Integer>> result1 = new ArrayList<List<Integer>>();
			Combination comb = new Combination(rulePOSs.length, relatedPOSs.size());
			int[] c = comb.next();
			while(c != null) {
				//System.out.println(Arrays.toString(c));
				boolean match = true;
				for(int i=0; i<rulePOSs.length; i++) {
					String rpos = rulePOSs[i];
					if(rpos.indexOf("(") != -1) {
						rpos = rpos.split("\\(")[1];
					}
					if(rpos.indexOf(")") != -1) {
						rpos = rpos.split("\\)")[0];
					}
					if(!rpos.equals(terms[relatedPOSs.get(c[i])])) {
						match = false;
					}
				}
				if(match) {
					List<Integer> temp = new ArrayList<Integer>();
					for(int i=0; i<c.length; i++) {
						temp.add(relatedPOSs.get(c[i]));
						System.out.print(relatedPOSs.get(c[i])+", ");
					}
					result1.add(temp);
					System.out.println();
				}
				c = comb.next();
			}
			
			System.out.println("找是否連續");
			List<List<Integer>> result2 = new ArrayList<List<Integer>>();
			for(int i=0; i<result1.size(); i++) {
				List<Integer> candidate = result1.get(i);
				for(Integer inte : candidate) {
					System.out.print(inte+", ");
				}
				System.out.println();
				int preposition = candidate.get(0);
				boolean isContinue = false;
				boolean match = true;
				for(int j=0; j<rulePOSs.length; j++) {
					if(isContinue) {
						if(candidate.get(j) - preposition > 1) {
							match = false;
						}
					}
					preposition = candidate.get(j);
					
					String rpos = rulePOSs[j];
					if(rpos.indexOf("(") != -1) {
						rpos = rpos.split("\\(")[1];
						isContinue = true;
					}
					if(rpos.indexOf(")") != -1) {
						rpos = rpos.split("\\)")[0];
						isContinue = false;
					}
				}
				if(match) {
					result2.add(candidate);
				}
			}
			
			for(int i=0; i<result2.size(); i++) {
				List<Integer> candidate = result2.get(i);
				for(int j=0; j<candidate.size(); j++) {
					System.out.print(terms[candidate.get(j)]+"("+candidate.get(j)+") ");
				}
				System.out.println();
			}

		}
	}
	
	
	public List<List<Term>> findAllSatisfyTerm(String rule, Sentence sentence) {
		List<List<Term>> result = new ArrayList<List<Term>>();
		List<Term> terms = sentence.getTerms();
//		System.out.println("---------------"+rule+"---------------");
		String[] rulePOSs = rule.split("\\+");
		List<Integer> relatedPOSs = new ArrayList<Integer>();
		for(int i=0; i<terms.size(); i++) {
			String tpos = terms.get(i).getPartOfSpeech();
			for(String rulePOS : rulePOSs) {
				if(rulePOS.indexOf("(") != -1) {
					rulePOS = rulePOS.split("\\(")[1];
				}
				if(rulePOS.indexOf(")") != -1) {
					rulePOS = rulePOS.split("\\)")[0];
				}
				if(tpos.equals(rulePOS)) {
					relatedPOSs.add(i);
//					System.out.print(i+", ");
					break;
				}
			}
		}
//		System.out.println();
//		System.out.println(Arrays.toString(terms.toArray()));
//		for(int i=0; i<relatedPOSs.size(); i++) {
//			System.out.print(terms.get(relatedPOSs.get(i))+" ");
//		}
//		System.out.println();
//		System.out.println("找符合詞性");
		List<List<Integer>> result1 = new ArrayList<List<Integer>>();
		Combination comb = new Combination(rulePOSs.length, relatedPOSs.size());
		int[] c = comb.next();
		while(c != null) {
			//System.out.println(Arrays.toString(c));
			boolean match = true;
			for(int i=0; i<rulePOSs.length; i++) {
				String rpos = rulePOSs[i];
				if(rpos.indexOf("(") != -1) {
					rpos = rpos.split("\\(")[1];
				}
				if(rpos.indexOf(")") != -1) {
					rpos = rpos.split("\\)")[0];
				}
				if(!rpos.equals(terms.get(relatedPOSs.get(c[i])).getPartOfSpeech())) {
					match = false;
				}
			}
			if(match) {
				List<Integer> temp = new ArrayList<Integer>();
				for(int i=0; i<c.length; i++) {
					temp.add(relatedPOSs.get(c[i]));
//					System.out.print(relatedPOSs.get(c[i])+", ");
				}
				result1.add(temp);
//				System.out.println();
			}
			c = comb.next();
		}
		
//		System.out.println("找是否連續");
		List<List<Integer>> result2 = new ArrayList<List<Integer>>();
		for(int i=0; i<result1.size(); i++) {
			List<Integer> candidate = result1.get(i);
//			for(Integer inte : candidate) {
//				System.out.print(inte+", ");
//			}
//			System.out.println();
			int preposition = candidate.get(0);
			boolean isContinue = false;
			boolean match = true;
			for(int j=0; j<rulePOSs.length; j++) {
				if(isContinue) {
					if(candidate.get(j) - preposition > 1) {
						match = false;
					}
				}
				preposition = candidate.get(j);
				
				String rpos = rulePOSs[j];
				if(rpos.indexOf("(") != -1) {
					rpos = rpos.split("\\(")[1];
					isContinue = true;
				}
				if(rpos.indexOf(")") != -1) {
					rpos = rpos.split("\\)")[0];
					isContinue = false;
				}
			}
			if(match) {
				result2.add(candidate);
			}
		}
		
		for(int i=0; i<result2.size(); i++) {
			List<Integer> candidate = result2.get(i);
			List<Term> candidateTemp = new ArrayList<Term>();
			for(int j=0; j<candidate.size(); j++) {
//				System.out.print(terms.get(candidate.get(j))+"("+candidate.get(j)+") ");
				Term term = terms.get(candidate.get(j));
				term.setPosition(candidate.get(j));
				candidateTemp.add(term);
			}
			result.add(candidateTemp);
//			System.out.println();
		}
		return result;
	}
}
