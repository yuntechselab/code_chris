package WeaveNerTools;

import java.net.URLEncoder;

import yahooNewsTagTools.Wiki;
import entity.WikiObject;

public class CompanyDetection {
    // 是否為公司
    public static boolean isCompanyByWikipedia(String companyName) {
	boolean isCompany = false;
	try {
	    Wiki wiki = new Wiki();
	    WikiObject wo;
	    companyName = companyName.replace(" ", "_");
	    companyName = URLEncoder.encode(companyName,"UTF-8");
//	    System.out.println("Determine:" + companyName);
	    wo = wiki.createWikiObject(companyName, "http://zh.wikipedia.org/w/api.php?action=parse&format=xml&page=" + companyName + "&redirects");
	    if (wo != null) {
		String infoBox = wo.getInfoBoxSource();
//		System.out.println("InfoBox:" + infoBox);
		if (infoBox.contains("公司") || infoBox.contains("股票") || infoBox.contains("組織法規")) {
		    isCompany = true;
		}
		for (String c : wo.getOpenCategory()) {
//			System.out.println("OpenCategory:" + c);
		    if (c.contains("公司") || c.contains("政府機構") || c.contains("政黨") || c.contains("建立") || c.contains("隊") || c.contains("教育機構") ||c.contains("局")|| c.contains("組織")) { //Chris add "教育機構"、"局"、"組織"
			isCompany = true;
			break;
		    }
		}
		if (wo.getOpenCategory().toString().contains("出生")) {
		    isCompany = false;
		}
	    }

	} catch (Exception e) {

	} finally {
	    return isCompany;
	}
    }

    // 是否為公司
    public static boolean isCompanyByWikipediaEng(String companyName) {
	boolean isCompany = false;
	try {
	    Wiki wiki = new Wiki();
	    WikiObject wo;
	    companyName = companyName.replace(" ", "_");
//	    companyName = URLEncoder.encode(companyName,"UTF-8");
	    wo = wiki.createWikiObject(companyName, "http://en.wikipedia.org/w/api.php?action=parse&format=xml&page=" + companyName + "&redirects");
	    if (wo != null) {
		String infoBox = wo.getInfoBoxSource();
		if (infoBox.contains("Inc.") || infoBox.contains("Founded") || infoBox.contains("Employees")) {
		    isCompany = true;
		}
		for (String c : wo.getOpenCategory()) {
		    if (c.contains("companies") || c.contains("established") || c.contains("parties")) {
			isCompany = true;
			break;
		    }
		}
		//空格用底線取代
		if (wo.getOpenCategory().toString().contains("births") || wo.getOpenCategory().toString().contains("States_and_territories_established_in")) {
		    isCompany = false;
		}
	    }

	} catch (Exception e) {

	} finally {
	    return isCompany;
	}
    }
}
