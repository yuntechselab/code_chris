import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import tools.CKIPTool;
import tools.files.LoadProps;
import databaseTools.OurSqlConnection;

/**
 * input:一欄內有多個長詞，列和列長詞之間並未去重複 ex. 簡單長詞:[(台股青紅燈,1.0,Na+Na,null,簡單長詞,true,false,,), (中國觀光客,1.0,Nc+Na,null,簡單長詞,false,false,,), (經濟現象,1.0,Na+Na,null,簡單長詞,false,false,,), (旅館股票,1.0,Nc+Na,null,簡單長詞,false,false,,), (產能過剩股,1.0,Na+Na,null,簡單長詞,false,false,,), (石化原料,1.0,Na+Na,null,簡單長詞,false,false,,), (南韓海力士,1.0,Nc+Na+Na,null,簡單長詞,false,false,,), (海力士,1.0,Na+Na,null,簡單長詞,false,false,,), (終端產品售價,1.0,Nc+Na+Na,null,簡單長詞,false,false,,), (產品售價,1.0,Na+Na,null,簡單長詞,false,false,,), (動物本能,1.0,Na+Na,null,簡單長詞,false,false,,)]
 * output:將所有長詞一個放一列，把在資料庫內的簡單長詞(未分割，未去重複)，來進行處裡，並給予ID ex.ID:50 SimpleTerm:補貼電費 */
public class SimpleLongTermtoDB {
	public static void main(String[] args) throws SQLException {
		// ------------------------------------------------------------
		OurSqlConnection conn = new OurSqlConnection(LoadProps.getProperties("databaseName"));
		String sql = "Select * from  yahoo_news_world3000;";
		conn.setSql(sql);
		ResultSet result = conn.getResultSet();
		int i = 0;
		Map<String, Integer> map = new HashMap<String, Integer>();
		while (result.next()) {
			String ckip = result.getString("CkipSimpleLongTerm");
			Map<String, Integer> temp = CKIPTool.getTerms(ckip);
			// 將所有簡單長詞放入HashMap內，達到去重複字詞
			for (String s : temp.keySet()) {
				map.put(s, i);
				i++;
			}
			System.out.println("此篇共有" + temp.size() + "個查詢詞");
			System.out.println("累計共有" + map.size() + "個查詢詞");
		}

		ArrayList<String> wordList = new ArrayList<String>();
		for (String s : map.keySet()) {
			wordList.add(s);
		}
		System.out.println("ArrayList內有:" + wordList.size() + "個詞");

		ArrayList<String> wordSQLList = new ArrayList<String>();
		for (int s = 1; s < wordList.size(); s++) {
			System.out.println("DO " + wordList.get(s));

			wordSQLList
					.add("Insert into simplelongterm(SimpleTerm,ID) values('"
							+ wordList.get(s) + "','" + s + "')");
		}
		IntoDB.writeIntoDB(wordSQLList);
        System.out.println("finish");
	}
}
