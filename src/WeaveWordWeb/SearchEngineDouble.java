package WeaveWordWeb;

import java.io.UnsupportedEncodingException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import yahooNewsTagTools.ChangeCode;
import databaseTools.OurSqlConnectionSinglton;
import entity.WikiTermBean;

public class SearchEngineDouble {
	public static void main(String[] args) {
		String queryWord = "女性政治人物";
		try {
			queryWord = ChangeCode.toLong(queryWord);
			// queryWord = new String(queryWord.getBytes("ISO-8859-1"),
			// "UTF-8");
			SearchEngineDouble search = new SearchEngineDouble(queryWord);
			Map<String, WikiTermBean> map = search.getSearchResult();

			Map<String, ArrayList<String>> doubleResult = new HashMap<>();

			System.out.println("result" + map);
			for (String secondQueryWord : map.keySet()) {
				System.out.println("secondQueryWord: " + secondQueryWord);
				SearchEngineDouble searchSecondLayer = new SearchEngineDouble(
						secondQueryWord);
				Map<String, WikiTermBean> secondLayerMap = searchSecondLayer
						.getSearchResult();
				ArrayList<String> arrayList = new ArrayList<String>();
				if (secondLayerMap != null) {
					for (String secondLayerString : secondLayerMap.keySet()) {
						System.out.println("secondLayerString: "
								+ secondLayerString);
						arrayList.add(secondLayerString);
					}
					doubleResult.put(secondQueryWord, arrayList);
				}
			}
			System.out.println(doubleResult);
			search.queryTwoLayer();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public Map<String, ArrayList<String>> queryTwoLayer() {
		SearchEngineDouble search = new SearchEngineDouble(queryWord);
		Map<String, WikiTermBean> map = search.getSearchResult();

		Map<String, ArrayList<String>> doubleResult = new HashMap<>();

		System.out.println("result " + map);
		for (String secondQueryWord : map.keySet()) { //null
			System.out.println("secondQueryWord: " + secondQueryWord);
			SearchEngineDouble searchSecondLayer = new SearchEngineDouble(
					secondQueryWord);
			Map<String, WikiTermBean> secondLayerMap = searchSecondLayer
					.getSearchResult();
			ArrayList<String> arrayList = new ArrayList<String>();
			if (secondLayerMap != null) {
				for (String secondLayerString : secondLayerMap.keySet()) {
					System.out.println("secondLayerString: "
							+ secondLayerString);
					arrayList.add(secondLayerString);
				}
				doubleResult.put(secondQueryWord, arrayList);
			}
		}
		
		
		System.out.println(doubleResult);
		return doubleResult;
	}

	private String queryWord;

	public SearchEngineDouble(String queryWord) {
		this.queryWord = queryWord;
	}

	public Map<String, WikiTermBean> getSearchResult() {

		SingleTermForWikiTerm single = new SingleTermForWikiTerm(this.queryWord);
		Map<String, WikiTermBean> result1 = null;
		try {
			result1 = single.getSynoym();
			// System.out.println("result1: "+result1);
			if (result1 == null) {
				return null;
			} else {
				result1 = SingleTermForWikiTerm.removeDuplicate(result1);
			}

		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		return result1;
	}
}
