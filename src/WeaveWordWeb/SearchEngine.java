package WeaveWordWeb;

import java.io.UnsupportedEncodingException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import yahooNewsTagTools.ChangeCode;
import databaseTools.OurSqlConnectionSinglton;
import entity.WikiTermBean;

public class SearchEngine {
	public static void main(String[] args) {
		String queryWord = "皮卡丘";
		try {
			queryWord = ChangeCode.toLong(queryWord);
			// queryWord = new String(queryWord.getBytes("ISO-8859-1"),
			// "UTF-8");
			SearchEngine search = new SearchEngine(queryWord);
			Map<String, WikiTermBean> map = search.getSearchResult();
			System.out.println("result" + map);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static String searchTableName = "answerword";
	private String queryWord;

	public SearchEngine(String queryWord) {
		this.queryWord = queryWord;
	}

	public Map<String, WikiTermBean> getSearchResult() {

		SingleTermForWikiTerm single = new SingleTermForWikiTerm(this.queryWord);
		Map<String, WikiTermBean> result1 = null;
		try {
			result1 = single.getSynoym();
			// System.out.println("result1: "+result1);
			if (result1 == null) {
				return null;
			} else {
				result1 = SingleTermForWikiTerm.removeDuplicate(result1);
			}

		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		// Map<String, WikiTermBean> result2 = null;
		// ;
		// try {
		// result2 = single.getRelationshipWord();
		// System.out.println("result2: "+result2);
		//
		// result2 = null;
		// } catch (SQLException e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }
		// if (result2 == null) {
		// result2 = new HashMap<String, WikiTermBean>();
		// }
		// result2.putAll(result1);
		// OurSqlConnectionSinglton.close();

		// return result2;
		return result1;

	}

}
