package WeaveWordWeb;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONArray;

import entity.WikiTermBean;
import tools.Hashtools;
import yahooNewsTagTools.Encoder;

public class PresentationLogic {
	// get top WikiTermBean not yet
	public static Map<String, WikiTermBean> getTop(
			Map<String, WikiTermBean> terms, int top) {
		Map<String, WikiTermBean> sortedTerms = Hashtools.sortByValue(terms);
		// if(sortedTerms.keySet().size()>top){
		// System.out.println("TOP:" + top);
		// int index = 0;
		// Map<String , WikiTermBean> topTerms = new HashMap<String ,
		// WikiTermBean>();
		// //test
		// // for(String s : sortedTerms.keySet()){
		// // System.out.println("Sorted:" + s);
		// // }
		// //
		// for(String ss : sortedTerms.keySet()){
		// if(index < top){
		// if(sortedTerms.get(ss).getReasonability().equals("0%") ){
		// if(!(sortedTerms.get(ss).getMappingRule().equals("openCategory") ||
		// sortedTerms.get(ss).getMappingRule().equals("Reference"))){
		// topTerms.put(ss, sortedTerms.get(ss));
		// index++;
		// }else{
		// continue;
		// }
		// }else{
		// topTerms.put(ss, sortedTerms.get(ss));
		// index++;
		// }
		// }else{
		// break;
		// }
		// }
		// // System.out.println("Index:" + index);
		// // for(String ss : topTerms.keySet()){
		// // System.out.println(ss);
		// // }
		// while(index < top){
		// for(String ss : sortedTerms.keySet()){
		// if(!topTerms.containsKey(ss)){
		// // System.out.println("ADD:" +ss);
		// topTerms.put(ss, sortedTerms.get(ss));
		// index++;
		// break;
		// }
		// }
		// }
		// return topTerms;
		// }else{
		// return sortedTerms;
		// }
		return sortedTerms;
	}

	public static JSONObject getPresentationJson(Map<String, WikiTermBean> terms) {
		// ontology presentation logic
		Map<String, Map<String, String[]>> finalResult = new HashMap<String, Map<String, String[]>>(); // Presentation
																										// JSON
																										// format
		Map<String, String[]> syn = new HashMap<String, String[]>();
		ArrayList<String> ary = new ArrayList<String>();
		for (String s : terms.keySet()) {
			ary.add(s);
		}

		int size = ary.size();
		String[] ary2 = new String[size];

		for (int i = 0; i < size; i++) {
			ary2[i] = ary.get(i);
		}

		syn.put("syn", ary2);
		finalResult.put("verb", syn);
		JSONObject json = new JSONObject(finalResult);
		return json;
	}

	public static JSONObject getAcceptanceLogicJson(
			Map<String, WikiTermBean> terms) {
		Map<String, WikiTermBean> sortedTerms = Hashtools.sortByValue(terms);
		JSONObject json = new JSONObject(sortedTerms);
		for (String s : sortedTerms.keySet()) {
			try {
				JSONObject tmpJ = json.getJSONObject(s);

				json.remove(s);
				json.put(s, tmpJ);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return json;
	}

	public static String getQueryWordAndType(String queryWord, String type) {
		if (type.equals("Company")) {
			queryWord = queryWord + "\n" + "(組織)";
		} else if (type.equals("Stuff")) {
			queryWord = queryWord + "\n" + "(東西)";
		} else if (type.equals("Person")) {
			queryWord = queryWord + "\n" + "(人名)";
		} else if (type.equals("Event")) {
			queryWord = queryWord + "\n" + "(事)";
		} else if (type.equals("Place")) {
			queryWord = queryWord + "\n" + "(地名)";
		}
		return queryWord;
	}

	public static Map<String, WikiTermBean> removeNoise(
			Map<String, WikiTermBean> terms) {
		ArrayList<String> tmpS = new ArrayList<String>();
		for (String s : terms.keySet()) {
			if (PresentationLogic.IsNoise(s)) {
				tmpS.add(s);
			}
		}
		for (String s : tmpS) {
			terms.remove(s);
		}
		return terms;
	}

	private static boolean IsNoise(String s) {
		if (s.equals("本地鏈接的維基共享資源分類與Wikidata不同")) {
			return true;
		} else if (s.equals("含有明確引用中文的條目")) {
			return true;
		} else if (s.equals("有明確引用中文的條目")) {
			return true;
		} else if (s.equals("地鏈接的維基共享資源分類與Wikidata不同")) {
			return true;
		} else if (s.equals("本地相關圖片與維基數據不同")) {
			return true;
		} else if (s.equals("使用無數據行信息框模板的條目")) {
			return true;
		} else if (s.equals("有未列明來源語句的條目")) {
			return true;
		} else if (s.contains("維基百科來源")) {
			return true;
		} else if (s.equals("使用Break*模板的條目")) {
			return true;
		} else if (s.equals("用Break*模板的條目")) {
			return true;
		} else if (s.equals("含有英語的條目")) {
			return true;
		} else if (s.contains("條目")) {
			return true;
		} else if (s.contains("頁面") || s.contains("消歧義")) {
			return true;
		} else if (s.contains("含有日語的條目")) {
			return true;
		} else if (s.contains("含有英語的條目")) {
			return true;
		} else if (s.contains("的條目")) {
			return true;
		} else if (s.contains("Category")) {
			return true;
		} else if (s.length() > 10 && s.indexOf("、")==-1) {
			return true;
		}

		return false;
	}

	public static JSONObject getTwoLayerPresentationJson(
			Map<String, ArrayList<String>> terms, String queryWord) throws JSONException {
		// TODO Auto-generated method stub

		// 去重複
		System.out.println("terms before: " + terms);
		Map<String, ArrayList<String>> termsReduceDuplicate = new HashMap<>();
		ArrayList<String> tempArray = new ArrayList<>();

		System.out.println("queryWord: " + queryWord);
		tempArray.add(queryWord);

		for (String string : terms.keySet()) {
			string = Encoder.SimToTra(string);

			if (!tempArray.contains(string) && !IsNoise(string)) {
				// System.out.println("string: " + string);
				tempArray.add(string);
				ArrayList<String> secondLayerArray = new ArrayList<>();

				if (terms.get(string) != null) {

					for (String string2 : terms.get(string)) {
						string2 = Encoder.SimToTra(string2);

						if (!tempArray.contains(string2) && !IsNoise(string2)) {
							// System.out.println("string2: " + string2);

//							tempArray.add(string2);
							secondLayerArray.add(string2);
						}
					}
				}
				termsReduceDuplicate.put(string, secondLayerArray);
			}
		}

		System.out.println("tempArray: " + tempArray);
		System.out.println("termsReduceDuplicate: ");

		for (String str : termsReduceDuplicate.keySet()) {
			System.out.println(str + " : " + termsReduceDuplicate.get(str));

		}
		terms.clear();
		terms = termsReduceDuplicate;

		// json轉換
		// JSON
		// format
		//16,0908 Richard, Jason
		int countObj=0;
		//Map<String, Map<String, String[]>> finalResult = new HashMap<String, Map<String, String[]>>(); // Presentation
		JSONObject jsonObjectlevel1 = new JSONObject();
		for (String string : terms.keySet()) {
			JSONObject jsonObjectlevel2 = new JSONObject();
			JSONArray jsonArray = new JSONArray();
			//計算object數量
			countObj++;
			ArrayList<String> array = terms.get(string);
			//Map<String, String[]> syn = new HashMap<String, String[]>();-
			int size = array.size();
			String[] ary2 = new String[size];
			jsonObjectlevel2.put("0", string);
			//將陣列資料放入json陣列中
			for (int i = 0; i < size; i++) {
				jsonArray.put(array.get(i));
			}
			jsonObjectlevel2.put("syn", jsonArray);
			jsonObjectlevel1.put(String.valueOf(countObj), jsonObjectlevel2);
		}
		return jsonObjectlevel1;
	}
}
