package entity;

import java.util.ArrayList;
import java.util.List;

public class Sentence {
	private String sentence;
	private String sentencePOS;
	private List<Term> terms;
	public Sentence() {
		this(null, null, null);
	}
	public Sentence(String sentence, String sentencePOS, List<Term> terms) {
		this.sentence = sentence;
		this.sentencePOS = sentencePOS;
		this.terms = terms;
	}
	public String getSentence() {
		if(sentence == null) {
			StringBuilder sb = new StringBuilder();
			for(Term term : terms) {
				sb.append(term.getTerm());
			}
			sentence = sb.toString();
		}
		return sentence;
	}
	public String getSentencePOS() {
		return sentencePOS;
	}
	public List<Term> getTerms() {
		return terms;
	}
	public List<List<Term>> compareWithRule(String rule) {
		if(rule == null || "".equals(rule)) { //沒有輸入rule
			return null;
		}
		String[] rulePOSs = rule.split("\\+"); //分割規則
		if(rulePOSs.length > terms.size()) { //rule裡的詞性比句子裡的詞性還多
			return null;
		}
		boolean continuous = false; //判斷是否連續規則
		boolean[] isMeets = new boolean[rulePOSs.length]; //記錄句子裡的字詞是否符合rule裡的詞性
		int lastIndex = 0;
		int continuousIndex = 0;
		List<List<Term>> result = new ArrayList<List<Term>>();
		for(int times=0; times<terms.size()-rulePOSs.length + 1; times++) { //次數
			for(int i=times; i<terms.size(); i++) { //第一個字
				List<Term> satisfyTerms = new ArrayList<Term>();
				continuous = false;
				lastIndex = i;
				continuousIndex = 0;
				for(int j=0, index=i; j<rulePOSs.length && index<terms.size();) {
					String termPOS = terms.get(index).getPartOfSpeech(); //取得句子裡字詞的詞性
					String rulePOS = rulePOSs[j]; //取得rule裡的詞性
					isMeets[j] = false;
					if(rulePOSs[j].indexOf("(") != -1) { //將左括號去掉
						rulePOS = rulePOS.split("\\(")[1];
					}
					if(rulePOSs[j].indexOf(")") != -1) { //將右括號去掉
						rulePOS = rulePOS.split("\\(")[0];
					}
					if(termPOS.equals(rulePOS)) { //比較規則裡的詞性與句子裡的字詞詞性
						isMeets[j] = true;
						satisfyTerms.add(terms.get(index));
						lastIndex = index;
						if(continuous) {
							if(index - lastIndex > 1) {
								isMeets[j] = false;
							}
						}
						j++;
					}
					if(rulePOSs[j].indexOf("(") != -1) { //設定連續規則
						continuous = true;
					}
					if(rulePOSs[j].indexOf(")") != -1) { //將右括號去掉
						continuous = false;
					}
				}
			}
		}
		return result;
	}
}
