package entity;

public class HtmlEntity {
	private String url;
	private String html;
	private long time;
	private String queryWord;
	public HtmlEntity(String url, String html, long time) {
		super();
		this.url = url;
		this.html = html;
		this.time = time;
	}
	public String getQueryWord() {
		return queryWord;
	}
	public void setQueryWord(String queryWord) {
		this.queryWord = queryWord;
	}
	public HtmlEntity(String url, String html) {
		super();
		this.url = url;
		this.html = html;
	}
	public HtmlEntity() {
		super();
	}
	public HtmlEntity(String url, String html, long time,
			String queryWord) {
		// TODO Auto-generated constructor stub
		super();
		this.url = url;
		this.html = html;
		this.time = time;
		this.queryWord = queryWord;		
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getHtml() {
		return html;
	}
	public void setHtml(String html) {
		this.html = html;
	}
	public long getTime() {
		return time;
	}
	public void setTime(long time) {
		this.time = time;
	}
}
