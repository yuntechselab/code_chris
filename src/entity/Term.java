package entity;

public class Term implements Cloneable {
	private String term;
	private String partOfSpeech;
	private int count;
	private int position;
	
	public Term() {
		this(null, null, 0, 0);
	}
	public Term(String term, String partOfSpeech, int position) {
		this(term, partOfSpeech, 0, position);
	}
	public Term(String term, String partOfSpeech, int count, int position) {
		this.term = term;
		this.partOfSpeech = partOfSpeech;
		this.count = count;
	}
	public void setTerm(String term) {
		this.term = term;
	}
	public String getTerm() {
		return term;
	}
	public void setPartOfSpeech(String pos) {
		this.partOfSpeech = pos;
	}
	public String getPartOfSpeech() {
		return partOfSpeech;
	}
	public void setCount(int count) {
		this.count = count;
	}
	public int getCount() {
		return count;
	}
	public void setPosition(int position) {
		this.position = position;
	}
	public int getPosition() {
		return position;
	}
	public String toString() {
		return new StringBuilder().append(term).append(":").append(partOfSpeech).append("(").append(position).append(")").toString();
	}
	public boolean equals(Object o) {
		return o != null && o instanceof Term && position == (((Term) o).getPosition()) && term.equals(((Term) o).getTerm()) && partOfSpeech.equals(((Term) o).getPartOfSpeech());
	}
	public int hashCode() {
		int result = 17;
		result = 37 * result + position;
		result = 37 * result + term.hashCode();
		result = 37 * result + partOfSpeech.hashCode();
		return result;
	}
	public Term clone() {
		Term clone = new Term();
		clone.setTerm(this.term);
		clone.setPartOfSpeech(this.partOfSpeech);
		clone.setPosition(this.position);
		clone.setCount(this.count);
		return clone;
	}
}
