package entity;

public class TagWord {
	private String tagWord;
	private String tagType;
	
	public TagWord(String tagWord, String tagType) {
		super();
		this.tagWord = tagWord;
		this.tagType = tagType;
	}
	public String getTagWord() {
		return tagWord;
	}
	public void setTagWord(String tagWord) {
		this.tagWord = tagWord;
	}
	public String getTagType() {
		return tagType;
	}
	public void setTagType(String tagType) {
		this.tagType = tagType;
	}
}
