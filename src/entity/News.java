package entity;

import java.io.FileInputStream;

public class News {
	private String _id;
	private String _title;
	private String _content;
	private String _htmlContent;
	private String _tagContent;
	private String _url;
	private String _publisher;
	private String _date;
	private String _time;
	private FileInputStream _image;
	private String _category;
	private String _county;
	private String _htmlRelatedNewsContent;

	public String get_htmlRelatedNewsContent() {
		return _htmlRelatedNewsContent;
	}

	public void set_htmlRelatedNewsContent(String _htmlRelatedNewsContent) {
		this._htmlRelatedNewsContent = _htmlRelatedNewsContent;
	}

	public String get_htmlContent() {
		return _htmlContent;
	}

	public void set_htmlContent(String _htmlContent) {
		this._htmlContent = _htmlContent;
	}

	public String get_tagContent() {
		return _tagContent;
	}

	public void set_tagContent(String _tagContent) {
		this._tagContent = _tagContent;
	}

	public String get_id() {
		return _id;
	}

	public void set_id(String _id) {
		this._id = _id;
	}

	public String get_title() {
		return _title;
	}

	public void set_title(String _title) {
		this._title = _title;
	}

	public String get_content() {
		return _content;
	}

	public void set_content(String _content) {
		this._content = _content;
	}

	public String get_url() {
		return _url;
	}

	public void set_url(String _url) {
		this._url = _url;
	}

	public String get_publisher() {
		return _publisher;
	}

	public void set_publisher(String _publisher) {
		this._publisher = _publisher;
	}

	public String get_date() {
		return _date;
	}

	public void set_date(String _date) {
		this._date = _date;
	}

	public String get_time() {
		return _time;
	}

	public void set_time(String _time) {
		this._time = _time;
	}

	public FileInputStream get_image() {
		return _image;
	}

	public void set_image(FileInputStream _image) {
		this._image = _image;
	}

	public String get_category() {
		return _category;
	}

	public void set_category(String _category) {
		this._category = _category;
	}

	public String get_county() {
		return _county;
	}

	public void set_county(String _county) {
		this._county = _county;
	}
}
