package entity;

public class PatternObject {
	private int leftRange;
	private int rightRange;
	private String patternName;
	public PatternObject(String patternName){
		this.patternName = patternName;
	}
	public void setPatternName(String patternName) {
		this.patternName = patternName;
	}
	public int getLeftRange() {
		return leftRange;
	}
	public void setLeftRange(int leftRange) {
		this.leftRange = leftRange;
	}
	public int getRightRange() {
		return rightRange;
	}
	public void setRightRange(int rightRange) {
		this.rightRange = rightRange;
	}
	public String getPatternName() {
		return patternName;
	}
	
	
}
