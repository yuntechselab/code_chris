package entity;

import java.util.ArrayList;
import java.util.HashSet;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import java.util.HashMap;

public class WikiObject {
	private String searchKey;
	private String sourceCode;
	private String firstHeading;
	private String firstContent;
	private Element firstContentHTML;
	private ArrayList<String> openCategory;
	private ArrayList<String> paragraphs;
	private Elements paragraphsHTML;
	private HashMap<String, String> synonyms;
	private boolean isPolysemy;
	private String infoBoxSource;
	private HashSet<String> polysemySet;
	private boolean isExist;
	private ArrayList<String> references;

	public WikiObject(String searchKey) {
		setSearchKey(searchKey);
		openCategory = new ArrayList<String>();
		paragraphs = new ArrayList<String>();
		paragraphsHTML = new Elements();
		synonyms = new HashMap<String, String>();
		isPolysemy = false;
		polysemySet = new HashSet<String>();
		infoBoxSource = "";
		references = new ArrayList<String>();
		isExist = true;
	}

	public String getFirstHeading() {
		return firstHeading;
	}

	public void setFirstHeading(String firstHeading) {
		this.firstHeading = firstHeading;
	}

	public ArrayList<String> getOpenCategory() {
		return openCategory;
	}

	public void setOpenCategory(ArrayList<String> openCategory) {
		this.openCategory = openCategory;
	}

	public String getFirstContent() {
		return firstContent;
	}

	public void setFirstContent(String firstContent) {
		this.firstContent = firstContent;
	}

	public String getSourceCode() {
		return sourceCode;
	}

	public void setSourceCode(String sourceCode) {
		this.sourceCode = sourceCode;
	}

	public String getSearchKey() {
		return searchKey;
	}

	public void setSearchKey(String searchKey) {
		this.searchKey = searchKey;
	}

	public ArrayList<String> getParagraphs() {
		return paragraphs;
	}

	public HashMap<String, String> getSynonyms() {
		return synonyms;
	}

	public void setSynonyms(HashMap<String, String> synonyms) {
		this.synonyms = synonyms;
	}

	public void setParagraphs(ArrayList<String> paragraphs) {
		this.paragraphs = paragraphs;
	}

	public boolean isPolysemy() {
		return isPolysemy;
	}

	public void setPolysemy(boolean isPolysemy) {
		this.isPolysemy = isPolysemy;
	}

	public Element getFirstContentHTML() {
		return firstContentHTML;
	}

	public void setFirstContentHTML(Element firstContentHTML) {
		this.firstContentHTML = firstContentHTML;
	}

	public Elements getParagraphsHTML() {
		return paragraphsHTML;
	}

	public void setParagraphsHTML(Elements paragraphsHTML) {
		this.paragraphsHTML = paragraphsHTML;
	}

	public HashSet<String> getPolysemySet() {
		return polysemySet;
	}

	public void setPolysemySet(HashSet<String> polysemySet) {
		this.polysemySet = polysemySet;
	}

	public String getInfoBoxSource() {
		return infoBoxSource;
	}

	public void setInfoBoxSource(String infoBoxSource) {
		this.infoBoxSource = infoBoxSource;
	}

	public boolean isExist() {
		return isExist;
	}

	public void setExist(boolean isExist) {
		this.isExist = isExist;
	}

	public ArrayList<String> getReferences() {
		return references;
	}

	public void setReferences(ArrayList<String> references) {
		this.references = references;
	}

}
