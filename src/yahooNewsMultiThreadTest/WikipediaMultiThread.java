package yahooNewsMultiThreadTest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CountDownLatch;




import WeaveNerTools.DetectionEngine;
import database.DbInvoker;
import entity.TagWord;
import socket.SearchEngine;
import synonymResearch.WikiTermBean_backup;

public class WikipediaMultiThread {
	List result = new ArrayList<>();

	public Map<String, String> wikiNerDetect(List<String> termList) {
		Map<String, String> tagWordTypeMap = new HashMap<>();
		CountDownLatch countDownLatch = new CountDownLatch(3);

		for (String key : termList) {
			// 启动100个子线程
			SampleTask task = new SampleTask(key, tagWordTypeMap,
					countDownLatch);
			Thread thread = new Thread(task);
			thread.start();

		}
		
		try {
			// 主线程等待所有子线程执行完成，再向下执行
			countDownLatch.await();
			return tagWordTypeMap;

		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return tagWordTypeMap;

	}

	public static void main(String[] args) {
		// 用来测试的List
		List<String> data = new ArrayList<>();
		// 用来让主线程等待100个子线程执行完毕
		CountDownLatch countDownLatch = new CountDownLatch(100);
		// 启动100个子线程
		for (int i = 0; i < 100; i++) {
			SampleTask task = new SampleTask(data, countDownLatch);
			Thread thread = new Thread(task);
			thread.start();
		}
//		try {
//			// 主线程等待所有子线程执行完成，再向下执行
//			countDownLatch.await();
//		} catch (InterruptedException e) {
//			e.printStackTrace();
//		}
		// List的size
		System.out.println(data.size());
	}
}

class SampleTask implements Runnable {
	CountDownLatch countDownLatch;
	Map<String, String> tagWordTypeMap;
	String key;
	private List<String> data;
	public SampleTask(List<String> data, CountDownLatch countDownLatch) {
		this.data = data;
		this.countDownLatch = countDownLatch;
	}

	public SampleTask(String key, Map<String, String> tagWordTypeMap,
			CountDownLatch countDownLatch) {
		// TODO Auto-generated constructor stub
		this.key = key;
		this.tagWordTypeMap = tagWordTypeMap;
		this.countDownLatch = countDownLatch;
	}

	@Override
	public void run() {
		// 每个线程向List中添加100个元素
		try {
			// System.out.println(key+key.length());
			DbInvoker dbInvoker = new DbInvoker();
			String type = dbInvoker.readTagWord(key);
			String nonTagWord = dbInvoker.readNonTagWord(key);
			System.out.println("------------------" + key
					+ "------------------");

			if (type != null && type != "" && !type.isEmpty()
					&& !type.equals("")) {
				System.out.println("--------------" + key
						+ "有在TagWord資料庫------------------");
				synchronized (tagWordTypeMap) {
					tagWordTypeMap.put(key, type);
				}
			} else if (nonTagWord != null && nonTagWord != ""
					&& !nonTagWord.isEmpty() && !nonTagWord.equals("")) {
				System.out.println("--------------" + key
						+ "有在NonTagWord資料庫------------------");
			} else {
				System.out.println("------------------" + key
						+ "沒有在Tag與NonTag資料庫------------------");
				type = new DetectionEngine().getTypeFromWikiWoDb(key);
				if (type != null && type != "" && !type.isEmpty()
						&& !type.equals("")) {

					synchronized (tagWordTypeMap) {
						tagWordTypeMap.put(key, type);
					}
					TagWord tagWord = new TagWord(key, type);
					dbInvoker.saveTagWord(tagWord);
				} else {
					dbInvoker.saveNonTagWord(key);
				}

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		// 完成一个子线程
		countDownLatch.countDown();
	}

}