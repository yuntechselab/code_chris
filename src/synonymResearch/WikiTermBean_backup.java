package synonymResearch;
/***
 * 存放queryWord的相關資訊。
 * @author ChrisWu
 *
 */
public class WikiTermBean_backup  implements Comparable  {
	private String mappingWord;
	private String mappingRule;
	private String reasonability;
	private int totalNumber ; 
	private int agreeNumber;
	
	public int getTotalNumber() {
		return totalNumber;
	}
	public void setTotalNumber(int totalNumber) {
		this.totalNumber = totalNumber;
	}
	public int getAgreeNumber() {
		return agreeNumber;
	}
	public void setAgreeNumber(int agreeNumber) {
		this.agreeNumber = agreeNumber;
	}
	public WikiTermBean_backup(String mappingWord, String mappingRule,
			String reasonability, int totalNumber, int agreeNumber) {
		super();
		this.mappingWord = mappingWord;
		this.mappingRule = mappingRule;
		this.reasonability = reasonability;
		this.totalNumber = totalNumber;
		this.agreeNumber = agreeNumber;
	}
	public String getMappingWord() {
		return mappingWord;
	}
	public void setMappingWord(String mappingWord) {
		this.mappingWord = mappingWord;
	}
	public String getMappingRule() {
		return mappingRule;
	}
	public void setMappingRule(String mappingRule) {
		this.mappingRule = mappingRule;
	}
	public String getReasonability() {
		return reasonability;
	}
	public void setReasonability(String reasonability) {
		this.reasonability = reasonability;
	}
	@Override
	public String toString() {
		return "WikiTermBean [mappingWord=" + mappingWord + ", mappingRule="
				+ mappingRule + ", reasonability=" + reasonability
				+ ", totalNumber=" + totalNumber + ", agreeNumber="
				+ agreeNumber + "]";
	}
	@Override
	public int compareTo(Object o) {
		double own = Double.parseDouble(this.reasonability.replace("%", ""));
//		System.out.println("own:" + own);
		double other = Double.parseDouble((((WikiTermBean_backup)o ).reasonability.replace("%", "")));
		double check = own - other;
		if(check >0 ){
			return 1;
		}else if(check <0){
			return -1;
		}
		return 0;
	}
}
