package synonymResearch;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import tools.Hashtools;

import com.go.ChangeCode;

import databaseTools.OurSqlConnectionSinglton;
import entity.WikiTermBean;
/***
 * 執行單一queryWord，回傳要顯示的結果
 * 回傳WikiTerm物件給Client端
 * @author ChrisWu
 *
 */
public class SingleTermForWikiTerm {
	
	
	
	private String queryWord ;
	public SingleTermForWikiTerm(String queryWord){
		this.queryWord = queryWord;
	}
	public Map<String , WikiTermBean_backup> getRelationshipWord() throws SQLException{
		Map<String, WikiTermBean_backup> finalResult = new HashMap<String , WikiTermBean_backup>();
		OurSqlConnectionSinglton dB = OurSqlConnectionSinglton.getInstance();
		dB.setSql("Select * From answerword Where mappingWord = '" + queryWord + "'");
		ResultSet result = dB.getResultSet();
		result.last();
		int rowcount = result.getRow();
		if(rowcount != 0){
			result.first();
			result.previous();
			while(result.next()){
				String relationshipWord = result.getString("queryWord");
				WikiTermBean_backup t = new WikiTermBean_backup( relationshipWord , "relationship" , "100%" ,0 , 0);
				finalResult.put(relationshipWord, t);
			}
			return finalResult;
		}else{
			return null;
		}
	}
	public Map<String , WikiTermBean_backup> getSynoym() throws SQLException{
		 OurSqlConnectionSinglton dB = OurSqlConnectionSinglton.getInstance();
		 dB.setSql("Select * From answerword Where queryWord ='" + queryWord +"'");
		 ResultSet result = dB.getResultSet();
		 result.last();//指標移到最後
		 int rowcount = result.getRow();//取得總列數 
		 Map<String , WikiTermBean_backup> finalResult = null ;
		 // 是否已存在DB
		 if(rowcount == 0 ){
			 System.out.println("線上找資料...");
			 ArrayList<String> sqlList = Match.searchSynonymSQL(queryWord ,54321);
			 //是否有找到東西
			 if(sqlList.size() != 0){
				 System.out.println("do sqlList");
				 Store.writeIntoDB(sqlList); //線上找到資料存入DB
				 System.out.println("do open");
				 this.storeCategory(sqlList);//將catagory拆開寫入DB
//				 SingleTermForWikiTerm.storeAlreadyWord(queryWord);//openCategory有拆開過的詞要寫入alreadyTable內
				 dB.setSql("Select * From answerword Where queryWord ='" + queryWord +"'");
				 result = dB.getResultSet();
				 finalResult = this.getResult(result);
			 }else{
				 System.out.println("網路抓無資料!");
				 return null;
			 }
			 
			 return finalResult;
		 }else{
//			 System.out.println("exsit in DB");
			 result.first();//指標回到第一個
			 result.previous();//指標再往前一格
			 finalResult = this.getResult(result);
			 return finalResult;
		 }
	}
	
	private Map<String , WikiTermBean_backup > getResult(ResultSet result){
//		System.out.println("execute get result");
		Map<String , WikiTermBean_backup> totalTerm = new HashMap<String , WikiTermBean_backup >();
		boolean flag = true;
		try {
			while(result.next()){
//				if(flag){
//					String tmp = result.getString("openCategory");
//					if(!(tmp.equals("[]"))){
//						flag = false;
//						ArrayList<String> OCTerm = this.getOpenCategory(tmp);
//						for(String s : OCTerm){
//							totalTerm.put(s, "Rule");
//						}
//					}
//				}
				String mappingWord = result.getString("mappingWord");
				String mappingRule = result.getString("mappingRule");
				String reasonability = result.getString("reasonability");
				int totalNumber = result.getInt("totalNumber");
				int agreeNumber = result.getInt("agreeNumber");
				WikiTermBean_backup t = new WikiTermBean_backup( mappingWord , mappingRule , reasonability , totalNumber , agreeNumber );
				totalTerm.put(mappingWord, t);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("getResult error !!");
		}
		return totalTerm;
	}
	private boolean storeCategory(ArrayList<String> sqlList){
		boolean SmallFlag = true;
		ArrayList<String> sqlListResult = new ArrayList<String>();
		for(String s : sqlList){
			for(String ss : s.split("'0%','")){
				if(SmallFlag){
					SmallFlag = false;
					continue;
				}
//				System.out.println(ss);
				ss = ss.replaceAll(" ", "");//去空格
				ss = ChangeCode.toLong(ss);//簡轉繁
				ss = ss.substring(1, ss.indexOf("]"));
				
				//判斷是否只有一個詞
				if(!(ss.equals(""))){
					if(ss.indexOf(",") != 0){
						for(String s1 : ss.split(",")){
							String sql = "Insert into answerword(queryWord,mappingWord,mappingRule,reasonability,openCategory,mappingTime,SimpleID,totalNumber,agreeNumber) values('"+ queryWord + "','" + s1 + "','openCategory','0%','[]','0','54321','0','0')";
							sqlListResult.add(sql);
//							System.out.println(sql);
						}
					}else{
						String sql = "Insert into answerword(queryWord,mappingWord,mappingRule,reasonability,openCategory,mappingTime,SimpleID,totalNumber,agreeNumber) values('"+ queryWord + "','" + ss + "','openCategory','0%','[]','0','54321','0','0')";
						sqlListResult.add(sql);
					}
					Store.writeIntoDB(sqlListResult);
					return true;
				}else{
					SmallFlag = true;
				}
			}	
		}
		return false;
		
		
		
	}
	private ArrayList<String> getOpenCategory(String openCategory){
		ArrayList<String> result = new ArrayList<String>();
		openCategory = openCategory.replaceAll(" ", "");//去空格
		openCategory = ChangeCode.toLong(openCategory);//簡轉繁
		openCategory = openCategory.replaceAll("\\[|\\]", "");//去前後中括號
		//判斷是否只有一個詞
		if(openCategory.indexOf(",") != -1){
			for(String ss : openCategory.split(","))
				result.add(ss);
		}else{
			result.add(openCategory);
		}
		return result;
	}
	public static Map<String , WikiTermBean_backup > removeDuplicate(Map<String ,WikiTermBean_backup> map){
		map = Hashtools.sortByKeyLengthForWikiTerm_backup(map);
		ArrayList<String> tmpK = new ArrayList<String>(map.keySet());
		ArrayList<WikiTermBean_backup> tmpV = new ArrayList<WikiTermBean_backup>(map.values());
		for(int i=0; i<tmpK.size(); i++){
			String tmpS = tmpK.get(i);
			for(int j = 0 ; j<tmpK.size(); j++){
				String tmpS2 = tmpK.get(j);
				if(tmpS.equals(tmpS2)){
					continue;
				}else{
					if(tmpS.contains(tmpS2)){
//						System.out.println("Remove:" + tmpS2);
						tmpK.remove(j);
						tmpV.remove(j);	
						j--;
					}	
				}
			}
		}
		Map<String , WikiTermBean_backup> result = new HashMap<String , WikiTermBean_backup>();
		for(int k =0; k<tmpK.size(); k++){
			result.put(tmpK.get(k), tmpV.get(k));
		}
		return result;
	}
	public static void storeAlreadyWord(String queryWord){
		OurSqlConnectionSinglton dB = OurSqlConnectionSinglton.getInstance();
		dB.setSql("Insert into alreadyTable (alreadyWord) VALUES ('" + queryWord + "')");
		dB.updateData();
	}
	 
}
