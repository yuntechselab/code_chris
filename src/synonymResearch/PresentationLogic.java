package synonymResearch;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.json.JSONException;
import org.json.JSONObject;

import tools.Hashtools;

public class PresentationLogic {
	//get top WikiTermBean  not yet
	public static Map<String , WikiTermBean_backup> getTop(Map<String , WikiTermBean_backup> terms , int top){	
		Map<String , WikiTermBean_backup> sortedTerms = Hashtools.sortByValue(terms);
		if(sortedTerms.keySet().size()>top){
			System.out.println("TOP:" + top);
			int index = 0;
			Map<String , WikiTermBean_backup> topTerms = new HashMap<String , WikiTermBean_backup>();
			//test
//			for(String s : sortedTerms.keySet()){
//				System.out.println("Sorted:" + s);
//			}
			//
			for(String ss : sortedTerms.keySet()){
				if(index < top){
					if(sortedTerms.get(ss).getReasonability().equals("0%") ){
						if(!(sortedTerms.get(ss).getMappingRule().equals("openCategory") || sortedTerms.get(ss).getMappingRule().equals("Reference"))){
							topTerms.put(ss, sortedTerms.get(ss));
							index++;
						}else{
							continue;
						}
					}else{
						topTerms.put(ss, sortedTerms.get(ss));
						index++;
					}
				}else{
					break;
				}
			}
//			System.out.println("Index:" + index);
//			for(String ss : topTerms.keySet()){
//				System.out.println(ss);
//			}
			while(index < top){
				for(String ss : sortedTerms.keySet()){
					if(!topTerms.containsKey(ss)){
//						System.out.println("ADD:" +ss);
						topTerms.put(ss, sortedTerms.get(ss));
						index++;
						break;
					}
				}
			}
			return topTerms;
		}else{
			return sortedTerms;
		}
	}
	public static JSONObject getPresentationJson(Map<String , WikiTermBean_backup> terms){
		//ontology  presentation logic 
		Map<String , Map<String , String[]>> finalResult = new HashMap<String , Map<String , String[]>>(); // Presentation JSON format
		Map<String , String[]> syn = new HashMap<String , String[]>();
		ArrayList<String> ary = new ArrayList<String>();
		for(String s : terms.keySet()){
			ary.add(s);
		}
		int size = ary.size();
		String[] ary2 = new String[size];
		for(int i =0; i<size; i++){
			ary2[i] = ary.get(i);
		}
		
		syn.put("syn",  ary2);
		finalResult.put("verb", syn);
		JSONObject json = new JSONObject(finalResult);
		return json;
	}
	public static JSONObject getAcceptanceLogicJson(Map<String , WikiTermBean_backup> terms){
		Map<String , WikiTermBean_backup> sortedTerms = Hashtools.sortByValue(terms);
		JSONObject json = new JSONObject(sortedTerms);
		for(String s : sortedTerms.keySet()){
			try {
				JSONObject tmpJ = json.getJSONObject(s);
				

				json.remove(s);
				json.put(s, tmpJ);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return json;
	}
	public static String getQueryWordAndType(String queryWord  , String type ){
		if(type.equals("Company")){
			queryWord = queryWord +"\n" + "(組織)";
		}else if(type.equals("Stuff")){
			queryWord = queryWord + "\n" +"(東西)";
		}else if(type.equals("Person")){
			queryWord = queryWord +"\n" +"(人名)";
		}else if(type.equals("Event")){
			queryWord = queryWord +"\n" +"(事)";
		}else if(type.equals("Place")){
			queryWord = queryWord +"\n" +"(地名)";
		}	
		return queryWord;
	}
	public static Map<String , WikiTermBean_backup> removeNoise(Map<String , WikiTermBean_backup> terms){
		ArrayList<String> tmpS = new ArrayList<String>();
		for(String s : terms.keySet()){
			if(PresentationLogic.IsNoise(s)){
				tmpS.add(s);
			}
		}
		for(String s: tmpS){
			terms.remove(s);
		}
		return terms;
	}
	private static boolean IsNoise(String s){
		if(s.equals("本地鏈接的維基共享資源分類與Wikidata不同")){
			return true;
		}else if(s.equals("含有明確引用中文的條目")){
			return true;
		}else if(s.equals("有明確引用中文的條目")){
			return true;
		}else if(s.equals("地鏈接的維基共享資源分類與Wikidata不同")){
			return true;
		}else if(s.equals("本地相關圖片與維基數據不同")){
			return true;
		}else if(s.equals("使用無數據行信息框模板的條目")){
			return true;
		}else if(s.equals("有未列明來源語句的條目")){
			return true;
		}else if(s.contains("維基百科來源")){
			return true;
		}else if(s.equals("使用Break*模板的條目")){
			return true;
		}else if(s.equals("用Break*模板的條目")){
			return true;
		}else if(s.equals("含有英語的條目")){
			return true;
		}else if(s.contains("條目")){
			return true;
		}else if (s.contains("頁面") || s.contains("消歧義")){
			return true;
		}else if(s.length() >20){
		
			return true;
		}
		return false;
	}
}
