package synonymResearch;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import tools.Hashtools;

import com.go.ChangeCode;

import databaseTools.OurSqlConnectionSinglton;
/***
 * 執行單一queryWord，回傳要顯示的結果
 * @author ChrisWu
 *
 */
public class SingleTerm {
	private String queryWord ;
	public SingleTerm(String queryWord){
		this.queryWord = queryWord;
	}
	public Map<String , String> getRelationshipWord() throws SQLException{
		Map<String , String> finalResult = new HashMap<String , String>();
		OurSqlConnectionSinglton dB = OurSqlConnectionSinglton.getInstance();
		dB.setSql("Select * From answerword Where mappingWord = '" + queryWord + "'");
		ResultSet result = dB.getResultSet();
		result.last();
		int rowcount = result.getRow();
		if(rowcount != 0){
			result.first();
			result.previous();
			while(result.next()){
				String relationshipWord = result.getString("queryWord");
				finalResult.put(relationshipWord, "relationship");
			}
			return finalResult;
		}else{
			return null;
		}
	}
	public Map<String , String> getSynoym() throws SQLException{
		 OurSqlConnectionSinglton dB = OurSqlConnectionSinglton.getInstance();
		 dB.setSql("Select * From answerword Where queryWord ='" + queryWord +"'");
		 ResultSet result = dB.getResultSet();
		 result.last();//指標移到最後
		 int rowcount = result.getRow();//取得總列數 
		 Map<String , String> finalResult = null ;
		 // 是否已存在DB
		 if(rowcount == 0 ){
			 System.out.println("線上找資料...");
			 ArrayList<String> sqlList = Match.searchSynonymSQL(queryWord ,54321);
			 //是否有找到東西
			 if(sqlList.size() != 0){
				 System.out.println("do sqlList");
				 Store.writeIntoDB(sqlList); //線上找到資料存入DB
				 System.out.println("do open");
				 this.storeCategory(sqlList);//將catagory拆開寫入DB
				 result = dB.getResultSet();
				 finalResult = this.getResult(result);
			 }else{
				 System.out.println("網路抓無資料!");
				 return null;
			 }
			 
			 return finalResult;
		 }else{
//			 System.out.println("exsit in DB");
			 result.first();//指標回到第一個
			 result.previous();//指標再往前一格
			 finalResult = this.getResult(result);
			 return finalResult;
		 }
	}
	
	private Map<String , String > getResult(ResultSet result){
//		System.out.println("execute get result");
		Map<String , String> totalTerm = new HashMap<String , String >();
		boolean flag = true;
		try {
			while(result.next()){
//				if(flag){
//					String tmp = result.getString("openCategory");
//					if(!(tmp.equals("[]"))){
//						flag = false;
//						ArrayList<String> OCTerm = this.getOpenCategory(tmp);
//						for(String s : OCTerm){
//							totalTerm.put(s, "Rule");
//						}
//					}
//				}
				String mappingWord = result.getString("mappingWord");
				String mappingRule = result.getString("mappingRule");
				String reasonability = result.getString("reasonability");
				totalTerm.put(mappingWord, mappingRule);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("getResult error !!");
		}
		return totalTerm;
	}
	private boolean storeCategory(ArrayList<String> sqlList){
		boolean SmallFlag = true;
		ArrayList<String> sqlListResult = new ArrayList<String>();
		for(String s : sqlList){
			for(String ss : s.split("'100%','")){
				if(SmallFlag){
					SmallFlag = false;
					continue;
				}
//				System.out.println(ss);
				ss = ss.replaceAll(" ", "");//去空格
				ss = ChangeCode.toLong(ss);//簡轉繁
				ss = ss.substring(1, ss.indexOf("]"));
				
				//判斷是否只有一個詞
				if(!(ss.equals(""))){
					if(ss.indexOf(",") != 0){
						for(String s1 : ss.split(",")){
							String sql = "Insert into answerword(queryWord,mappingWord,mappingRule,reasonability,openCategory,mappingTime,SimpleID) values('"+ queryWord + "','" + s1 + "','openCategory','100%','[]','0','54321')";
							sqlListResult.add(sql);
//							System.out.println(sql);
						}
					}else{
						String sql = "Insert into answerword(queryWord,mappingWord,mappingRule,reasonability,openCategory,mappingTime,SimpleID) values('"+ queryWord + "','" + ss + "','openCategory','100%','[]','0','54321')";
						sqlListResult.add(sql);
					}
					Store.writeIntoDB(sqlListResult);
					return true;
				}else{
					SmallFlag = true;
				}
			}	
		}
		return false;
		
		
		
	}
	private ArrayList<String> getOpenCategory(String openCategory){
		ArrayList<String> result = new ArrayList<String>();
		openCategory = openCategory.replaceAll(" ", "");//去空格
		openCategory = ChangeCode.toLong(openCategory);//簡轉繁
		openCategory = openCategory.replaceAll("\\[|\\]", "");//去前後中括號
		//判斷是否只有一個詞
		if(openCategory.indexOf(",") != -1){
			for(String ss : openCategory.split(","))
				result.add(ss);
		}else{
			result.add(openCategory);
		}
		return result;
	}
	public static Map<String , String > removeDuplicate(Map<String ,String> map){
		map = Hashtools.sortByKeyLength(map);
		ArrayList<String> tmpK = new ArrayList<String>(map.keySet());
		ArrayList<String> tmpV = new ArrayList<String>(map.values());
		for(int i=0; i<tmpK.size(); i++){
			String tmpS = tmpK.get(i);
			for(int j = 0 ; j<tmpK.size(); j++){
				String tmpS2 = tmpK.get(j);
				if(tmpS.equals(tmpS2)){
					continue;
				}else{
					if(tmpS.contains(tmpS2)){
//						System.out.println("Remove:" + tmpS2);
						tmpK.remove(j);
						tmpV.remove(j);	
						j--;
					}	
				}
			}
		}
		Map<String , String> result = new HashMap<String , String>();
		for(int k =0; k<tmpK.size(); k++){
			result.put(tmpK.get(k), tmpV.get(k));
		}
		
		return result;
	}
	 
}
