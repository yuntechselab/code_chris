package synonymResearch;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Timer;

import javax.swing.text.AbstractDocument.Content;

import tools.CKIPTool;
import tools.files.LoadProps;
import Process.ContentFilter;



import com.go.ChangeCode;
import com.go.Ngram;

import databaseTools.OurSqlConnection;

/**
 * 資料來源為不重複且有編號之字詞(不適用重複且未整理之字) ex.SimpleTerm:FB ,ID:4
 * 未整理之字詞，請至SimpleLongTermtoDB預先處理
 */
public class Main {
	public static void main(String[] args) throws SQLException {
		
		// ------------------------------------------------------------
		
		OurSqlConnection conn = new OurSqlConnection(LoadProps.getProperties("databaseName"));
       
		String sql = "Select * from simplelongterm_lifestyle where ID >= 24977 ";// where ID between '31771' and '36582'";
	
		conn.setSql(sql);
		
		ResultSet result = conn.getResultSet();
		
		int count = 0;//計算不重複的查詢字
		while (result.next()) {
			String CKIPSimple = result.getString("SimpleTerm");	
			Integer ID = result.getInt("ID");		    
		    System.out.println("DO " + CKIPSimple+"  ID:"+ID);
		    ArrayList<String> sqlList = Match.searchSynonymSQL(CKIPSimple,ID);
		    System.out.println("找到" + sqlList.size() + "筆配對資料");
			Store.writeIntoDB(sqlList);
			++count;
			System.out.println("-----------------------------");
			
		}
		
		System.out.println("共有" + count +"不重複的查詢字");
		
		System.out.println("搜尋完畢");
		conn.close();
		/*
		 * String search_txt = ""; int i=0; Map<String, Integer> map = new
		 * HashMap<String, Integer>(); while(result.next()){ String ckip =
		 * result.getString("CkipSimpleLongTerm"); Map<String , Integer > temp =
		 * CKIPTool.getTerms(ckip); //將所有簡單長詞放入HashMap內，達到去重複字詞 for(String s :
		 * temp.keySet()){ map.put(s, i); i++; }
		 * System.out.println("此篇共有"+temp.size()+"個查詢詞");
		 * System.out.println("累計共有"+i+"個查詢詞"); } //跑HashMap內的資料到wiki上去找 Thread
		 * thread = new Thread(); int q=1;
		 * 
		 * for(String s:map.keySet()){ System.out.println("DO " + s); q++;
		 */
		/*
		 * if (q % 10==0) //休息幾毫秒(10秒) try{ thread.sleep(3000);
		 * }catch(InterruptedException e){ } else
		 */

		/*
		 * search_txt = s; ArrayList<String> sqlList =
		 * Match.searchSynonymSQL(search_txt);
		 * System.out.println("找到"+sqlList.size()+"筆配對資料");
		 * Store.writeIntoDB(sqlList);
		 * 
		 * } System.out.println("全部HashMap資料"+map.entrySet());
		 * System.out.println("累計共有"+i+"個查詢詞");
		 * System.out.println("Hashmap有"+map.size()+"個詞");
		 */

		// ----------------------------------------------------------------------
		/*
		 * String search_txt = "卡通明星彩蛋"; ArrayList<String> sqlList =
		 * Match.searchSynonymSQL(search_txt);
		 * System.out.println("找到"+sqlList.size()+"筆配對資料");
		 * Store.writeIntoDB(sqlList);
		 */
	}

	/*
	public static void go() {
		Source source = new Source();
		OurSqlConnection conn = new OurSqlConnection("test");
		// 3836篇新聞
		String sql = "Select * from nuclear_power_test_news where date between '2012-8-1' and '2012-8-31'";
		conn.setSql(sql);
		ResultSet rs = conn.getResultSet();
		try {
			while (rs.next()) {
				System.out.println("正在跑" + rs.getInt("uid") + "...");
				if (rs.getString("content") != null) {
					// 取得新聞內文
					String content = rs.getString("content");
					// 取得查詢字
					HashSet<String> set = source.getQueryWordsByCkip(content);
					System.out.println("有" + set.size() + "個關鍵字");
					// 每一個查詢字逐一丟入Wikipedia中同義字
					for (String search_txt : set) {
						System.out.println("尋找'" + search_txt + "'...");
						ArrayList<String> sqlList = Match
								.searchSynonymSQL(search_txt);
						System.out.println("找到" + sqlList.size() + "筆配對資料");
						Store.writeIntoDB(sqlList);
					}
				}
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	*/
}
