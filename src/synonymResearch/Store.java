package synonymResearch;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import tools.files.LoadProps;

import com.go.Wiki;

import databaseTools.OurSqlConnection;
import databaseTools.OurSqlConnectionSinglton;
import entity.WikiObject;

public class Store {
    public static void writeIntoDB(ArrayList<String> wordSQLList) {
	OurSqlConnection conn = new OurSqlConnection(LoadProps.getProperties("databaseName"));
	for(String sql : wordSQLList){
	    System.out.println(sql);
	    conn.setSql(sql);
	    conn.InsertNew(sql);
	}
	conn.close();
	
    }
    public static void UseSingltonWriteIntoDB(ArrayList<String> wordSQLList) {
    	OurSqlConnectionSinglton dB = OurSqlConnectionSinglton.getInstance();
    	for(String sql : wordSQLList){
//    	    System.out.println(sql);
    	    dB.setSql(sql);
    	    dB.InsertNew(sql);
    	}
    }
}
