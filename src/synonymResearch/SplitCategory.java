package synonymResearch;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.go.ChangeCode;

import databaseTools.OurSqlConnectionSinglton;
/***
 * 執行openCategory分離Term出來並存入DB內。
 * 已經執行過的會寫入alreadyTable中，防止重複執行。
 * @author ChrisWu
 *
 */
public class SplitCategory {
	public static void main(String[] args) throws SQLException{
		OurSqlConnectionSinglton dB = OurSqlConnectionSinglton.getInstance();
		String tableName = "answerword_all";
		
		//Get Already Words
		dB.setSql("SELECT * FROM alreadyTable");
		ResultSet result  = dB.getResultSet();
		ArrayList<String>  already = new ArrayList<String>();
		while(result.next()){
			already.add(result.getString("alreadyWord"));
		}
		
		//Do Split Work
		dB.setSql("SELECT * FROM "+ tableName +" WHERE openCategory != '[]' ");
	    result = dB.getResultSet();
		ArrayList<String> sqlListResult = new ArrayList<String>();
		while(result.next()){
			if(already.contains(result.getString("queryWord"))){
//				System.out.println( result.getString("queryWord") +"is already , so skip !");
				continue;
			}else{
				System.out.println("Doing "+ result.getString("queryWord") +"---------");
				String queryWord = result.getString("queryWord");
				String openCategory = result.getString("openCategory");
				openCategory = openCategory.replaceAll(" ", "");//去空格
				openCategory = ChangeCode.toLong(openCategory);//簡轉繁
				openCategory = openCategory.replaceAll("\\[|\\]", "");//去前後中括號
				if(openCategory.indexOf(",") != -1){
					for(String ss : openCategory.split(",")){
						sqlListResult.add("Insert into "+ tableName +" (queryWord,mappingWord,mappingRule,reasonability,openCategory,mappingTime,SimpleID) values('"+ queryWord + "','" + ss + "','openCategory','100%','[]','0','54321')");
					}	
				}else{
					sqlListResult.add("Insert into "+ tableName +" (queryWord,mappingWord,mappingRule,reasonability,openCategory,mappingTime,SimpleID) values('"+ queryWord + "','" + openCategory + "','openCategory','100%','[]','0','54321')");
				}
				Store.UseSingltonWriteIntoDB(sqlListResult);
				dB.setSql("Insert into alreadyTable (alreadyWord) VALUES ('" + queryWord + "')");
				dB.updateData();
				sqlListResult.clear();
				already.add(queryWord);
			}
		
		}
		OurSqlConnectionSinglton.close();
		System.out.println("done!!");
	}
}
