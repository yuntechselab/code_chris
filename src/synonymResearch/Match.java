package synonymResearch;

import java.net.URLEncoder;
import java.util.*;

import com.go.Wiki;

import entity.WikiObject;

public class Match {
    public static ArrayList<String> searchSynonymSQL(String search_txt,Integer ID) {
	ArrayList<String> sqlList = new ArrayList<String>();
	
	try {
	    Wiki wiki = new Wiki();
	   
	    WikiObject wo = wiki.createWikiObject(search_txt, "http://zh.wikipedia.org/w/api.php?action=parse&format=xml&page=" +URLEncoder.encode(search_txt, "UTF-8" )+ "&redirects"); // "")
//	    for(String key :wo.getSynonyms().keySet()){
//	    	System.out.println("Key:" + key + " Value:" + wo.getSynonyms().get(key));
//	    }
//	    String url = "http://zh.wikipedia.org/w/api.php?action=parse&format=xml&page=" + search_txt + "&redirects";
//	    WikiObject wo = wiki.createWikiObject(URLEncoder.encode(url), "UTF-8");
	 System.out.println("Wo:"+wo);
	    Date d = new Date();
	    // 一形多詞
	    if (wo.isExist() == false) {
	    	
		StringBuilder sb = new StringBuilder();
		int count = 0;
		// 參考
		for (String s : wo.getReferences()) {
			if(s.contains("'")){
				s=s.replace("'","''");
			}
			sqlList.add("Insert into answerword(queryWord,mappingWord,mappingRule,reasonability,openCategory,mappingTime,SimpleID,totalNumber,agreeNumber) values('" + search_txt + "','" + s + "','Reference','0%','" + wo.getOpenCategory().toString().replace("'", "") + "','" + d.getTime() + "','" + ID + "','0','0')");
		    sb.append("參考：" + s + "\n");
		    //只抓三筆參考
		    if (++count >= 3) {
			break;
		    }
		}
		System.out.println(sb.toString());
	    } else {
		// 一型多詞
		if (wo.isPolysemy()) {
		    StringBuilder sb = new StringBuilder();
		    for (String s : wo.getPolysemySet()) {
		    	if(s.contains("'")){
					s=s.replace("'","''");
				}
		    	if(s.contains("?")){
					s=s.replace("?"," ");
				}
			sb.append("一型多詞：" + s + "\n");
			sqlList.add("Insert into answerword(queryWord,mappingWord,mappingRule,reasonability,openCategory,mappingTime,SimpleID,totalNumber,agreeNumber) values('" + search_txt + "','" + s + "','Polysemy','0%','" + wo.getOpenCategory().toString().replace("'", "") + "','" + d.getTime() + "','" + ID + "','0','0')" );
		    }
		    System.out.println(sb.toString());
		} else {
		    HashMap<String, String> synonyms = wo.getSynonyms();
		    StringBuilder sb = new StringBuilder();
		    ArrayList<String> ruleResult = new ArrayList<String>();

		    // Rule Mapping
		    for (String s : synonyms.keySet()) {
			String mappingType = synonyms.get(s);
			
			if (mappingType.equals("Rule")) {
				if(s.contains("'")){
					s=s.replace("'","''");}
				if(s.contains("?")){
					s=s.replace("?"," ");}
			    ruleResult.add(s);
			} else {
				if(s.contains("'")){
					s=s.replace("'","''");}
				if(s.contains("?")){
					s=s.replace("?"," ");}
			    sb.append("Rule Mapping：" + s + "\n");
			}
			sqlList.add("Insert into answerword(queryWord,mappingWord,mappingRule,reasonability,openCategory,mappingTime,SimpleID,totalNumber,agreeNumber) values('" + search_txt + "','" + s + "','"+mappingType+"','0%','" + wo.getOpenCategory().toString().replace("'", "") + "','" + d.getTime() + "','" + ID + "','0','0')");
		    }
		    for (String rule : ruleResult) {
			sb.append("Insert into answerword " + rule + "\n");

		    }
		    System.out.println(sb.toString());
		}
	    }

	} catch (IndexOutOfBoundsException iobe) {
	    System.out.println("目前查無此同義字配對");
	} catch (Exception e) {
	    System.out.println("出錯" + e.toString());
	    System.out.println("這裡有問題");
	}
	finally{
	    return sqlList;
	}
    }
}
