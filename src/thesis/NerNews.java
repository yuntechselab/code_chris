package thesis;

import java.util.List;

public class NerNews {
	private String sourceTitle;
	private String sourceContent;
	private List<String> titlePOS;
	private List<String> contentPOS;
	private List<Sentence> titleSentences;
	private List<Sentence> contentSentences;
	private List<Term> titleList;
	private List<Term> contentList;
	//建構子
	public NerNews() {
		this(null, null, null, null, null, null, null, null);
	}
	public NerNews(String sourceTitle, String sourceContent) {
		this(sourceTitle, sourceContent, null, null, null, null, null, null);
	}
	public NerNews(String sourceTitle, String sourceContent, List<String> titlePOS, List<String> contentPOS) {
		this(sourceTitle, sourceContent, titlePOS, contentPOS, null, null, null, null);
	}
	public NerNews(List<Term> titleList, List<Term> contentList) {
		this(null, null, null, null, titleList, contentList, null, null);
	}
	public NerNews(String sourceTitle, String sourceContent, List<String> titlePOS, List<String> contentPOS, List<Term> titleList, List<Term> contentList, List<Sentence> titleSentences, List<Sentence> contentSentences) {
		this.sourceTitle = sourceTitle;
		this.sourceContent = sourceContent;
		this.titlePOS = titlePOS;
		this.contentPOS = contentPOS;
		this.titleList = titleList;
		this.contentList = contentList;
		this.titleSentences = titleSentences;
		this.contentSentences = contentSentences;
	}
	// news content getter
	public String getSourceContent() {
		return sourceContent;
	}
	public List<String> getContentPOS() {
		return contentPOS;
	}
	public List<Sentence> getContentSentences() {
		return contentSentences;
	}
	public List<Term> getContentList() {
		return contentList;
	}
	public String getContent() {
		if(sourceContent == null) {
			return concatenateContent();
		}
		return sourceContent;
	}
	public String concatenateContent() {
		StringBuilder sb = new StringBuilder();
		for(Term term : contentList) {
			sb.append(term.getTerm());
		}
		return sb.toString();
	}
	// news title getter
	public String getSourceTitle() {
		return sourceTitle;
	}
	public List<String> getTitlePOS() {
		return titlePOS;
	}
	public List<Sentence> getTitleSentences() {
		return titleSentences;
	}
	public List<Term> getTitleList() {
		return titleList;
	}
	public String getTitle() {
		if(sourceTitle == null) {
			return concatenateTitle();
		}
		return sourceTitle;
	}
	public String concatenateTitle() {
		StringBuilder sb = new StringBuilder();
		for(Term term : titleList) {
			sb.append(term.getTerm());
		}
		return sb.toString();
	}
	
}
