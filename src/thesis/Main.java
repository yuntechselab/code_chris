package thesis;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import tools.files.LoadProps;

public class Main {

	/**
	 * 執行人事時地物查詢。 沒再用了
	 * 
	 * @param args
	 * @throws SQLException
	 * @throws ClassNotFoundException
	 */
	private String querWord;

	public static void main(String[] args) throws ClassNotFoundException,
			SQLException {
		// System.out.println(Main.parseRuleForSingleTerm("噶瑪魏斯理"));
		System.out
				.println(process4Web(
						"颱風「燦樹」撲日 狂風暴雨亂交通",
						"要去日本的民眾，要多留意了，今年第七號颱風「璨樹」，正朝日本北海道前進，颱風暴風圈，更早在昨天(16日)，就籠罩關東以及東北地區，帶來狂風暴雨，福島累計雨量超過200毫米，茨城縣更緊急撤離100多戶民眾，山形新幹線今天(17日)早上被迫停開，包括仙台等地機場，也有數十個架次停飛。NHK記者：「這裡是(茨城)水戶市中心的幹道，可以看到路面已被大水淹沒。」暴雨不斷，道路積水半個輪胎高，汽車在水裡開、行人在水裡走，出門從頭濕到腳，超級狼狽。今年第七號颱風「璨樹」，16日起暴風圈開始籠罩日本東半部，關東、東北和北海道部分地區，都颳起狂風暴雨，累計雨量飆破數百毫米，部分區域已經發布避難警報，開始撤離居民。民眾：「我是一個人住的，所以來這邊比較安心一點。」NHK主播：「颱風今後預計繼續北上，沿著東北的太平洋沿岸往北前進，今天(17日)傍晚到晚間，將非常接近北海道地區。」北海道和千葉等地，頻傳有民眾汽車，被大水圍困，拋錨在路中央等待救援。岸邊也掀起6公尺以上的巨浪，JR山形線和鹿島線，17日都被迫停開，其餘鐵公路和飛機班次，也受到影響。NHK記者：「羽田機場這邊螢幕已打出通知，表示因為颱風影響，班機可能延誤或停開。」日本氣象廳就表示，從關東到北海道的局部地區，17日深夜為止，還可能降下每小時80毫米的暴雨，北海道累積雨量，更會突破250毫米。提醒民眾要做好防颱準備。（民視新聞謝郁瑩綜合報導） "));
	}

	public void setparseRuleForSingleTerm(String word) {
		this.querWord = word;
	}

	// 傳入查詢詞，回傳查詢詞之符合規則類型，並接續在原始查詢詞後面。ex : queryWord ="總統馬英九" , return queryWord
	// = "總統馬英九(人名)"
	public static String parseRuleForSingleTerm(String queryWord) {
		String s = queryWord;
		NewsData newsData = new NewsData();
		newsData.addTitle(s);
		newsData.addContent(s);
		// 取得sinica的物件
		Sinica sinica = Sinica.getInstance();
		// 針對每篇新聞進行CKIP斷詞
		List<NerNews> segmentedNews = Main.parseWithCKIP(sinica, newsData);
		// 讀取人工規則
		Map<String, List<String>> rules = readManualRuleFromFile(LoadProps
				.getProperties("rulePath"));
		// 比對字詞的人，事，地，時
		Sentence sentence = segmentedNews.get(0).getTitleSentences().get(0);
		// 判斷是否為單獨Na or Nb or Nc
		if (sentence.getSentencePOS().split("　").length == 2) {
			String[] tmpS = sentence.getSentencePOS().split("　");
			int indexFirst = tmpS[1].indexOf("(");
			int indexEnd = tmpS[1].indexOf(")");
			String pos = tmpS[1].substring(indexFirst + 1, indexEnd);
			if (pos.equals("Na")) {
				return queryWord + "(物)";
			} else if (pos.equals("Nb")) {
				return queryWord + "(人名)";
			} else if (pos.equals("Nc")) {
				return queryWord + "(地名)";
			} else if (pos.equals("Nd")) {
				return queryWord + "";
			}
			// System.out.println(pos);
		}
		// System.out.println("DO:" +sentence.getSentencePOS());
		// System.out.println("length:" +
		// sentence.getSentencePOS().split("　").length);

		Map<String, Map<String, List<List<Term>>>> satisfy = satisfyRule(
				sentence, rules);
		// System.out.println("START");
		Map<String, List<List<Term>>> parseResult = satisfy.get("rule");
		String matchRule = "";
		int count = 0;
		for (String lineOfRule : parseResult.keySet()) {
			// System.out.println(ss);
			List<List<Term>> ttt = parseResult.get(lineOfRule);
			if (ttt.size() != 0) {
				matchRule = lineOfRule;
				if (count == 0) {
					count++;
					// System.out.println(ttt);
				} else {
					matchRule = "More";
					// System.out.println(ttt);
					break;
				}
			}
		}
		System.out.println(queryWord + " 符合規則為:" + matchRule);

		// 判斷是否只符合一個規則，並取得符合規則之類型
		if (matchRule.equals("More") || matchRule.equals("")) {
			return queryWord;
		} else {
			String tmpType = null;
			boolean flag = false;
			for (String ss : rules.get("rule")) {
				if (ss.contains("==")) {
					tmpType = ss.replace("==", "");
					continue;
				} else {
					if (ss.equals(matchRule)) {
						flag = true;
						break;
					}
				}
			}
			if (flag) {
				return queryWord + "(" + tmpType + ")";
			} else {
				return queryWord;
			}

		}
	}

	public static String process4Web(String title, String content) {
		System.out.println(title);
		System.out.println(content);
		NewsData newsData = new NewsData();
		newsData.addTitle(title);
		newsData.addContent(content);
		Map<String, Map<String, Map<String, Integer>>> result = Main
				.process(newsData);
		StringBuilder sb = new StringBuilder();
		System.out.println("block~~~~");
		for (String t : result.keySet()) {
			sb.append(t).append("\n\n");
			System.out.println("T" + t);
			for (String filename : result.get(t).keySet()) {
				System.out.println(filename);
				if ("Nb".equals(filename)) {
					sb.append("找人名：");
				} else if ("Nc".equals(filename)) {
					sb.append("找地點：");
				} else if ("Na".equals(filename)) {
					sb.append("找事物：");
				} else if ("Nd".equals(filename)) {
					sb.append("找時間：");
				}
				sb.append(result.get(t).get(filename).toString());
				sb.append("\n\n");
			}
			sb.append("\n\n");
		}
		return sb.toString();
	}

	private static Map<String, Map<String, Map<String, Integer>>> process(
			NewsData newsData) {
		Map<String, Map<String, Map<String, Integer>>> result = new HashMap<String, Map<String, Map<String, Integer>>>();
		long startTime = System.currentTimeMillis();

		// 取得sinica的物件
		Sinica sinica = Sinica.getInstance();
		// 針對每篇新聞進行CKIP斷詞
		List<NerNews> segmentedNews = parseWithCKIP(sinica, newsData);
		long ckipTime = System.currentTimeMillis();

		// 讀取人工規則
		System.out.println("讀取人工規則...");
		Map<String, List<String>> rules = readManualRuleFromFile(LoadProps
				.getProperties("rulePath"));
		for (String filename : rules.keySet()) {
			System.out.println(filename);
			for (String rule : rules.get(filename)) {
				System.out.println("rule: " + rule);
			}
		}
		System.out.println("\n\n");

		// 計算每篇新聞的每一句子符合人工規則的數量
		System.out.println("********[句子符合rule的數量]********");
		for (NerNews news : segmentedNews) {
			for (Sentence sentence : news.getTitleSentences()) {
				Map<String, Map<String, List<List<Term>>>> satisfy = satisfyRule(
						sentence, rules);
				printSentenceSatisfyRulesResult(satisfy, sentence);
			}
			// for(Sentence sentence : news.getContentSentences()) {
			// Map<String, Map<String, List<List<Term>>>> satisfy =
			// satisfyRule(sentence, rules);
			// printSentenceSatisfyRulesResult(satisfy, sentence);
			// }
		}
		System.out.println("\n\n");

		// 計算每一條人工規則符合句子的數量
//		System.out.println("********[rule符合句子的數量]********");
//		for (String filename : rules.keySet()) {
//			System.out.println("--------------------找" + filename
//					+ "符合規則的句子數量--------------------");
//			for (String rule : rules.get(filename)) {
//				Map<String, List<Sentence>> satisfySentence = satisfySentence(
//						rule, segmentedNews);
//				printRuleSatisfySentencesResult(satisfySentence);
//			}
//		}
		System.out.println("\n\n");

		// 計算每一條人工規則符合新聞的數量
		System.out.println("********[rule符合新聞的數量]********");
		// for(String filename : rules.keySet()) {
		// System.out.println("--------------------找"+filename+"符合規則的新聞數量--------------------");
		// for(String rule : rules.get(filename)) {
		// Map<String, List<News>> satisfyNews = satisfyNews(rule,
		// segmentedNews);
		// printRuleSatisfyNewsResult(satisfyNews);
		// }
		// }
		System.out.println("\n\n");

		// 找出每篇新聞的人，事，地，時
		System.out.println("********[每篇新聞的人，事，地，時]********");
		for (NerNews news : segmentedNews) {
			Map<String, Map<String, Integer>> answers = new HashMap<String, Map<String, Integer>>();
			for (Sentence sentence : news.getTitleSentences()) {
				System.out.println("DO:" + sentence.getSentence());
				System.out.println("DO:" + sentence.getSentencePOS());

				Map<String, Map<String, List<List<Term>>>> satisfy = satisfyRule(
						sentence, rules);
				countSentenceSatisfyRulesResult(satisfy, sentence, answers);
			}
//			 for(Sentence sentence : news.getContentSentences()) {
//			 Map<String, Map<String, List<List<Term>>>> satisfy =
//			 satisfyRule(sentence, rules);
//			 countSentenceSatisfyRulesResult(satisfy, sentence, answers);
//			 }
//			 System.out.println(news.getTitle());
			result.put(news.getTitle(), answers);
			for (String filename : answers.keySet()) {
				if ("Nb".equals(filename)) {
					System.out.print("找人：");
				} else if ("Nc".equals(filename)) {
					System.out.print("找地：");
				} else if ("Na".equals(filename)) {
					System.out.print("找事：");
				} else if ("Nd".equals(filename)) {
					System.out.print("找時：");
				}
				System.out.println(answers.get(filename).toString());
			}
			// System.out.println();
		}
		System.out.println("end\n\n");
		long endTime = System.currentTimeMillis();

		// 實驗時間
		// System.out.println("********[實驗時間]********");
		// System.out.println("CKIP斷詞及剖析總時間："+(ckipTime-startTime)/1000.0+" 秒");
		// System.out.println("演算法執行總時間："+(endTime-ckipTime)/1000.0+" 秒");
		// System.out.println("實驗共花了"+(endTime-startTime)/1000.0+"秒");
		// System.out.println("CKIP斷詞及剖析佔總實驗時間比例："+String.format("%1$,.3f",
		// (1.0*(ckipTime-startTime)/(endTime-startTime))*100)+" %");
		// System.out.println("演算法執行佔總實驗時間比例："+String.format("%1$,.3f",
		// (1.0*(endTime-ckipTime)/(endTime-startTime))*100)+" %");

		return result;
	}

	private static void printRuleSatisfySentencesResult(
			Map<String, List<Sentence>> satisfy) {
		for (String rule : satisfy.keySet()) {
			System.out.println(rule + " :  " + satisfy.get(rule).size());
			if (satisfy.get(rule).size() != 0) {
				for (Sentence sentence : satisfy.get(rule)) {
					System.out.println(sentence.getSentencePOS());
				}
			}
		}
	}

	private static void printRuleSatisfyNewsResult(
			Map<String, List<NerNews>> satisfy) {
		for (String rule : satisfy.keySet()) {
			System.out.println(rule + " :  " + satisfy.get(rule).size());
			if (satisfy.get(rule).size() != 0) {
				for (NerNews news : satisfy.get(rule)) {
					System.out.println(news.getTitle());
				}
			}
		}
	}

	private static Map<String, List<Sentence>> satisfySentence(String rule,
			List<NerNews> segmentedNews) {
		Map<String, List<Sentence>> result = new HashMap<String, List<Sentence>>();
		List<Sentence> sentences = new ArrayList<Sentence>();
		for (NerNews news : segmentedNews) {
			for (Sentence sentence : news.getTitleSentences()) {
				if (compareRuleAndSentence(rule, sentence)) {
					sentences.add(sentence);
				}
			}
			for (Sentence sentence : news.getContentSentences()) {
				if (compareRuleAndSentence(rule, sentence)) {
					sentences.add(sentence);
				}
			}
		}
		result.put(rule, sentences);
		return result;
	}

	private static Map<String, List<NerNews>> satisfyNews(String rule,
			List<NerNews> segmentedNews) {
		Map<String, List<NerNews>> result = new HashMap<String, List<NerNews>>();
		List<NerNews> newsList = new ArrayList<NerNews>();
		for (NerNews news : segmentedNews) {
			boolean isSatisfy = false;
			for (Sentence sentence : news.getTitleSentences()) {
				if (compareRuleAndSentence(rule, sentence)) {
					isSatisfy = true;
				}
			}
			for (Sentence sentence : news.getContentSentences()) {
				if (compareRuleAndSentence(rule, sentence)) {
					isSatisfy = true;
				}
			}
			if (isSatisfy) {
				newsList.add(news);
			}
		}
		result.put(rule, newsList);
		return result;
	}

	private static boolean compareRuleAndSentence(String rule, Sentence sentence) {
		boolean result = true;
		FindAllSatisfyTerm tool = FindAllSatisfyTerm.getInstance();
		List<List<Term>> terms = tool.findAllSatisfyTerm(rule, sentence);
		if (terms.size() == 0) {
			result = false;
		}
		return result;
	}

	private static void printSentenceSatisfyRulesResult(
			Map<String, Map<String, List<List<Term>>>> satisfy,
			Sentence sentence) {
		for (String filename : satisfy.keySet()) {
			System.out.print(sentence.getSentence() + "----------------找"
					+ filename);
			int count = 0;
			for (String rule : satisfy.get(filename).keySet()) {
				if (satisfy.get(filename).get(rule).size() != 0) {
					++count;
				}
			}
			System.out.println("    " + count);
			for (String rule : satisfy.get(filename).keySet()) {
				if (satisfy.get(filename).get(rule).size() != 0) {
					System.out.println("規則：" + rule);
					for (List<Term> terms : satisfy.get(filename).get(rule)) {
						System.out.print("[");
						for (Term term : terms) {
							System.out.print(term + ", ");
						}
						System.out.println("]");
					}
				}
			}
		}
		System.out.println();
	}

	private static Map<String, Map<String, Integer>> countSentenceSatisfyRulesResult(
			Map<String, Map<String, List<List<Term>>>> satisfy,
			Sentence sentence, Map<String, Map<String, Integer>> answers) {
		for (String filename : satisfy.keySet()) {
			Map<String, Integer> eachItems;
			if (answers.get(filename) == null) {
				eachItems = new HashMap<String, Integer>();
			} else {
				eachItems = answers.get(filename);
			}
			// System.out.print(sentence.getSentence()+"----------------找"+filename);
			int count = 0;
			for (String rule : satisfy.get(filename).keySet()) {
				if (satisfy.get(filename).get(rule).size() != 0) {
					++count;
				}
			}
			// System.out.println("    "+count);
			for (String rule : satisfy.get(filename).keySet()) {
				if (satisfy.get(filename).get(rule).size() != 0) {
					// System.out.println("規則："+rule);
					for (List<Term> terms : satisfy.get(filename).get(rule)) {
						// System.out.print("[");
						// 合併term

						if (rule.indexOf("(") != -1 && rule.indexOf(")") != -1) {
							// System.out.println("======================="+rule+"===============");
							// for(Term t : terms) {
							// System.out.print(t+", ");
							// }
							// System.out.println();
							List<Term> mergeTerms = new ArrayList<Term>();
							String[] rulePOSs = rule.split("\\+");
							boolean isContinuous = false;
							for (int i = 0; i < rulePOSs.length; i++) {
								Term term = terms.get(i).clone();
								// System.out.println("present term: "+term);
								if (isContinuous) {
									Term previous = mergeTerms.get(mergeTerms
											.size() - 1);
									previous.setTerm(previous.getTerm()
											+ term.getTerm());
									previous.setPartOfSpeech(previous
											.getPartOfSpeech()
											+ "+"
											+ term.getPartOfSpeech());
									// System.out.println("previous: "+previous);
								} else {
									mergeTerms.add(term);
								}
								String rpos = rulePOSs[i];
								if (rpos.indexOf("(") != -1) {
									isContinuous = true;
								} else if (rpos.indexOf(")") != -1) {
									isContinuous = false;
								}
							}
							terms = mergeTerms;
						}

						for (Term term : terms) {
							// System.out.print(term + ", ");
							if (filename.equals(term.getPartOfSpeech())
									|| term.getPartOfSpeech().indexOf(filename) != -1) {
								if (eachItems.get(term.getTerm()) == null) {
									eachItems.put(term.getTerm(), 1);
								} else {
									int occourence = eachItems.get(term
											.getTerm());
									occourence += 1;
									eachItems.put(term.getTerm(), occourence);
								}
							}
						}
						// System.out.println("]");
					}
				}
			}
			answers.put(filename, eachItems);
		}

		return answers;
	}

	private static Map<String, Map<String, List<List<Term>>>> satisfyRule(
			Sentence sentence, Map<String, List<String>> rules) {
		Map<String, Map<String, List<List<Term>>>> result = new HashMap<String, Map<String, List<List<Term>>>>();
		// List<Term> terms = sentence.getTerms(); //取出每個句子的字詞
		// List<String> interestedPOS =
		// InterestedPOS.getInstance().getInterestedPOSList();
		FindAllSatisfyTerm tool = FindAllSatisfyTerm.getInstance();
		for (String filename : rules.keySet()) { // 取得rule的檔名
			for (String rule : rules.get(filename)) { // 取出每條rule
				// System.out.println("DO:"+ rule);
				List<List<Term>> satisfyTerms = tool.findAllSatisfyTerm(rule,
						sentence);
				// System.out.println(satisfyTerms.size());
				if (result.get(filename) == null) {
					result.put(filename,
							new HashMap<String, List<List<Term>>>());
				}
				if (result.get(filename).get(rule) == null) {
					result.get(filename).put(rule, new ArrayList<List<Term>>());
				}
				if (satisfyTerms.size() != 0) {
					result.get(filename).put(rule, satisfyTerms);
				}
			}
		}
		return result;
	}

	public static List<NerNews> parseWithCKIP(Sinica sinica, NewsData newsData) {
		System.out.println("執行CKIP斷詞...");
		if (sinica == null) {
			return null;
		}
		if (newsData == null) {
			return null;
		}
		List<NerNews> result = new ArrayList<NerNews>();
		long ckipTotalTime = 0, parseTermTime = 0, parseSentenceTime = 0;
		for (int i = 0; i < newsData.size(); i++) {
			System.out.println((i + 1) + ":");
			String sourceTitle = newsData.getTitles().get(i);
			System.out.println("原始新聞標題：" + sourceTitle);
			String sourceContent = newsData.getContents().get(i);
			System.out.println("原始新聞內容：" + sourceContent);
			System.out.println("CKIP斷詞中...");
			long time1 = System.currentTimeMillis();
			List<String> titlePOS = sinica.segment(sourceTitle).getUwpos();
			List<String> contentPOS = sinica.segment(sourceContent).getUwpos();
			long time2 = System.currentTimeMillis();
			// System.out.println("取得字詞詞性...");
			long time3 = System.currentTimeMillis();
			List<Term> titleList = parseTermFromSource(titlePOS);
			List<Term> contentList = parseTermFromSource(contentPOS);
			long time4 = System.currentTimeMillis();
			// System.out.println("取得句子...");
			long time5 = System.currentTimeMillis();
			List<Sentence> titleSentences = parseSentenceFromSource(titlePOS);
			List<Sentence> contentSentences = parseSentenceFromSource(contentPOS);
			long time6 = System.currentTimeMillis();
			NerNews news = new NerNews(sourceTitle, sourceContent, titlePOS,
					contentPOS, titleList, contentList, titleSentences,
					contentSentences);
			result.add(news);
			System.out.print("斷詞後的新聞標題：");
			for (Sentence sentence : news.getTitleSentences()) {
				for (Term term : sentence.getTerms()) {
					System.out.print(term);
				}
			}
			System.out.println();
			System.out.print("斷詞後的新聞內容：");
			for (Sentence sentence : news.getContentSentences()) {
				for (Term term : sentence.getTerms()) {
					System.out.print(term);
				}
			}
			System.out.println();
			ckipTotalTime += (time2 - time1);
			parseTermTime += (time4 - time3);
			parseSentenceTime += (time6 - time5);
			// System.out.println("ckip斷詞時間: "+(time2 - time1)+"毫秒");
			// System.out.println("剖析句子時間: "+(time6 - time5)+"毫秒");
			// System.out.println("剖析字詞時間: "+(time4 - time3)+"毫秒");
			// System.out.println();
		}
		// System.out.println("CKIP斷詞總時間： "+(ckipTotalTime/1000.0)+" 秒");
		// System.out.println("每篇新聞平均斷詞時間"+String.format("%1$,.3f",
		// ((ckipTotalTime/1000.0)/result.size()))+" 秒");
		// System.out.println("剖析句子總時間： "+(parseSentenceTime/1000.0)+" 秒");
		// System.out.println("每篇新聞平均剖析句子時間"+String.format("%1$,.3f",
		// ((parseSentenceTime/1000.0)/result.size()))+" 秒");
		// System.out.println("剖析字詞總時間： "+(parseTermTime/1000.0)+" 秒");
		// System.out.println("每篇新聞平均剖析字詞時間"+String.format("%1$,.3f",
		// ((parseTermTime/1000.0)/result.size()))+" 秒");
		// System.out.println("\n\n\n");
		return result;
	}

	private static List<Sentence> parseSentenceFromSource(List<String> sourcePOS) {
		List<Sentence> result = new ArrayList<Sentence>();
		for (String sentencePOS : sourcePOS) {
			sentencePOS = sentencePOS.replace(" ", "");
			List<Term> terms = parseTerm(sentencePOS);
			Sentence sentence = new Sentence(null, sentencePOS, terms);
			result.add(sentence);
		}
		return result;
	}

	private static List<Term> parseTerm(String sourcePOS) {
		List<Term> result = new ArrayList<Term>();
		if (sourcePOS == null || "".equals(sourcePOS)) {
			return result;
		}
		sourcePOS = sourcePOS.replace(" ", "");
		// String[] ss = s.split("\\([a-zA-Z]+\\)");
		String[] termPOSs = sourcePOS.split("[　| ]");
		for (String termPOS : termPOSs) {
			if (termPOS != null && !"".equals(termPOS)) {
				if (termPOS.indexOf("((") != -1) {
					termPOS = termPOS.replace("((", "（(");
				}
				if (termPOS.indexOf(")(") != -1) {
					termPOS = termPOS.replace(")(", "）(");
				}
				termPOS = termPOS.split("\\)")[0];
				String[] terms = termPOS.split("\\(");
				if (terms.length > 1) {
					Term term = new Term(terms[0], terms[1], 0);
					result.add(term);
				}
			}
		}
		for (int i = 0; i < result.size(); i++) {
			Term term = result.get(i);
			term.setPosition(i);
		}
		return result;
	}

	private static List<Term> parseTermFromSource(List<String> sourcePOS) {
		List<Term> result = new ArrayList<Term>();
		for (String sentencePOS : sourcePOS) {
			result.addAll(parseTerm(sentencePOS));
		}
		return result;
	}

	private static Map<String, List<String>> readManualRuleFromFile(
			String filepath) {
		System.out.println("讀取檔案：" + filepath);
		Map<String, List<String>> result = new HashMap<String, List<String>>();
		try {
			File file = new File(filepath);
			// 判斷是否為資料夾，若為資料夾即遞迴呼叫該方法
			if (file.isDirectory()) {
				String[] subfilenames = file.list();
				for (String subfilename : subfilenames) {
					String path = new StringBuilder(filepath).append("/")
							.append(subfilename).toString();
					result.putAll(readManualRuleFromFile(path));
				}
			} else {

				String filename = file.getName();
				String filenameExtension = filename.substring(
						filename.indexOf(".") + 1, filename.length())
						.toUpperCase();
				if ("TXT".equals(filenameExtension)) {
					List<String> rules = new ArrayList<String>();
					BufferedReader fin = new BufferedReader(
							new InputStreamReader(new FileInputStream(file),
									"UTF-8")); // 需指定編碼，TXT需存成UTF-8編碼
					// BufferedReader fin = new BufferedReader(new
					// FileReader(file));

					while (fin.ready()) {
						rules.add(fin.readLine());
					}
					fin.close();
					result.put(filename.substring(0, filename.indexOf(".")),
							rules);
				}
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return result;
	}
}
