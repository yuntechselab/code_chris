package thesis;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;

public class Sinica {
	private static Sinica instance;
	private static final String ckipurl = "http://sunlight.iis.sinica.edu.tw/cgi-bin/text.cgi"; // CKIP的連結
	private ArrayList<String> uwpos = null; // 存取每個詞
	private HashMap<String, String> uw_list = null; // 未知詞清單

	public static Sinica getInstance() {
		if(instance == null) {
			instance = new Sinica();
		}
		return instance;
	}
	private Sinica() { // 建構子
	}

	private Sinica(String query) { // 建構子
		segment(query);
	}

	public Sinica segment(String query) {
		// TODO Auto-generated method stub
		//System.out.println(query);
		HashMap<String, String> data = new HashMap<String, String>();
		data.put("query", query);
		String ckipout = getPostResponse(ckipurl, data);
		//System.out.println(ckipout);
		
		String id = ckipout.substring(ckipout.indexOf("pool/") + 5, ckipout
				.lastIndexOf(".html"));
		String ans1 = "http://sunlight.iis.sinica.edu.tw/uwextract/pool/" + id
				+ ".txt.process.txt";
		String ans2 = "http://sunlight.iis.sinica.edu.tw/uwextract/pool/" + id
				+ ".tag.txt";
		String ans3 = "http://sunlight.iis.sinica.edu.tw/uwextract/pool/" + id
				+ ".uw.txt";
		String uwprocess = "";
		uwpos = new ArrayList<String>();
		uw_list = new HashMap<String, String>();
		
		try {
			//System.out.println(getResponse(ans1));
			uwprocess = ContentFilter.reNum(getResponse(ans1));
			//System.out.println(uwprocess);
			//System.out.println(getResponse(ans2));
			for (String sentence : ContentFilter.reNum(
					getResponse(ans2).replaceAll("-", "")).split("\n\n")) {
				uwpos.add(sentence.trim());
				//System.out.println(sentence);
			}
			//System.out.println(getResponse(ans3));
			for (String item : ContentFilter.reNum(getResponse(ans3)).split(
					"\n")) {
				if (!"".equals(item)) {
					uw_list.put(item.substring(0, item.indexOf("  ")), "XX");
				}
			}
		} catch (Exception e) {
			System.out.println("CKIP Error File Not Found!!");
		}
		
		for (String line : uwprocess.split("\n")) {
			if (line.indexOf("中國人名") != -1) {
				line = line.replaceAll("\\(\\??\\)\\(\\w+\\)", "").substring(
						line.indexOf("　") + 1)
						+ "　";
				for (String term : line.split("　")) {
					if (uw_list.containsKey(term))
						if ("XX".equals(uw_list.get(term))) {
							uw_list.put(term, "CH");
						}
				}
			} else if (line.indexOf("歐美譯名") != -1) {
				line = line.replaceAll("\\(\\??\\)\\(\\w+\\)", "").substring(
						line.indexOf("　") + 1)
						+ "　";
				for (String term : line.split("　")) {
					if (uw_list.containsKey(term))
						if ("XX".equals(uw_list.get(term))) {
							uw_list.put(term, "EN");
						}
				}
			} else if (line.indexOf("MergingAlgo") != -1) {
				line = line.replaceAll("\\(\\??\\)\\(\\w+\\)", "").substring(
						line.indexOf("　") + 1)
						+ "　";
				for (String term : line.split("　")) {
					if (uw_list.containsKey(term))
						if ("XX".equals(uw_list.get(term))) {
							uw_list.put(term, "MA");
						}
				}
			} else if (line.indexOf("複合詞 ") != -1) {
				line = line.replaceAll("\\(\\??\\)\\(\\w+\\)", "").substring(
						line.indexOf("　") + 1)
						+ "　";
				for (String term : line.split("　")) {
					if (uw_list.containsKey(term))
						if ("XX".equals(uw_list.get(term))) {
							uw_list.put(term, "CT");
						}
				}
			} else if (line.indexOf("(DM)") != -1) {
				line = line.replaceAll("\\(\\??\\)", "").substring(
						line.indexOf("　") + 1)
						+ "　";
				for (String term : line.split("\\)　")) {
					String pos = term.substring(term.indexOf("(") + 1);
					term = term.substring(0, term.indexOf("("));
					if ("DM".equals(pos)) {
						uw_list.put(term, pos);
					}
				}
			}
		}
		return this;
	}

	private String getPostResponse(String link, HashMap<String, String> data) { // 送出Post並取得Response
		// TODO Auto-generated method stub
		StringBuffer buf = new StringBuffer();
		
		try {
			URL url = new URL(link);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestProperty("User-Agent", "Mozilla/5.0");
			conn.setRequestProperty("Referer",
					"http://sunlight.iis.sinica.edu.tw/uwextract/");
			conn.setRequestProperty("Content-Type", "text;charset=big5");
			conn.setRequestMethod("POST");
			conn.setDoOutput(true);
			conn.setDoInput(true);
			PrintWriter output = new PrintWriter(new OutputStreamWriter(conn
					.getOutputStream(), "CP950"));
			
			for (String param : data.keySet()) {
				output.print(param + "=" + data.get(param));
				output.flush();
			}
			
			output.close();
			BufferedReader in = new BufferedReader(new InputStreamReader(conn
					.getInputStream(), "CP950"));
			String line;
			
			while ((line = in.readLine()) != null) {
				buf.append(line + "\n");
			}
			
			in.close();
			conn.disconnect();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return buf.toString();
	}

	private String getResponse(String link) throws IOException { // 獲取資訊
		// TODO Auto-generated method stub
		StringBuffer buf = new StringBuffer();
		URL url = new URL(link);
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		conn.setRequestProperty("User-Agent", "Mozilla/5.0");
		conn.setRequestProperty("Referer",
				"http://sunlight.iis.sinica.edu.tw/uwextract/");
		conn.setRequestProperty("Content-Type", "text;charset=big5");
		BufferedReader in = new BufferedReader(new InputStreamReader(conn
				.getInputStream(), "CP950"));
		String line;

		while ((line = in.readLine()) != null) {
			buf.append(line + "\n");
		}

		in.close();
		conn.disconnect();

		return buf.toString();
	}
	
	public HashMap<String, String> getUwlist() { // 取得未知詞清單
		return uw_list;
	}
	
	public ArrayList<String> getUwpos() {
		// TODO Auto-generated method stub
		return uwpos;
	}

}
