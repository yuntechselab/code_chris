package thesis;

import java.util.ArrayList;
import java.util.List;

public class InterestedPOS {
	private String[] pos;
	private List<String> posList;
	private static InterestedPOS instance;
	private InterestedPOS() {
		this.pos = new String[] {
				"Na", "Nb", "Nc", "Ncd", "Nd",
				"VA", "VAC", "VB", "VC", "VCL",
				"VD", "VE", "VF", "VG", "VH", "VHC",
				"VI", "VJ", "VK", "VL", "V_2"
		};
		posList = null;
	}
	public static InterestedPOS getInstance() {
		if(instance == null) {
			instance = new InterestedPOS();
		}
		return instance;
	}
	public String[] getInterestedPOS() {
		return pos;
	}
	public List<String> getInterestedPOSList() {
		if(posList == null) {
		posList = new ArrayList<String>();
			for(String s : pos) {
				posList.add(s);
			}
		}
		return posList;
	}
}
