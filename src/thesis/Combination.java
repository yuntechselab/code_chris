package thesis;

public class Combination {
	int n, m;

	int[] pre;// previous combination.

	public Combination(int n, int m) {
		this.n = n;
		this.m = m;
	}

	/**
	 * 取下一个组合。可避免一次性返回所有的组合(数量巨大，浪费资源)。 if return null,所有组合均已取完。
	 */
	public int[] next() {
		if(m < n) return null;
		if (pre == null) {// 取第一个组合，以后的所有组合都经上一个组合变化而来。
			pre = new int[n];
			for (int i = 0; i < pre.length; i++) {
				pre[i] = i;
			}
			int[] ret = new int[n];
			System.arraycopy(pre, 0, ret, 0, n);
			return ret;
		}
		int ni = n - 1, maxNi = m - 1;
		while (pre[ni] + 1 > maxNi) {// 从右至左，找到有增量空间的位。
			ni--;
			maxNi--;
			if (ni < 0)
				return null;// 若未找到，说明了所有的组合均已取完。
		}
		pre[ni]++;
		while (++ni < n) {
			pre[ni] = pre[ni - 1] + 1;
		}
		int[] ret = new int[n];
		System.arraycopy(pre, 0, ret, 0, n);
		return ret;
	}
}
