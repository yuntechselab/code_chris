package thesis;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class ThesisMain {
	public static String compare(String[] terms, String rule, boolean continuous) {
		StringBuilder sb = new StringBuilder();
		
		return sb.toString();
	}
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String[] rules = new String[] {
				"Na+Nc+Nb",
				"(Na+Nc+Nb)",
				"Nc+(Na+Nc+Nb)",
				"(Na+Nc+Nb)+VE",
				"Nc+(Na+Nc+Nb)+VE"
		};
		String[] terms = new String[] {
				"Nc","Na","Na","Nc","Nb","Ne","P","VE","Na","Nc","Nb","Neu"
		};
		for(String rule : rules) {
			System.out.println("---------------"+rule+"---------------");
			String[] rulePOSs = rule.split("\\+");
			for(int times=0; times<terms.length-rulePOSs.length+1; times++) {
				System.out.println(times+":");
				for(int i=0, index=times; i<rulePOSs.length&&index<terms.length; index++) {
					String pos = rulePOSs[i];
					if(rulePOSs[i].indexOf("(") != -1) {
						pos = pos.split("\\(")[1];
					}
					if(rulePOSs[i].indexOf(")") != -1) {
						pos = pos.split("\\)")[0];
					}
					if(pos.equals(terms[index])) {
						System.out.print(terms[index]+"("+index+") ");
						i++;
					}
				}
				System.out.println();
			}
		}
		// TODO Auto-generated method stub
//		try {
			/* step 1
			 * 連結資料庫
			 */
/*			String url = "140.125.84.58:3306";
			String dbName = "news_db";
			Map<String, String> params = new HashMap<String, String>();
			params.put("useUnicode", "true");
			params.put("characterEncoding", "UTF8");
			String user = "root";
			String password = "huangcm";
			Connection connNewsDB = AccessDB.getConnection(url, dbName, params, user, password);
			System.out.println("成功連結資料庫" + connNewsDB.toString());
			
			/* step 2
			 * 取字詞庫裡的"人名字詞"
			 */
/*			Map<String, String> personNameCorpus = AccessDB.getPersonNameCorpus();
			System.out.println("共有" + personNameCorpus.size() + "筆人名");
			System.out.println(personNameCorpus.toString());
			
			/* step 3
			 * 輸入問句查詢(主要查詢人名)
			 */
			
/*			BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
			String input = br.readLine();
			
			
			/* step 4
			 * 查詢新聞資料庫(140.125.84.58/news_db)
			 */
/*			Statement stmt = connNewsDB.createStatement();
			ResultSet rs = null;
			String sql = "";
			while(!input.toLowerCase().equals("exit")) {
				//sql = "SELECT * FROM news201106 WHERE news_title LIKE '%"+input+"%' OR news_content LIKE '%"+input+"%' ORDER BY date desc LIMIT 10";
				sql = "SELECT * FROM news201106";
				rs = stmt.executeQuery(sql);
				ResultSetMetaData rsmd = rs.getMetaData();
				for(int i=0; i<rsmd.getColumnCount(); i++) {
					System.out.println(rsmd.getColumnName(i));
				}
			}
			/* step 5
			 * 人名斷詞
			 */
			
			/* step 6
			 * 轉換成為2元矩陣
			 */
			
			/* step 7
			 * 找出答案
			 */
			
			/*
			 * 關閉資料庫
			 */
//			connNewsDB.close();
//		} catch (ClassNotFoundException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} catch (SQLException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		
	}
	public static void testTransform() {
		String[] columns = {"fish", "beef", "pork", "chicken"};
		String[] rows = {"fred", "jess", "bob", "mel"};
		int[][] matrix = {{1, 0, 0, 1},
						  {1, 1, 1, 0},
						  {1, 0, 1, 1},
						  {1, 0, 1, 1}};
		double threshold = 0.75;
		int maxSupport = 0;
		double maxS = 0.0;
		for(int i=0; i<columns.length; i++) {
			int count = 0;
			for(int j=0; j<rows.length; j++) {
				count += matrix[j][i];
			}
			double s = ((double)count)/((double)rows.length);
			if(s > maxS) {
				maxS = s;
				maxSupport = i;
			}
		}
		for(int i=0; i<columns.length; i++) {
			if(i == maxSupport) continue;
			int count = 0;
			for(int j=0; j<rows.length; j++) {
				if(matrix[j][maxSupport] == 1 && matrix[j][i] == 1) {
					count ++;
				}
			}
			double s = ((double)count)/((double)rows.length);
			
		}
	}
}
