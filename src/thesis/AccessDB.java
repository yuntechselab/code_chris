package thesis;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

public class AccessDB {
	public static Connection getConnection(String url, String dbName, Map<String, String> params, String user, String password) throws ClassNotFoundException, SQLException {
		if(url.equals("") || url == null) {
			return null;
		}
		StringBuilder sb = new StringBuilder();
		sb.append("jdbc:mysql://"+url);
		sb.append("/"+dbName);
		sb.append("?");
		for(String key : params.keySet()) {
			sb.append(key);
			sb.append("="+params.get(key));
			sb.append("&");
		}
		sb.append("user="+user);
		sb.append("&password="+password);
		return getConnection(sb.toString());
		
	}
	public static Connection getConnection(String url) throws ClassNotFoundException, SQLException {
		if(url.equals("") || url == null) {
			return null;
		}
		String driverName = "com.mysql.jdbc.Driver";
		Class.forName(driverName); //載入驅動程式
		Connection conn = null;
		conn  = DriverManager.getConnection(url);
		return conn;
	}
	public static Map<String, String> getPersonNameCorpus() throws ClassNotFoundException, SQLException {
		Map<String, String> result = new HashMap<String, String>();
		String url = "jdbc:mysql://140.125.84.58:3306/corpus?useUnicode=true&characterEncoding=UTF8&user=root&password=huangcm";
		Connection conn = getConnection(url);
		String sql = "SELECT name FROM cname";
		PreparedStatement pstmt= conn.prepareStatement(sql);
		ResultSet rs = pstmt.executeQuery();
		while(rs.next()) {
			result.put(rs.getString("name"), rs.getString("name"));
		}
		return result;
	}
} 
