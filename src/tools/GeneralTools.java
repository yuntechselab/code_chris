package tools;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class GeneralTools {

    // 判斷是否是全英文字&數字
    public static boolean isEngish(String txt) {
	boolean result = false;

	result = txt.matches("[a-zA-Z\\s\\p{Punct}\\p{ASCII}]*");
	if (!result) {
	    result = txt.matches("[a-zA-Z\\s\\p{Punct}‘’]*");
	}
	return result;
    }

    // 根據標點符號切開段落
    public static List<String> splitToSentence(String txt) {
	String[] sentencesA = txt.split("[\\，\\。\\(\\)\\「\\」\\（\\）\\、\\.\\「\\」\\〔\\〕\\／]");
	List<String> sentences = new ArrayList<String>();
	sentences = (List<String>) Arrays.asList(sentencesA);
	return sentences;
    }

    public static String getDomainName(String url) throws URISyntaxException {
	URI uri = new URI(url);
	String domain = uri.getHost();
	return domain.startsWith("www.") ? domain.substring(4) : domain;
    }

    // 取得這個字在這個pattern的所有範圍
    public static ArrayList<Integer> getRangeFromText(String inputStr, String patternStr) {
	ArrayList<Integer> result = new ArrayList<Integer>();
	Pattern pattern = Pattern.compile(patternStr);
	Matcher matcher = pattern.matcher(inputStr);
	boolean matchFound = matcher.find();
	while (matchFound) {
	    result.add(matcher.start());
	    if (matcher.end() + 1 <= inputStr.length()) {
		matchFound = matcher.find(matcher.end());
	    } else {
		break;
	    }
	}
	return result;
    }

    // 取得括號內的文字
    public static ArrayList<String> getWithinTxt(String inputStr, String leftSyn, String rightSyn) {
	ArrayList<String> result = new ArrayList<String>();
	ArrayList<Integer> left = getRangeFromText(inputStr, leftSyn);
	ArrayList<Integer> right = getRangeFromText(inputStr, rightSyn);
	for (int i = 0; i < left.size(); i++) {
	    int leftIndex = left.get(i);
	    int rightIndex = right.get(i);
	    String theWithinTxt = inputStr.substring(leftIndex + 1, rightIndex);
	    result.add(theWithinTxt);
	}

	left = getRangeFromText(inputStr, leftSyn);
	right = getRangeFromText(inputStr, rightSyn);
	for (int i = 0; i < left.size(); i++) {
	    int leftIndex = left.get(i);
	    int rightIndex = right.get(i);
	    String theWithinTxt = inputStr.substring(leftIndex + 1, rightIndex);
	    result.add(theWithinTxt);
	}
	return result;
    }

    // 取得括號內的文字
    public static ArrayList<String> getWithinTxtEng(String inputStr, String leftSyn, String rightSyn) {
	ArrayList<String> temp = getWithinTxt(inputStr, leftSyn, rightSyn);
	return temp;
    }
}
