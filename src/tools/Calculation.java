package tools;

public class Calculation {
    public static double calculateWeight(int newsSize, double TF, int DF) {
	double result = 0;
	result = TF * (getLog(newsSize / DF, 10) + 1);
	return result;
    }

    public static double getLog(double value, double base) {
	return Math.log(value) / Math.log(base);
    }
}
