package tools;

import java.util.*;

public class MapValueSort {
    /** inner class to do sorting of the map **/
    public static class ValueComparer implements Comparator {
	private Map _data = null;

	public ValueComparer(Map data) {
	    super();
	    _data = data;
	}

	public int compare(Object o1, Object o2) {
	    String e1 = (String) _data.get(o1);
	    String e2 = (String) _data.get(o2);
	    return e1.compareTo(e2);
	}
    }

    public static void printMap(Map data) {
	for (Iterator iter = data.keySet().iterator(); iter.hasNext();) {
	    String key = (String) iter.next();
	    System.out.println("Value/key:" + data.get(key) + "/" + key);
	}
    }
}
