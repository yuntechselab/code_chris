package tools;

import java.util.HashMap;
import java.util.Map;


/***
 * getTerms�A�N��Ʈw��줧terms�^���X�ӡC
 * getTermsTF�A�p��Terms���W�v�C
 * //removeDuplicate�A�h���Q�]�t��Terms�C
 * @author Chris
 *
 */
public class CKIPTool {	
	// getTerms
		public static Map<String,Integer> getTerms(String content){
			Map<String, Integer> result = new HashMap<String, Integer>();
			String[] tempContent = content.split("\\(");
			boolean skip = true;
			for(String s : tempContent){
				if(skip){
					skip = false;
					continue;
				}else{
					if(s.substring(0,1).matches("[\u4E00-\u9fa5]")){
						for(int i=0; i<s.length(); i++){
							if(s.substring(i,i+1).matches("[\u4E00-\u9fa5]|\\?|\\�H")){//[\u4E00-\u9fa5] ����r�d��
								continue;
							}else{
								result.put(s.substring(0,i),1);
								break;
							}
						}
					}
				}
			}

			return result;
		}
		//getTermsTF
		public static Map<String, Integer> getTermTF(Map<String,Integer> Terms,String content){
			for(String s: Terms.keySet()){
				int tf = content.split(s).length;
				Terms.put(s, tf);
			}
			return Terms;
		}
		
		//removeDuplicate
		public static Map<String , Integer> removeDuplicate(Map<String , Integer> Terms){
			String[] temp = new String[Terms.keySet().size()];
			String[] remove = new String[Terms.keySet().size()*10]; 
			int i=0;
			int k=0;
			for(String str : Terms.keySet()){
				temp[i] = str;
				i++;
			}
				
			for(int j=0; j<temp.length; j++){
				for(String str2 : Terms.keySet()){
					if(temp[j].contains(str2) && !temp[j].equals(str2)){
						//System.out.println("!!"+str2);
						remove[k] = str2;
						k++;
					}
				}
			}
			for(String str4: remove){
				Terms.remove(str4);
			}
			return Terms;
		}
}
