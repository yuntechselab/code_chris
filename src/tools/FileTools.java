﻿package tools;

import java.io.*;
import java.net.URL;
import java.nio.charset.Charset;

public class FileTools {
	// filename : D:\\text.txt
	public static boolean writeText(String text, String filename,
			String format, boolean append) {
		if (text.equals("")) {
			return false;
		}
		File file = new File(filename);//
		try {
			BufferedWriter bufWriter = new BufferedWriter(
					new OutputStreamWriter(new FileOutputStream(file, append),
							format));
			bufWriter.write(text);
			bufWriter.close();
			System.out.println(filename + "寫入成功");
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("error");
			return false;
		}
		return true;
	}

	public static String readFile(String filename) throws IOException {
		String content = "";
		File file = new File(filename);
		FileInputStream fis = null;
		BufferedInputStream bis = null;
		DataInputStream dis = null;
		try {
			fis = new FileInputStream(file);
			bis = new BufferedInputStream(fis);
			dis = new DataInputStream(bis);
			while (dis.available() != 0) {
				content += dis.readLine() + "\r\n";
			}
			fis.close();
			bis.close();
			dis.close();
			content = new String(content.getBytes("8859_1"), "UTF8");
			String strChineseString = content;
			byte[] byteUTF8 = null;
			byteUTF8 = strChineseString.getBytes(Charset.forName("utf-8"));
			return new String(byteUTF8, "utf-8");

		} catch (FileNotFoundException e) {
			System.out.println(e.toString());
			return null;
		} catch (IOException e) {
			System.out.println(e.toString());
			return null;
		}
	}

	public static void writeFile(String filename, String text)
			throws IOException {
		FileOutputStream fos = null;
		try {
			fos = new FileOutputStream(filename);
			fos.write(text.getBytes("utf-8"));
			System.out.println(filename + "寫入成功");
		} catch (IOException e) {
			close(fos);
			throw e;
		}
	}

	public static void close(Closeable closeable) {
		try {
			closeable.close();
		} catch (IOException ignored) {
		}
	}

	public static boolean getPhotoFromURL(String photoUrl, String filePath) {
		try {
			URL url = null;
			try {
				url = new URL(photoUrl);
			} catch (Exception e) {
				System.out.println("URL ERROR");
				return false;
			}
			FilterInputStream in = (FilterInputStream) url.openStream();
			File fileOut = new File(filePath);
			FileOutputStream out = new FileOutputStream(fileOut);
			byte[] bytes = new byte[1024];
			int c;
			while ((c = in.read(bytes)) != -1) {
				out.write(bytes, 0, c);
			}
			in.close();
			out.close();
			return true;
		} catch (Exception e) {
			System.out.println("Error!");
			return false;
		}
	}
}
