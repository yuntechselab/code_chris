package tools;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class NewsTools {
    public static Map<String, Integer> extractTerms(String segmentedContent) {
	Map<String, Integer> result = new HashMap<String, Integer>();
	Map<String, Integer> terms = new HashMap<String, Integer>();
	segmentedContent = segmentedContent.replaceAll("<\\[", "<");
	segmentedContent = segmentedContent.replaceAll("]>", ">");
	String[] removeLeftSurrounds = segmentedContent.split("<");
	for (int i = 1; i < removeLeftSurrounds.length; i++) {
	    String cname = removeLeftSurrounds[i].split(">")[0];
	    int value = 1;
	    if (terms.containsKey(cname)) {
		value = terms.get(cname) + 1;
	    }
	    terms.put(cname, value);

	}
	removeLeftSurrounds = segmentedContent.split("\\[");
	for (int i = 1; i < removeLeftSurrounds.length; i++) {
	    String term = removeLeftSurrounds[i].split("]")[0];
	    int value = 1;
	    if (terms.containsKey(term)) {
		value = terms.get(term) + 1;
	    }
	    terms.put(term, value);
	}

	Set<String> keys = terms.keySet();
	for (String s : keys) {
	    if (terms.get(s) > 0)
		result.put(s, terms.get(s));
	}
	return result;
    }
}
