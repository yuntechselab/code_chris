package tools;

public class CalcTimes {
	/*
	 * 計算執行時間所需的欄位
	 * 
	 * Field		Description
	 * ================================
	 * startTime 		開始時間
	 * finalTime		結速時間
	 * 
	 */
	private static long startTime;
	private static long finalTime;
	
	/*
	 * 記錄開始的時間
	 */
	public static void setStartTime(){
		startTime = System.nanoTime();
	}
	
	/*
	 * 取得開始的時間
	 */
	public static long getStartTime(){
		return startTime;
	}
	
	/*
	 * 記錄結速的時間
	 */
	public static void setFinalTime(){
		finalTime = System.nanoTime();
	}
	
	/*
	 * 取得結束的時間
	 */
	public static long getFinalTime(){
		return finalTime;
	}
	
	/*
	 * 取得執行時間
	 */
	public static String getMeasure(){
		setFinalTime();
		return String.format("%.9f sec.", (double)(getFinalTime() - getStartTime()) / Math.pow(10, 9));
	}

}
