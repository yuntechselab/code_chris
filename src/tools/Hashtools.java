package tools;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import entity.WikiTermBean;
import synonymResearch.WikiTermBean_backup;


public class Hashtools {

    // <Term,TF>
    public static HashMap<String, Integer> StringToHash(String s) {
	HashMap<String, Integer> result = new HashMap<String, Integer>();
	s = s.replace("{", "");
	s = s.replace("}", "");
	s = s.replace(" ", "");
	String[] termList = s.split(",");
	for (int i = 0; i < termList.length; i++) {
	    result.put(termList[i].split("=")[0], new Integer(termList[i].split("=")[1])); // Term
											   // =
											   // TF
	}
	return result;
    }
    
    //Summarization<Sentence,TF>
    public static HashMap<String, Double> summarizationStrToHash(String s) {
	HashMap<String, Double> result = new HashMap<String, Double>();
	s = s.replace("{", "");
	s = s.replace("}", "");
	s = s.replace(" ", "");
	String[] termList = s.split(",");
	for (int i = 0; i < termList.length; i++) {
	    String[] dataSegment = termList[i].split("=");
	    result.put(dataSegment[0], new Double(dataSegment[1])); // Sentence,TF
	}
	return result;
    }
   
    public static Map sortByValue(Map map) {
	List list = new LinkedList(map.entrySet());
	Collections.sort(list, new Comparator() {
	    public int compare(Object o1, Object o2) {
		return ((Comparable) ((Map.Entry) (o2)).getValue()).compareTo(((Map.Entry) (o1)).getValue());
	    }
	});

	Map result = new LinkedHashMap();
	for (Iterator it = list.iterator(); it.hasNext();) {
	    Map.Entry entry = (Map.Entry) it.next();
	    result.put(entry.getKey(), entry.getValue());
	}
	return result;
    }
    /*
     
     */
    //由大到小，依據Key的字串長度   (ChrisUw add)
    public static Map<String , String> sortByKeyLength(Map<String , String> map){
    	List list = new LinkedList(map.entrySet());
       	Collections.sort(list, new Comparator() {
       	    public int compare(Object o1, Object o2) {
       		return ((Comparable) ((Map.Entry) (o1)).getKey().toString().length()).compareTo(((Map.Entry) (o2)).getKey().toString().length());
       	    }
       	});
       	Collections.reverse(list);
       	Map result = new LinkedHashMap();
       	for (Iterator it = list.iterator(); it.hasNext();) {
       	    Map.Entry entry = (Map.Entry) it.next();
       	    result.put(entry.getKey(), entry.getValue());
       	}
       	return result;
    }
    //由大到小，依據Key的字串長度   (ChrisUw add)    
    public static Map<String , WikiTermBean_backup> sortByKeyLengthForWikiTerm_backup(Map<String, WikiTermBean_backup> map){
    	List list = new LinkedList(map.entrySet());
       	Collections.sort(list, new Comparator() {
       	    public int compare(Object o1, Object o2) {
       		return ((Comparable) ((Map.Entry) (o1)).getKey().toString().length()).compareTo(((Map.Entry) (o2)).getKey().toString().length());
       	    }
       	});
       	Collections.reverse(list);
       	Map result = new LinkedHashMap();
       	for (Iterator it = list.iterator(); it.hasNext();) {
       	    Map.Entry entry = (Map.Entry) it.next();
       	    result.put(entry.getKey(), entry.getValue());
       	}
       	return result;
    }
    //由大到小，依據Key的字串長度   (ChrisUw add)    
    public static Map<String , WikiTermBean> sortByKeyLengthForWikiTerm(Map<String, WikiTermBean> map){
    	List list = new LinkedList(map.entrySet());
       	Collections.sort(list, new Comparator() {
       	    public int compare(Object o1, Object o2) {
       		return ((Comparable) ((Map.Entry) (o1)).getKey().toString().length()).compareTo(((Map.Entry) (o2)).getKey().toString().length());
       	    }
       	});
       	Collections.reverse(list);
       	Map result = new LinkedHashMap();
       	for (Iterator it = list.iterator(); it.hasNext();) {
       	    Map.Entry entry = (Map.Entry) it.next();
       	    result.put(entry.getKey(), entry.getValue());
       	}
       	return result;
    }
    public static Map sortByKey(Map map) {
   	List list = new LinkedList(map.entrySet());
   	Collections.sort(list, new Comparator() {
   	    public int compare(Object o1, Object o2) {
   		return ((Comparable) ((Map.Entry) (o1)).getKey()).compareTo(((Map.Entry) (o2)).getKey());
   	    }
   	});

   	Map result = new LinkedHashMap();
   	for (Iterator it = list.iterator(); it.hasNext();) {
   	    Map.Entry entry = (Map.Entry) it.next();
   	    result.put(entry.getKey(), entry.getValue());
   	}
   	return result;
       }

//    public static Map sortByTermWeight(Map unsortedMap) {
//	Map sortedMap = new HashMap();
//	List list = new ArrayList(unsortedMap.entrySet());
//	java.util.Collections.sort(list, new Comparator() {
//	    public int compare(Object o1, Object o2) {
//		Map.Entry entry1 = (Map.Entry) o1;
//		Map.Entry entry2 = (Map.Entry) o2;
//
//		Double weight1 = ((Term) entry1.getValue()).getWeight();
//		Double weight2 = ((Term) entry2.getValue()).getWeight();
//		return weight1.compareTo(weight2);
//
//	    }
//	});
//	for (Iterator listIt = list.iterator(); listIt.hasNext();) {
//	    Map.Entry sitePairs = (Map.Entry) listIt.next();
//	    sortedMap.put(sitePairs.getKey(), sitePairs.getValue());
//	}
//	return sortedMap;
//    }
}
