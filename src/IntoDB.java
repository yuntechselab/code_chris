import java.util.ArrayList;



import tools.files.LoadProps;
import databaseTools.OurSqlConnection;


public class IntoDB {
	public static void writeIntoDB(ArrayList<String> wordList) {
		OurSqlConnection conn = new OurSqlConnection(LoadProps.getProperties("databaseName"));
		for(String sql : wordList){
		    System.out.println(sql);
		    conn.setSql(sql);
		    conn.InsertNew(sql);
		}
	    }
	}