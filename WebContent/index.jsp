
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page session="true"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script type="text/javascript" charset="utf-8">
	document.getElementsByTagName('html')[0].className = 'hasJS';
</script>

<link rel="shortcut icon" type="image/x-icon" href="favicon.ico">

<!-- Tony -->
<link rel="stylesheet" href="css/vs.css" type="text/css" media="screen">
<script src="https://www.google.com/jsapi" type="text/javascript"></script>
<script type="text/javascript" charset="utf-8">
	google.load("jquery", "1.4.4");
</script>
<script src="js/arbor.js" type="text/javascript" charset="utf-8"></script>
<script src="js/arbor-tween.js" type="text/javascript" charset="utf-8"></script>
<script src="js/arbor-graphics.js" type="text/javascript"
	charset="utf-8"></script>
<script src="js/visual-thesaurus.js" type="text/javascript"
	charset="utf-8"></script>
<meta http-equiv="Content-Script-Type" content="text/javascript" />
<meta http-equiv="Content-Style-Type" content="text/css" />
<script type="text/javascript"
	src="https://code.jquery.com/jquery-latest.min.js"></script>
<style type="text/css">
ul, li {
	margin: 0;
	padding: 0;
	list-style: none;
}

.abgne_tab {
	
	clear: left;
	width: 400px;
	margin: 20px 0;
}




ul.tabs {
	width: 100%;
	height: 32px;
	border-bottom: 1px solid #999;
	border-left: 1px solid #999;
}

ul.tabs li {
	float: left;
	height: 31px;
	line-height: 31px;
	overflow: hidden;
	position: relative;
	margin-bottom: -1px; /* 讓 li 往下移來遮住 ul 的部份 border-bottom */
	
	border-left: none;
	background: #e1e1e1;
}

ul.tabs li a {
	display: block;
	padding: 0 20px;
	color: #000;
	border: 1px solid #fff;
	text-decoration: none;
}

ul.tabs li a:hover {
	background: #ccc;
}

ul.tabs li.active {
	background: #fff;
	border-bottom: 1px solid #fff;
}

ul.tabs li.active a:hover {
	background: #fff;
}

div.tab_container {
	clear: left;
	width: 100%;
	border: 1px solid #999;
	border-top: none;
	background: #fff;
}

div.tab_container .tab_content {
	padding: 20px;
}

div.tab_container .tab_content h2 {
	margin: 0 0 20px;
}

.sec_tab {
	width: 100%; /* 第2個頁籤區塊的寬度 */
	/* 1000px 這種呈現方式真的是一位白癡笨到極點呆瓜腦殘智障腦袋裝糨糊的一個人才想出來的方法*/
}

input[type="submit"] {
	position: absolute;
	top: 0px;
	left: 180px;
	background: url(images/searchIcon.gif);
	background-repeat: no-repeat;
	background-size: cover;
	width: 30px;
	height: 30px;
	border: 0px;
}

#Button1 {
	left:320px;
	top:11px;
	width:35px;
	height:35px;
}

.fix{
  position: fixed;
  bottom: 0;
  right: 0;
  width: 200px;
  background-color: white;
  }

#TT {
	position: absolute;
	top: 11px;
	left: 20px;
	height: 30px;
	width:320px;
}

#formSubmit {
	height: 30px;
}

#ontologyButton {
	background: url(images/Ontology_01.jpg);
	background-repeat: no-repeat;
	background-size: cover;
	width: 130px;
}

#suggest {
	background: url(images/Suggest_01.jpg);
	background-repeat: no-repeat;
	background-size: cover;
	width: 130px;
}

#ontology {
	background: url(images/Acceptance_01.jpg);
	background-repeat: no-repeat;
	background-size: cover;
	width: 130px;
}

#logo img {
	position: absolute;
	top: 5px;
	/*left: 600px; 這種呈現方式真的是一位白癡笨到極點呆瓜腦殘智障腦袋裝糨糊的一個人才想出來的方法*/
	right:0px;
	width: 400px;
	height: 122px;
}

#tab4 {
	
}


</style>

<script type="text/javascript">
	$(document)
			.ready(
					function() {
						
						var cWidth = window.innerWidth;
						var cHeight = window.innerHeight;
						
						//
						if ($("#TT").val() == '') {

							
							
							//logo img
							$("#logoimg").attr("src","images/logo.png");
							$("#logoimg").attr("width","800");
							
							//Div control logo and inputbox and icon
							
							$('.TT').css({
								    'position' : 'absolute',
							        'left' : '50%',
							        'top' : '50%' ,
							        'margin-left' : '-400px', 
							        'margin-top' : '-150px'
							    });
							
							
							$("#formSubmit").css("position", "absolute");
							$("#formSubmit").css("top", "370px");
							$("#formSubmit").css("left", "220px");
							

							$("#TT").css("position","absolute");
							$("#TT").css("width", "330px");
							$("#TT").css("height", "20px");
							$("#TT").css("left", "0");
							$("#TT").css("top", "-100px");
							/* $("#TT").css("left", "100px");
							$("#TT").css("top", "-20px"); 這種呈現方式真的是一位白癡笨到極點呆瓜腦殘智障腦袋裝糨糊的一個人才想出來的方法*/
							
							
							$("input[type='submit']").css("position",
									"relative");
							$("input[type='submit']").css("top", "-100px");
							//$("input[type='submit']").css("left", "90px");
							$("input[type='submit']").css("background",
									"url(images/searchIcon.gif)");
							$("input[type='submit']").css("background-repeat",
									"no-repeat");
							$("input[type='submit']").css("background-size",
									"contain");
							$("input[type='submit']").css("width", "30px");
							$("input[type='submit']").css("height", "26px");
							$("input[type='submit']").css("border", "0");
						}else{
							$(".iframeCss").css("height", cHeight-50);
						}

						//
						val = $("#wordInput").val();
						visThesaurus.wordRequest(val);
						$("#formWrapper").hide();
						$("#wrapper").hide(); //Chris add 2014/10/14 (maybe have some error!!);

					});
</script>
<script type="text/javascript">
	$(function() {
		// 預設顯示第一個 Tab
		var _showTab = 0;
		$('.abgne_tab').each(
				function() {
					// 目前的頁籤區塊
					var $tab = $(this);

					var $defaultLi = $('ul.tabs li', $tab).eq(_showTab)
							.addClass('active');
					$($defaultLi.find('a').attr('href')).siblings().hide();

					// 當 li 頁籤被點擊時...
					// 若要改成滑鼠移到 li 頁籤就切換時, 把 click 改成 mouseover
					$('ul.tabs li', $tab).click(
							function() {
								// 找出 li 中的超連結 href(#id)
								var $this = $(this), _clickTab = $this
										.find('a').attr('href');
								// 把目前點擊到的 li 頁籤加上 .active
								// 並把兄弟元素中有 .active 的都移除 class
								$this.addClass('active').siblings('.active')
										.removeClass('active');
								// 淡入相對應的內容並隱藏兄弟元素
								$(_clickTab).stop(false, true).fadeIn()
										.siblings().hide();

								return false;
							}).find('a').focus(function() {
						this.blur();
					});
				});
	});
</script>




<script>
	$(function() {
		callMe = function(strNo) {
			$('#TT').val(strNo);
			$("#formSubmit").submit();
		}

	});
</script>
<script>
	function aTest() {

	}
	$(window).resize(function() {
		window.location.reload("https://140.125.84.49/Wiki/")
	});
</script>
<title>Weave the web of words</title>





</head>

<body>
	<div class="TT"><img id="logoimg" alt="" src="">
	<form id="formSubmit" name="form1" action="ControllerDouble" method="POST">
		<input name="wordInput" type="text" id="TT"
			value="<%if (session.getAttribute("queryWord") != null) {
				String queryWord = session.getAttribute("queryWord").toString();
				if (queryWord != null) {
					out.print(queryWord);
				}
			}%>" />
		<input type="submit" name="Button1" id="Button1" value=""
			onclick="aTest()" />
		<!-- value="查詢" -->
	</form>
</div>
	<%
		if (session.getAttribute("queryWord") != null) {
			out.write("<div id=\"logo\">");
			out.write("<a href=\"reload.jsp\"><img src=\"images/logo.png\">");
			out.write("</div>");
			out.write("<div id=\"wrapper\"");
			out.write("<form action=\"/\" id=\"formWrapper\">");
			out.write("<label for=\"wordInput\">");
			out.write("<input type=\"text\" name=\"wordInput\" id=\"wordInput\" value=\"\" />");
			out.write("<input type=\"submit\" value=\"Search\" id=\"wordSubmit\" name=\"wordSubmit\" />");
			out.write("</label>");
			out.write("</form>");
			out.write("<div id=\"re-search\" style=\"display: none;\">");
			out.write("<a href=\"#\" class=\"close\">X</a>");
			out.write("<p>Do you want to search for: <span></span></p>");
			out.write("<ul>");
			out.write("<li><a href=\"#\" class=\"yes\">Yes</a></li>");
			out.write("<li><a href=\"#\" class='no'>No</a></li>");
			out.write("</ul>");
			out.write("</div>");
			out.write("</div>");
			out.write("<div class=\"abgne_tab sec_tab\">");
			out.write("<ul class='tabs'>");
			out.write("<li id=\"ontologyButton\"><a href=\"#tab4\"></a></li>");
			out.write("<li id=\"ontology\"><a href=\"#tab3\"></a></li>");
			out.write("<li id=\"suggest\"><a href=\"#tab5\"></a></li>");
			out.write("</ul>");
			out.write("<div class=\"tab_container\">");
			out.write("<div id=\"tab4\" class=\"tab_content\">");
			out.write("<iframe class=\"iframeCss\" src=\"ontology.js.jsp\" name=\"bb\" width=\"100%\"   marginheight=\"0\" marginwidth=\"0\" frameborder=\"0\" scrolling=\"yes\"></iframe>");
			out.write("	<!--  <canvas id='viewport' ></canvas>-->");
			out.write("</div>");
			out.write("<div id=\"tab3\" class=\"tab_content\">");
			out.write("<iframe class=\"iframeCss\" src=\"acceptance.jsp\" name=\"bb\" width=\"100%\"   marginheight=\"0\" marginwidth=\"0\" frameborder=\"0\" scrolling=\"yes\"></iframe>");
			out.write("</div>");
			out.write("<div id=\"tab5\" class=\"tab_content\">");
			out.write("<iframe class=\"iframeCss\" src=\"suggest.jsp\" name=\"bb\" width=\"100%\"   marginheight=\"0\" marginwidth=\"0\" frameborder=\"0\" scrolling=\"yes\"></iframe>");
			out.write("</div>");
			out.write("</div>");
		}
	%>
	<!-- w950h400sec_tab1000px這種呈現方式真的是一位白癡笨到極點呆瓜腦殘智障腦袋裝糨糊的一個人才想出來的方法 -->
</body>
</html>