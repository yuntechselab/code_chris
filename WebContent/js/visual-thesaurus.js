/** 
 * @projectDescription	Visual Thesaurus
 * @author 	Matt Hobbs
 * @version 	0.1
 *
 * TODO:
 * DONE: Make words square
 * DONE: Try a few settings out,stop the juttering
 * Adjust colour pallete
 * DONE: Ajax loader
 * DONE: Clear canvas on search
 * DONE: Add link to data source + arbor
 * DONE: New API Key
 * DONE: Add cursors
 * DONE: Adjust alpha
 * DONE: Code cleanup
 * DONE: Add resize
 * DONE: Click word re-search
 */


var visThesaurus = function(){
    //Define vars
    var allData, val, sys;
	
	//Particle Colo(u)rs
	var particleColour = {
	    root: '#ffffff',//ff3030 #FF0000//中心點的顏色
		noun: '#FC930A',
		verb: '#CCF390',
		adjective: '#E0E05A',
		subset: '#F7C41F',
		words: '#AAAAAA'  //A0C230 #DEFFAC #0000FF //字的背景
	}
    
    return {
        init:function(){
            //Attach the events
           
            $("#wordSubmit").click(function (e) {
            	val = $("#wordInput").val();
                visThesaurus.wordRequest(val);
                
                return false;
            });
            
            //Start the renderer
            sys = arbor.ParticleSystem({friction:1, stiffness:900, repulsion:100, gravity: true, fps: 60, dt: 0.08, precision: 1.0});
            sys.renderer = Renderer();
        },
        wordRequest:function(word){
        	//測試json格式
        	//var url = "test1.jsp";
            //正常版
        	var url = "presentation.jsp";  //presentation.jsp
            //Clear all previous particles
			sys.prune(function(node, from, to){return true;});
			
            $.ajax({
				url: url,
				dataType: "json",
				type: 'GET',
				error: visThesaurus.wordError,
                success: visThesaurus.wordSuccess,
				complete: function(){$("img.loader").remove();}
            });
        },
        wordSuccess:function(data){
            //Original word isn't returned in data
            var original = $("#wordInput").val();
            
            
            
			//Store all data recieved
            allData = data;
            
            //Add root node
//            sys.addNode('home', { label: original, use: 'home', alpha: '1', color: particleColour.root, expanded: true, level: 0, parent: null });
            sys.addNode('home', { label: original, use: 'home', alpha: '1', color: null, expanded: true, level: 0, parent: null });

            //Original code
//            var dataSet= allData.verb.syn;
//            $.each(dataSet, function (i, word) {
//            var lbl = "lbl" + i;
//            sys.addNode(lbl, { label: word, wordlength: word.length, use: 'word', alpha: '1', color: particleColour.words, expanded: false, level:1, parent: 'home' });
//            sys.addEdge('home', lbl);
//            
//        	});
         
            //add level2
//            $.each(allData, function (i, word) {
//            	sys.addNode(i, {label: word[0], wordlength: word[0].length, use: 'word', alpha: '1', color: particleColour.words, expanded: false, level:2, parent: 'home' });
//                sys.addEdge('home', i);
//            }); 
            //第一層，標註有第二層的node
            //16,0909 Richard
            $.each(allData, function (i, word) {
            	if(allData[i].syn.length!=0){
            		word[0]=word[0]+' [﹢]'
            		//word[0]=word[0]+'👇'
            	}
            	sys.addNode(i, {label: word[0], wordlength: word[0].length, use: 'word', alpha: '1', color: particleColour.words, expanded: false, level:2, parent: 'home' });
                sys.addEdge('home', i);
            }); 
           
            $("#flag").html($.now());
           
            
        },
        wordError: function(error){
            console.warn("There was an error: " + error);
        },
        generateSubs: function(nodeName){
            //Used to generate the sub-sections
        	//第二層(展開)
        	//by Richard 160908
        	var level1 = allData[nodeName].syn;
        	for(var key in level1){
        		var level1Name = level1[key];
            	sys.addNode(level1Name, { label: level1[key], wordlength: level1[key].length, use: 'word', alpha: '1', color: particleColour.words, expanded: false, level:1, parent: nodeName });
                sys.addEdge(nodeName, level1Name);
        	}
        },
        generateWords: function(nodeName){
       
            
            
        },
		newSearch: function(newword){
			
			$re = $("#re-search");
			$re.find("p span").text(newword).end().hide();
			$("#re-search").hide();
			
			$("#re-search").hide();
			var bb = newword; 
			$("#TT").val(newword);
			$("p.error").remove();
			$("<img>").attr({class:"loader", src: "images/1.gif"}).appendTo("#formWrapper");
        
	        e = newword;
	        var wParent = window.parent;
	        wParent.callMe(e);

			$("#formSubmit").submit();
				visThesaurus.wordRequest(newword);
				return false;
				//alert(newword);
				
				//return false;
//			});
		},
		
    	//by Richard 160908
		removeNodes: function(name, level){
			if(level === 2){
	        	var level1 = allData[name].syn;
	            for(var key in level1){
	            	var level1Name = level1[key];
	            	sys.addNode(level1Name, { label: level1[key], wordlength: level1[key].length, use: 'word', alpha: '1', color: particleColour.words, expanded: false, level:1, parent: null });
	            	sys.pruneNode(level1[key]);
	            }
	            $.each(allData, function (i, word) {
	            	
	            	sys.addNode(i, {label: word[0], wordlength: word[0].length, use: 'word', alpha: '1', color: particleColour.words, expanded: false, level:2, parent: 'home' });
	            	sys.addEdge('home', i);
	            }); 
//				//Clicked node
////				var clicked = sys.getNode(name);
//				
//				//Clicked parent node
////				var parent = sys.getNode(clicked.data.parent);
//				
//				//Parent is nolonger expanded
////				parent.data.expanded = false;
//				
//				//Remove Children
////				sys.prune(function(node, from, to){
////					if(node.data.parent === name){
////						return true;
////					}
////				});
//////				
//////				//Remove clicked node too
////				sys.pruneNode(name);
			}
		}
    }
}();

//The arbor renderer
var Renderer = function(){
    var canvas = document.getElementById("viewport");
	var dom = $("#viewport");
    
    //Screen size
	var cWidth = canvas.width = window.innerWidth;
	var cHeight = canvas.height = window.innerHeight;
    
    var context = canvas.getContext('2d');
    var particleSystem;
   
    //canvas 是html元素
   var gfx = arbor.Graphics(canvas);
    
    return {
        init:function(system){
            //Define some particle system settings
            particleSystem = system;
            particleSystem.screenSize(cWidth, cHeight);
            particleSystem.screenPadding(100);
            
            //Node dragging
            this.initMouseHandling();
			
			//On window resize
			$(window).resize(this.windowsized);
        },
		windowsized:function(){
			cWidth = ("#tab4").innerWidth;
			cHeight =("#tab4").innerHeight; ////////123123123123123123213123123123
			
			particleSystem.screenSize(cWidth, cHeight);
		},
        redraw:function(){
            //Set the canvas styles for every redraw
            context.fillStyle = '#FFFFFF'; //設定Ontology背板
            context.fillRect(0,0, cWidth, cHeight);
            
            //The lines between nodes
            particleSystem.eachEdge(function(edge, pt1, pt2){
                // draw a line from pt1 to pt2
                context.strokeStyle = "rgba(0,0,0, .222)"; //rgba(0,0,0,.333)";
                context.lineWidth = 1;
                context.beginPath();
                context.moveTo(pt1.x, pt1.y);
                context.lineTo(pt2.x, pt2.y);
                context.stroke();
            });
            
            //The style of each node
            particleSystem.eachNode(function(node, pt){
                var w;
                
                //Create our different types of nodes
                if(node.data.use === 'home'){
                	if(node.data.label.length==1){ //判斷一個字元
                		w = node.data.label.length*120;
	                	gfx.oval(pt.x-w/2, pt.y-w/4-5, w, w/2, {fill:node.data.color, alpha:node.data.alpha});
	                    gfx.text(node.data.label, pt.x + 10, pt.y + 22, { color: '#000000', align: "center", font: "Arial", size: 70 });
	                	
                	}else{
	                	if(node.data.label.indexOf('(')==-1){
		                	w = node.data.label.length*48;
		                	gfx.oval(pt.x-w/2, pt.y-w/4-5, w, w/2, {fill:node.data.color, alpha:node.data.alpha});
		                    gfx.text(node.data.label, pt.x + 10, pt.y + 22, { color: '#000000', align: "center", font: "Arial", size: 70 });
		                	
	                	}else{
	                		//中心點
	                		w = node.data.label.length*21;
	                	    gfx.oval(pt.x-w/2, pt.y-w/4-5, w, w/2, {fill:node.data.color, alpha:node.data.alpha});
	                	    gfx.text(node.data.label, pt.x + 10, pt.y + 22  ,{ color: '#000000', align: "center", font: "Arial" , size: 70 });
	                	}	
                	}
                } else if(node.data.use === 'type'){
                    w = 70;
                    gfx.oval(pt.x-w/2, pt.y-w/2, w, w, {fill:node.data.color, alpha:node.data.alpha});
                    gfx.text(node.data.label, pt.x, pt.y + 4, { color: '#000000', align: "center", font: "Arial", size: 11 });
                } else if(node.data.use === 'sub-type'){
                    w = 100;  //80
                    gfx.oval(pt.x-w/2, (pt.y-w/2)+w/4, w, w/2, {fill:node.data.color, alpha:node.data.alpha});
                    gfx.text(node.data.label, pt.x, pt.y + 4, { color: '#000000', align: "center", font: "sans-serif", size: 10});
                } else if(node.data.use === 'word'){
//                	Original	#0000FF
//                	w = parseInt((node.data.wordlength*21) +0, 10);
//                    gfx.rect(pt.x - w / 2,pt.y - 8 , w, 30, 5,  { fill: node.data.color, alpha: node.data.alpha })
//                    gfx.text(node.data.label, pt.x, pt.y +15, { color: '#000000', align: "center", font: "Arial", size: 20 });
                	gfx.text(node.data.label, pt.x +25, pt.y +25, { color: '#000000', align: "center", font: "Arial", size: 22 });
                	
                }
            });
        },
        initMouseHandling:function(){
			var _mouseP, selected;
            var dragged = null;
			var nearest = null;

            var handler = {
				moved:function(e){
					var pos = $(canvas).offset();
					_mouseP = arbor.Point(e.pageX-pos.left, e.pageY-pos.top);
					nearest = particleSystem.nearest(_mouseP);
					
					if(!nearest.node){
						return false;
					}
					
					if(!(nearest.node.data.use === 'home')){
						selected = (nearest.distance < 30) ? nearest : null;
						if(selected){
							dom.addClass('hovered');
						} else {
							dom.removeClass('hovered');
						}
					}
					return false;
				},
                clicked:function(e){
                    var pos = $(canvas).offset();
                    _mouseP = arbor.Point(e.pageX-pos.left, e.pageY-pos.top);
                    dragged = particleSystem.nearest(_mouseP);
                    
                    if (dragged && dragged.node !== null){
                        //Store the clicked node name
						var nName, nLevel;
						
                        if(!dragged.node.data.expanded){
                            dragged.node.data.expanded = true;
                            nName = dragged.node.name;
                            nLevel = dragged.node.data.level;
                            if(nLevel === 2){
                                visThesaurus.generateSubs(nName);
                            } else if(nLevel === 3) {
                                //We are generating the words
                                visThesaurus.generateWords(nName);
							} else if(nLevel === 1) {
								//Clicking on a single word, search it?
								visThesaurus.newSearch(dragged.node.data.label);

                            } else {
                                return false;
                            }
                        } else {
							//Node expanded so remove
                        	dragged.node.data.expanded = false;
							nName = dragged.node.name;
							nLevel = dragged.node.data.level;
							visThesaurus.removeNodes(nName, nLevel);
						}
                        dragged.node.fixed = true;
                    }

                    $(canvas).bind('mousemove', handler.dragged);
                    $(window).bind('mouseup', handler.dropped);
                    return false;
                },
                dragged:function(e){
                    var pos = $(canvas).offset();
                    var s = arbor.Point(e.pageX-pos.left, e.pageY-pos.top);
                    
                    if (dragged && dragged.node !== null){
                        var p = particleSystem.fromScreen(s);
                        dragged.node.p = p;
                    }
                    
                    return false;
                },
                dropped:function(e){
                    if (dragged===null || dragged.node===undefined) {
                        return;
                    }
                    if (dragged.node !== null){
                        dragged.node.fixed = false;
                    }
                    dragged.node.tempMass = 1000;
                    dragged = null;
                    $(canvas).unbind('mousemove', handler.dragged);
                    $("#tab4").unbind('mouseup', handler.dropped); //$(window).unbind('mouseup', handler.dropped); 
                    _mouseP = null;
                    return false;
                }
            }

            // start listening
            $(canvas).mousedown(handler.clicked);
			$(canvas).mousemove(handler.moved);
        }
    }
}

jQuery(function($){
    visThesaurus.init();
});