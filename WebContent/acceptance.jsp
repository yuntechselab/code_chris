<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="org.json.*" %>
<%@ page import="java.util.*" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>acceptance</title>

<!-- link rel="stylesheet" href="//code.jquery.com/ui/1.11.0/themes/smoothness/jquery-ui.css"> -->
  <script src="//code.jquery.com/jquery-1.10.2.js"></script>
  <script src="//code.jquery.com/ui/1.11.0/jquery-ui.js"></script>
  		<link rel="stylesheet" href="css/jquery-ui.css" type="text/css">
  
  <script>
     $(function () {
    	  $("#error").hide(); //未輸入
    	 
          $("#tn").hide();
          var tn = $("#tn").text();
          for(var i=1;i<=tn;i++)
          {
              $("#slider-range-min"+i).slider({
                  disabled: true,
                  range: "min",
                  value: $("#aa"+i).text(),
                  min: 0,
                  max: $("#tt"+i).text(),
                  slide: function (event, ui) {
                  }
              });
          }
      });
  	function check(){
  		var index = $('input[name="hiddenIndex"]').val();
  		for (var i=1; i<=index; i++)
  		{	
  		   var term = "term" + i;
  		   if (typeof($('input:radio:checked[name='+ term+ ']').val()) ==='undefined')
  		   {
  			   $("#error").show();
  			 //window.alert("Have some missing");
  			 return false;
   		   }
  		}
  		$('#form1').submit();
  		return true;
  	}
   </script>
  <style>
  .aa{display:inline}
  #slider-range-min .ui-slider-range { background: #ef2929; }
  #slider-range-min .ui-slider-handle { border-color: #ef2929; }
  </style>
</head>
<body>


<%
	if(session.getAttribute("JSONAcceptance") != null){
		String tmp = session.getAttribute("JSONAcceptance").toString();
		JSONObject json = new JSONObject(tmp);
		String isAlready = session.getAttribute("alreadyAcceptance").toString();
		//out.print(json);
		out.print("<form id=form1 action=./ControllerAcceptance>");
		out.print("<table cellpadding=2 style="+"\""+"width:710px" + "\""+">");
		Iterator iterator  = json.keys();
		int index =1;
		out.print("<tr>");
		out.print("<th align=center style="+"\""+"width:200px" + "\""+" >相關詞</th>");
		out.print("<th align=center style="+"\""+"width:60px" + "\""+">萃取來源</th>");
		out.print("<th align=center style="+"\""+"width:200px" + "\""+">接受度(圖)</th>");
		out.print("<th align=center style="+"\""+"width:90px" + "\""+">接受人數/總人數</th>");
		if(isAlready.equals("false")){
			out.print("<th align=center style="+"\""+"width:70px" + "\""+">接受</th>");
			out.print("<th align=center style="+"\""+"width:90px" + "\""+">不接受</th>");
		}
		out.print("</tr>");
		out.print("<input type =hidden name=queryWord value="+session.getAttribute("queryWord")+">");
		while(iterator.hasNext()){
			out.print("<tr>");
			JSONObject tmpJson = json.getJSONObject(iterator.next().toString());
			out.print("<td align=center>");
			out.print(tmpJson.get("mappingWord") + "\t");
			out.print("<input type=hidden name=hiddenMappingWord"+ index +" value=\"" + tmpJson.get("mappingWord") +"\">");
			out.print("</td>");
			out.print("<td align=center>");
			out.print(tmpJson.get("mappingRule") + "\t");
			out.print("</td>");
			out.print("<td align=center>");
			
			out.print("<div id=" + "\"" + "slider-range-min" + index + "\"" + "style=" + "\"" + "width:200px" + "\"" + ">"+"</div>");
			
			out.print(tmpJson.get("reasonability") + "\t");
			out.print("</td>");
			out.print("<td align=center style="+"\""+"width:60px" + "\""+">");
			out.print("<div><div class="+"\""+"aa"+"\""+"id="+"\""+"aa"+index+"\""+"style=" + "\"" + "width:5px" + "\""+">"+tmpJson.get("agreeNumber")+"</div>" + "/" + "<div class="+"\""+"aa"+"\""+"id="+"\""+"tt"+index+"\""+"style=" + "\"" + "width:5px" + "\""+">"+tmpJson.get("totalNumber")+"</div></div>" );
			out.print("</td>");
			if(isAlready.equals("false")){
				out.print("<td align=center>");
				out.print("<INPUT type=radio id=term"+ index +" name=term"+ index + " value=accept>接受");
				out.print("</td>");
				out.print("<td align=center>");
				out.print("<INPUT type=radio id=term"+ index +" name=term"+ index + " value=notAccept>不接受");
				out.print("</td>");
				out.print("</tr>");
			}
			index++;
			//out.print("<br>");
		}
		out.print("<div id="+"\""+"tn"+"\""+">"+index+"</div>");
		index--;
		out.print("</table>");
		out.print("<br>");
		if(isAlready.equals("false")){
			out.print("<h2 style=color:red id='error'>有些接受度漏掉囉，請填寫完整!!!</h2><INPUT type=button value=送出  onClick=check()>");
		}
		out.print("<input type=hidden id=hiddenIndex name=hiddenIndex value="+ index +">");
		out.print("</form>");
		
		out.print("<br>");
		out.print("<br>");
		out.print("<br>");
		out.print("<br>");
	}
	 
	
%>
</body>
</html>