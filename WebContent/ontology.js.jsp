<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
 <%@ page session="true"%>
 <%@ page import="servlet.*"%>
 
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="keywords" content="">
		<meta name="description" content="">
		<meta name="author" content="">
		<title>Visual Tesaurus using Arbor.js</title>
		
		<link rel="stylesheet" href="css/vs.css" type="text/css" media="screen">
		<script src="https://www.google.com/jsapi" type="text/javascript"></script>
		<script type="text/javascript" charset="utf-8" >google.load("jquery", "1.4.4");</script>
		
		<script type="text/javascript" charset="utf-8" >document.getElementsByTagName('html')[0].className = 'hasJS';</script>
		<script src="js/arbor.js" type="text/javascript" charset="utf-8" ></script>
		<script src="js/arbor-tween.js" type="text/javascript" charset="utf-8" ></script>
		<script src="js/arbor-graphics.js" type="text/javascript" charset="utf-8" ></script>
		<script src="js/visual-thesaurus.js" type="text/javascript" charset="utf-8" ></script>
	
	
	<script type="text/javascript">
            $(document).ready(function () {
                val = $("#wordInput").val();
                visThesaurus.wordRequest(val);
                $("#formWrapper").hide();
            });
        </script>
	</head>

	<body>
        <!-- <div id="flag"></div> -->
		<div id="wrapper">
			<form  id="formWrapper" >
				
				<label for="wordInput">
					<input type="text" name="wordInput" id="wordInput" value="<%=session.getAttribute("finalQueryWord")%>" />
					<input type="submit" value="Search" id="wordSubmit" name="wordSubmit" />
				</label>
			</form>
			
			<div id="re-search" style="display: none;">
				<a href="#" class="close">X</a>
				<p>Do you want to search for: <span></span></p>
				<ul>
					<li><a href="#" class="yes">Yes</a></li>
					<li><a href="#" class="no">No</a></li>
				</ul>
			</div>
		</div>
		<canvas id="viewport"></canvas>
		
	
	</body>
</html>