<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="org.json.*" %>
<%@ page import="java.util.*" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>acceptance</title>

 <link rel="stylesheet" href="//code.jquery.com/ui/1.11.0/themes/smoothness/jquery-ui.css">
  <script src="//code.jquery.com/jquery-1.10.2.js"></script>
  <script src="//code.jquery.com/ui/1.11.0/jquery-ui.js"></script>
  
  <script>
     $(function () {
          $("#tn").hide();
          var tn = $("#tn").text()-1;
          for(var i=1;i<=tn;i++)
          {
              $("#slider-range-min"+i).slider({
                  disabled: true,
                  range: "min",
                  value: $("#aa"+i).text(),
                  min: 1,
                  max: $("#tt"+i).text(),
                  slide: function (event, ui) {
                  }
              });
          }
      });
  </script>
  
</head>
<body>


<%
	if(session.getAttribute("JSONAcceptance") != null){
		String tmp = session.getAttribute("JSONAcceptance").toString();
		JSONObject json = new JSONObject(tmp);
		//out.print(json);
		out.print("<form action=./ControllerAcceptance>");
		out.print("<table cellpadding=2>");
		Iterator iterator  = json.keys();
		int index =1;
		out.print("<tr>");
		out.print("<th align=center>相關詞</th>");
		out.print("<th align=center>萃取來源</th>");
		out.print("<th align=center>接受度(圖)</th>");
		out.print("<th align=center>接受人數/總人數</th>");
		out.print("<th align=center>接受</th>");
		out.print("<th align=center>不接受</th>");
		out.print("</tr>");
		while(iterator.hasNext()){
			out.print("<tr>");
			JSONObject tmpJson = json.getJSONObject(iterator.next().toString());
			out.print("<td align=center>");
			out.print(tmpJson.get("mappingWord") + "\t");
			out.print("<input type=hidden name=hiddenMappingWord"+ index +" value=\"" + tmpJson.get("mappingWord") +"\">");
			out.print("</td>");
			out.print("<td align=center>");
			out.print(tmpJson.get("mappingRule") + "\t");
			out.print("</td>");
			out.print("<td align=center>");
			
			out.print("<div id=" + "\"" + "slider-range-min" + index + "\"" + "style=" + "\"" + "width:500px" + "\"" + ">"+"</div>");
			
			out.print(tmpJson.get("reasonability") + "\t");
			out.print("</td>");
			out.print("<td align=center>");
			//int TT = Integer.parseInt(s)
			out.print("<div id="+"\""+"tt"+index+"\""+">"+100+"</div>" + "/" + "<div id="+"\""+"aa"+index+"\""+">"+10+"</div>" + "\t");
			out.print("</td>");
			out.print("<td align=center>");
			out.print("<INPUT type=radio name=term"+ index + " value=accept>接受");
			out.print("</td>");
			out.print("<td align=center>");
			out.print("<INPUT type=radio name=term" + index + " value=notAccept>不接受");
			out.print("</td>");
			out.print("</tr>");
			index++;
			
			//out.print("<br>");
		}
		out.print("<div id="+"\""+"tn"+"\""+">"+index+"</div>");
		index--;
		out.print("</table>");
		out.print("<br>");
		out.print("<INPUT type=submit value=送出>");
		out.print("<input type=hidden name=hiddenIndex value="+ index +">");
		out.print("</form>");
	}
	 
	
%>
</body>
</html>